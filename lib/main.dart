import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:olmaapp/src/blocs/address_cities/bloc.dart';
import 'package:olmaapp/src/blocs/address_districs/bloc.dart';
import 'package:olmaapp/src/blocs/address_wards/bloc.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/blocs/delivery_address/bloc.dart';
import 'package:olmaapp/src/blocs/shipping_methods/bloc.dart';
import 'package:olmaapp/src/repository/banner_repository.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'package:olmaapp/src/repository/category_product_repository.dart';
import 'package:olmaapp/src/repository/comment_repository.dart';
import 'package:olmaapp/src/repository/deal_repository.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'package:olmaapp/src/repository/news_repository.dart';
import 'package:olmaapp/src/repository/order_repository.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/repository/promotion_repository.dart';
import 'package:olmaapp/src/repository/shipping_method_repository.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/simple_bloc_delegate.dart';
import 'package:olmaapp/src/ui/screens/news/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/promotion/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/banner_home/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/list_category_home/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/list_deal_home/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/list_product_for_you/bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'src/app.dart';

class AppWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: App(),
    );
  }
}

void main() {
  //Intl.defaultLocale = "zh_HK";
  //initializeDateFormatting('en_US', null);

  final UserRepository userRepository = UserRepositoryImpl();
  final BannerRepository bannerRepository = BannerRepositoryImpl();
  final CategoryProductRepository categoryProductRepository =
      CategoryProductRepositoryImpl();

  final DealRepository dealRepository = DealRepositoryImpl();
  final ProductRepository productRepository = ProductRepositoryImpl();
  final CommentRepository commentRepository = CommentRepositoryImpl();
  final CartRepository cartRepository = CartRepositoryImpl();
  final PromotionRepository promotionRepository = PromotionRepositoryImpl();
  final NewsRepository newsRepository = NewsRepositoryImpl();
  final DeliveryAddressRepository addressRepository =
      DeliveryAddressRepositoryImpl();
  final ShippingMethodRepository shippingMethodRepository =
      ShippingMethodRepositoryImpl();
  final OrderRepository orderRepository = OrderRepositoryImpl();

  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MultiRepositoryProvider(
    providers: [
      RepositoryProvider<UserRepository>(create: (_) => userRepository),
      RepositoryProvider<BannerRepository>(create: (_) => bannerRepository),
      RepositoryProvider<CategoryProductRepository>(
          create: (_) => categoryProductRepository),
      RepositoryProvider<DealRepository>(create: (_) => dealRepository),
      RepositoryProvider<ProductRepository>(create: (_) => productRepository),
      RepositoryProvider<CommentRepository>(create: (_) => commentRepository),
      RepositoryProvider<CartRepository>(create: (_) => cartRepository),
      RepositoryProvider<PromotionRepository>(
          create: (_) => promotionRepository),
      RepositoryProvider<NewsRepository>(create: (_) => newsRepository),
      RepositoryProvider<DeliveryAddressRepository>(
          create: (_) => addressRepository),
      RepositoryProvider<ShippingMethodRepository>(
          create: (_) => shippingMethodRepository),
      RepositoryProvider<OrderRepository>(create: (_) => orderRepository),
    ],
    child: MultiBlocProvider(providers: [
      BlocProvider<AuthenticationBloc>(
        create: (BuildContext context) => AuthenticationBloc(
          userRepository: userRepository,
        )..add(AuthenticationStarted()),
      ),
      BlocProvider<CartBloc>(
        create: (BuildContext context) => CartBloc(
          cartRepository: cartRepository,
        )..add(FetchCartEvent()),
      ),
      BlocProvider<ListProductForYouBloc>(
        create: (BuildContext context) => ListProductForYouBloc(
          productRepository: productRepository,
        )..add(FetchListProductForYouEvent()),
      ),
      BlocProvider<ListDealHomeBloc>(
        create: (BuildContext context) => ListDealHomeBloc(
          dealRepository: dealRepository,
        )..add(FetchListDealHomeEvent()),
      ),
      BlocProvider<ListCategoryHomeBloc>(
        create: (BuildContext context) => ListCategoryHomeBloc(
          categoryProductRepository: categoryProductRepository,
        )..add(FetchListCategoryHomeEvent()),
      ),
      BlocProvider<BannerHomeBloc>(
        create: (BuildContext context) => BannerHomeBloc(
          bannerRepository: bannerRepository,
        )..add(FetchBannerHomeEvent()),
      ),
      BlocProvider<PromotionBloc>(
        create: (BuildContext context) => PromotionBloc(
          promotionRepository: promotionRepository,
        )..add(FetchPromotionEvent()),
      ),
      BlocProvider<NewsBloc>(
        create: (BuildContext context) => NewsBloc(
          newsRepository: newsRepository,
        )..add(FetchNewsEvent()),
      ),
      BlocProvider<DeliveryAddressBloc>(
        create: (BuildContext context) => DeliveryAddressBloc(
          deliveryAddressRepository: addressRepository,
        )..add(FetchDeliveryAddressEvent()),
      ),
      BlocProvider<AddressCitesBloc>(
        create: (BuildContext context) => AddressCitesBloc(
          deliveryAddressRepository: addressRepository,
        )..add(FetchAddressCitesEvent()),
      ),
      BlocProvider<AddressDistricsBloc>(
        create: (BuildContext context) => AddressDistricsBloc(
          deliveryAddressRepository: addressRepository,
        ),
      ),
      BlocProvider<AddressWardsBloc>(
        create: (BuildContext context) => AddressWardsBloc(
          deliveryAddressRepository: addressRepository,
        ),
      ),
      BlocProvider<OlmaNowBloc>(
        create: (BuildContext context) => OlmaNowBloc(
          shippingMethodRepository: shippingMethodRepository,
        ),
      ),
      BlocProvider<GHTKBloc>(
        create: (BuildContext context) => GHTKBloc(
          shippingMethodRepository: shippingMethodRepository,
        ),
      ),
      BlocProvider<GHNBloc>(
        create: (BuildContext context) => GHNBloc(
          shippingMethodRepository: shippingMethodRepository,
        ),
      ),
      BlocProvider<CommentsProductDetailBloc>(
        create: (BuildContext context) =>
            CommentsProductDetailBloc(commentRepository: commentRepository),
      )
    ], child: AppWrapper()),
  ));
}
