import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/layouts/app_bar_layout.dart';
import 'package:olmaapp/src/ui/layouts/bottom_navigation_bar_layout.dart';
import 'package:olmaapp/src/ui/screens/home_screen.dart';
import 'package:olmaapp/src/ui/screens/news/share_screen.dart';
import 'package:olmaapp/src/ui/screens/notification_screen.dart';
import 'package:olmaapp/src/ui/screens/promotion/promotion_screen.dart';
import 'package:olmaapp/src/ui/screens/user/user_screen.dart';

// ignore: missing_return
Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  int _currentIndex = 0;

  List<StatefulWidget> screens;

  _AppState() {
    screens = [
      HomeScreen(),
      PromotionScreen(),
      ShareScreen(),
      NotificationScreen(),
      UserScreen(),
    ];
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, message),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {}
    });
  }

  Widget _buildDialog(BuildContext context, Map<String, dynamic> message) {
    return AlertDialog(
      content: Text("$message"),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {},
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("Push Messaging token: $token");
    });
    _firebaseMessaging.subscribeToTopic("matchscore");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarLayout.getInstace(context, _currentIndex),
      body: Container(
        color: ColorConstants.GREY_APP_BG,
        child: screens[_currentIndex],
      ),
      bottomNavigationBar: BottomNavigationBarLayout.getInstace(
          context, currentIndex, setStateCurrentIndexApp),
    );
  }

  void setStateCurrentIndexApp(int currentIndex) {
    setState(() {
      _currentIndex = currentIndex;
    });
  }

  int get currentIndex => _currentIndex;
}
