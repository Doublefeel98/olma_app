import 'package:dio/dio.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'dart:async';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/models/product.dart';

abstract class ProductRepository {
  Future<ListProductForView> getListProductByCategoryProduct(
      int categoryProductId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1});
  Future<ListProductForView> getListProductByBrand(int brandId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1});
  Future<ListProductForView> getListProductByFilter(String q,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1});

  Future<List<ProductItem>> getListProductSuggestFilter(String keyword,
      {int limit = 5});

  Future<ListProductForYou> getListProductForYou(
      {int limit = 12,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1});

  Future<Product> getProductDetail(int id);
}

class ProductRepositoryImpl extends ProductRepository {
  @override
  Future<ListProductForView> getListProductByCategoryProduct(
      int categoryProductId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1}) async {
    // TODO: implement getListProductCategory

    var url = Endpoint.URL_GET_LIST_PRODUCT_BY_CATEGORY_PRODUCT +
        '?limit=$limit&lowestPrice=$lowestPrice&highestPrice=$highestPrice&rating=$rating&order=$order&page=$page&categoryProductId=$categoryProductId';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception(
            'Fail to get list product by category from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list product by category from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ListProductForView.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<ListProductForView> getListProductByBrand(int brandId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1}) async {
    // TODO: implement getListProductByBrand
    var url = Endpoint.URL_GET_LIST_PRODUCT_BY_BRAND +
        '?limit=$limit&lowestPrice=$lowestPrice&highestPrice=$highestPrice&rating=$rating&order=$order&page=$page&brandId=$brandId';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list product by brand from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list product by brand from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ListProductForView.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<ListProductForView> getListProductByFilter(String q,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1}) async {
    // TODO: implement getListProductByFilter
    var url = Endpoint.URL_GET_LIST_PRODUCT_BY_FILTER +
        '?limit=$limit&lowestPrice=$lowestPrice&highestPrice=$highestPrice&rating=$rating&order=$order&page=$page&q=$q';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list product filter from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list product filter from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ListProductForView.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<Product> getProductDetail(int id) async {
    // TODO: implement getProductDetail

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_PRODUCT_DETAIL + '$id',
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get product detail from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get product detail from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return Product.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<ListProductForYou> getListProductForYou(
      {int limit = 12,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1}) async {
    var url = Endpoint.URL_GET_LIST_PRODUCT_FOR_YOU +
        '?limit=$limit&lowestPrice=$lowestPrice&highestPrice=$highestPrice&rating=$rating&order=$order&page=$page';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list product filter from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list product filter from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ListProductForYou.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<List<ProductItem>> getListProductSuggestFilter(String keyword,
      {int limit = 5}) async {
    // TODO: implement getListProductSuggestFilter
    var url = Endpoint.URL_GET_LIST_PRODUCT_SUGGEST_FILTER +
        '?limit=$limit&keyword=$keyword';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception(
            'Fail to get list product suggest filter from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception(
          'Fail to get list product suggest filter from the Internet');
    }

    List<ProductItem> listProduct = List<ProductItem>();
    if (response != null) {
      dynamic data = response.data;

      for (var item in data["data"]) {
        listProduct.add(ProductItem.fromJson(item));
      }

      return listProduct;
    } else {
      return null;
    }
  }
}
