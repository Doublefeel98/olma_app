import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import 'package:olmaapp/src/models/category_news.dart';

abstract class NewsRepository {
  Future<List<CategoryNews>> getListCategoryNewsForHome();
  Future<News> getNewsDetail(int id);
}

class NewsRepositoryImpl extends NewsRepository {
  @override
  Future<List<CategoryNews>> getListCategoryNewsForHome() async {
    // TODO: implement getListCategoryNewsForHome

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_LIST_CATEGORY_NEWS,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list category news from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list category news from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<CategoryNews> listCategoryNews = new List<CategoryNews>();
      if (data != null) {
        data.forEach((v) {
          listCategoryNews.add(new CategoryNews.fromJson(v));
        });
      }
      return listCategoryNews;
    } else {
      return null;
    }
  }

  @override
  Future<News> getNewsDetail(int id) async {
    // TODO: implement getNewsDetail

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_NEWS_DETAIL,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get news detail from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get news detail from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return News.fromJson(data);
    } else {
      return null;
    }
  }
}
