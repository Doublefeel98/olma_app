import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class UserRepository {
  Future<User> getCurrentUser();

  Future<User> signIn(
      {@required String email,
      @required String password,
      bool rememberMe = false});

  Future<ResultUserResponse> signUp(
      {String name, String email, String password, String confirmPassword});

  Future<void> deleteToken();

  Future<void> persistToken(String token);

  Future<bool> hasToken();

  Future<User> signInWithGoogle();

  Future<User> signInWithFacebook();

  Future<void> signOut();

  Future<ResultUserResponse> updateProfile(
      {@required String name,
      @required String email,
      @required String phoneNumber});

  Future<ResultUserResponse> updatePassword(
      {String oldPassword = "",
      @required String newPassword,
      @required String confirmNewPassword});
}

class UserRepositoryImpl extends UserRepository {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;
  final FacebookLogin _facebooklogin;

  UserRepositoryImpl(
      {FirebaseAuth firebaseAuth,
      GoogleSignIn googleSignin,
      FacebookLogin facebookLogin})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
        _googleSignIn = googleSignin ?? GoogleSignIn(),
        _facebooklogin = facebookLogin ?? FacebookLogin();

  Future<User> getCurrentUser() async {
    String token =
        await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_CURRENT_USER,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get current user from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get current user from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return data["user"] != null ? User.fromJson(data["user"]) : null;
    } else {
      return null;
    }
  }

  @override
  Future<User> signIn(
      {@required String email,
      @required String password,
      bool rememberMe = false}) async {
    // set up POST request arguments
    String url = Endpoint.URL_POST_LOGIN_DEFAULT;

    Dio dio = Dio();
    Response response;
    rememberMe = rememberMe ?? false;
    try {
      response = await dio
          .post(url,
              data: {
                "email": email,
                "password": password,
                "remember_me": rememberMe
              },
              options: Options(
                  headers: {'Accept': 'application/json; charset=UTF-8'},
                  followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to normal login from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to normal login from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      String accessToken = data['accessToken'];

      await persistToken(accessToken);

      return User.fromJson(data['user']);
    } else {
      return null;
    }
  }

  // postData(String url, {Map<String, dynamic> body}) async {
  //   var dio = Dio();
  //   try {
  //     FormData formData = new FormData.fromMap(body);
  //     var response = await dio.post(url,
  //         data: formData,
  //         options: Options(
  //             headers: {'Accept': 'application/json; charset=UTF-8'},
  //             followRedirects: false));
  //     return response.data;
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  @override
  Future<void> deleteToken() async {
    /// delete from keystore/keychain
    await SharedPreferencesHelper.removeValue(USER_TOKEN_KEY);
    return;
  }

  @override
  Future<void> persistToken(String token) async {
    await SharedPreferencesHelper.addStringToSF(
        key: USER_TOKEN_KEY, value: token);
    return;
  }

  @override
  Future<bool> hasToken() async {
    /// read from keystore/keychain
    bool result = await SharedPreferencesHelper.containsKey(USER_TOKEN_KEY);
    return result;
  }

  @override
  Future<User> signInWithGoogle() async {
    // TODO: implement signInWithGoogle

    try {
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        await _firebaseAuth.signInWithCredential(credential);
        FirebaseUser firebaseUser = await _firebaseAuth.currentUser();

        return await signInWithSocial(
            "google", firebaseUser, googleAuth.accessToken);
      }
    } catch (e) {
      print(e);
    }

    return null;
  }

  @override
  Future<User> signInWithFacebook() async {
    // TODO: implement signInWithFacebook
    _facebooklogin.loginBehavior = FacebookLoginBehavior.nativeWithFallback;
    final result = await _facebooklogin.logIn(['email']);
    if (result.status == FacebookLoginStatus.loggedIn) {
      final credential = FacebookAuthProvider.getCredential(
        accessToken: result.accessToken.token,
      );
      await _firebaseAuth.signInWithCredential(credential);
      FirebaseUser firebaseUser = await _firebaseAuth.currentUser();

      return await signInWithSocial(
          "facebook", firebaseUser, result.accessToken.token);
    }
    return null;
  }

  Future<User> signInWithSocial(
      String provivder, FirebaseUser user, String token) async {
    String url = Endpoint.URL_POST_LOGIN_SOCAIL + '/$provivder';

    String email;
    String photoUrl;
    if (provivder == "facebook") {
      if (user.email == null) {
        final graphResponse = await http.get('https://graph.facebook.com/v2'
            '.12/me?fields=name,birthday,first_name,last_name,email,picture.type(large)&access_token=$token');
        final profile = json.decode(graphResponse.body);
        email = profile['email'];
        photoUrl = profile['picture']['data']['url'];
      } else {
        email = user.email;
        photoUrl = user.photoUrl;
      }
    } else {
      if (user.email == null) {
        final graphResponse = await http.get(
            'https://www.googleapis.com/oauth2/v1/userinfo?access_token=$token');
        final profile = json.decode(graphResponse.body);
        email = profile['email'];
        photoUrl = user.photoUrl;
      } else {
        email = user.email;
        photoUrl = user.photoUrl;
      }
    }

    Dio dio = Dio();
    Response response;
    try {
      response = await dio
          .post(url,
              data: {
                "email": email,
                "name": user.displayName,
                "avatar": photoUrl,
                "phoneNumber": user.phoneNumber,
                "providerId": user.providerId,
                "accessToken": token
              },
              options: Options(
                  headers: {'Accept': 'application/json; charset=UTF-8'},
                  followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to social login from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to social login from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      String accessToken = data['accessToken'];

      await persistToken(accessToken);

      return User.fromJson(data['user']);
    } else {
      return null;
    }
  }

  @override
  Future<void> signOut() async {
    // TODO: implement signOut
    await SharedPreferencesHelper.removeValue(USER_TOKEN_KEY);

    return Future.wait([
      _firebaseAuth.signOut(),
      _googleSignIn.signOut(),
      _facebooklogin.logOut(),
    ]);
  }

  @override
  Future<ResultUserResponse> signUp(
      {String name,
      String email,
      String password,
      String confirmPassword}) async {
    String url = Endpoint.URL_POST_REGISTER;

    Dio dio = Dio();
    Response response;
    try {
      response = await dio
          .post(url,
              data: {
                "name": name,
                "email": email,
                "password": password,
                "c_password": confirmPassword,
              },
              options: Options(
                  headers: {'Accept': 'application/json; charset=UTF-8'},
                  followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to normal signup from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to normal signup from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ResultUserResponse.fromJson(data);
    } else {
      return ResultUserResponse(
          success: false, message: 'Fail to normal signup from the Internet');
    }
  }

  @override
  Future<ResultUserResponse> updateProfile(
      {String name, String email, String phoneNumber}) async {
    // TODO: implement updateProfile
    String url = Endpoint.URL_POST_UPDATE_PROFILE;
    String token =
        await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

    Dio dio = Dio();
    Response response;
    try {
      response = await dio
          .post(url,
              data: {
                "name": name,
                "email": email,
                "phoneNumber": phoneNumber,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to update profile from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to update profile from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ResultUserResponse.fromJson(data);
    } else {
      return ResultUserResponse(
          success: false, message: 'Cập nhật thông tin thất bại');
    }
  }

  @override
  Future<ResultUserResponse> updatePassword(
      {String oldPassword = "",
      String newPassword,
      String confirmNewPassword}) async {
    // TODO: implement updatePassword
    String url = Endpoint.URL_POST_UPDATE_PASSWORD;
    String token =
        await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

    Dio dio = Dio();
    Response response;
    try {
      response = await dio
          .post(url,
              data: {
                "oldPassword": oldPassword,
                "newPassword": newPassword,
                "confirmNewPassword": confirmNewPassword,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to update password from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to update password from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ResultUserResponse.fromJson(data);
    } else {
      return ResultUserResponse(
          success: false, message: 'Cập nhật password thất bại');
    }
  }
}
