import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'dart:async';

import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class CartRepository {
  Future<Cart> getCart({List<int> listCartItemIdSelecteds});
  Future<CartResponse> addToCart(
      {@required int productId, @required int quantity});
  Future<CartResponse> updateQuantityCartItem(
      {@required int productId, @required int quantity});
  Future<bool> removeCartItem({@required int productId});
}

class CartRepositoryImpl extends CartRepository {
  @override
  Future<Cart> getCart({List<int> listCartItemIdSelecteds}) async {
    Options options = Options(headers: {
      'Accept': 'application/json; charset=UTF-8',
    }, followRedirects: false);

    String token =
        await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

    if (token != null) {
      options.headers.addAll({
        'Authorization': "Bearer $token",
      });
    }

    String cartToken =
        await SharedPreferencesHelper.getStringValuesSF(CART_TOKEN_KEY);
    if (cartToken != null) {
      options.headers.addAll({
        CART_TOKEN_HEADER: cartToken,
      });
    }

    Dio dio = Dio();
    Response response;

    Map<String, dynamic> queryParameters = {};

    if (listCartItemIdSelecteds != null) {
      queryParameters = {
        "listCartItemIdSelecteds": listCartItemIdSelecteds,
      };
    }

    try {
      response = await dio
          .get(Endpoint.URL_GET_CART,
              options: options, queryParameters: queryParameters)
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get cart from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get cart from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      if (token != null) {
        await SharedPreferencesHelper.removeValue(CART_TOKEN_KEY);
      }
      return Cart.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<CartResponse> addToCart(
      {@required int productId, @required int quantity}) async {
    // TODO: implement addToCart
    String url = Endpoint.URL_POST_ADD_TO_CART;

    Dio dio = Dio();
    Response response;
    try {
      Options options = Options(headers: {
        'Accept': 'application/json; charset=UTF-8',
      }, followRedirects: false);

      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

      if (token != null) {
        options.headers.addAll({
          'Authorization': "Bearer $token",
        });
      } else {
        String cartToken =
            await SharedPreferencesHelper.getStringValuesSF(CART_TOKEN_KEY);
        if (cartToken != null) {
          options.headers.addAll({
            CART_TOKEN_HEADER: cartToken,
          });
        }
      }

      response = await dio
          .post(url,
              data: {
                'productId': productId,
                'quantity': quantity,
              },
              options: options)
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to add to cart from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to add to cart from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;
      if (response.data[CART_TOKEN_KEY] != null) {
        await SharedPreferencesHelper.addStringToSF(
            key: CART_TOKEN_KEY, value: response.data[CART_TOKEN_KEY]);
      }
      return CartResponse.fromJson(data["data"]);
    } else {
      return null;
    }
  }

  Future<CartResponse> updateQuantityCartItem(
      {@required int productId, @required int quantity}) async {
    String url = Endpoint.URL_POST_UPDATE_QUANTITY_CART_ITEM;

    Dio dio = Dio();
    Response response;
    try {
      Options options = Options(headers: {
        'Accept': 'application/json; charset=UTF-8',
      }, followRedirects: false);

      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

      if (token != null) {
        options.headers.addAll({
          'Authorization': "Bearer $token",
        });
      } else {
        String cartToken =
            await SharedPreferencesHelper.getStringValuesSF(CART_TOKEN_KEY);
        if (cartToken != null) {
          options.headers.addAll({
            CART_TOKEN_HEADER: cartToken,
          });
        }
      }

      response = await dio
          .post(url,
              data: {
                'productId': productId,
                'quantity': quantity,
              },
              options: options)
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to update quantity cart item from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to update quantity cart item from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;
      return CartResponse.fromJson(data);
    } else {
      return null;
    }
  }

  Future<bool> removeCartItem({@required int productId}) async {
    String url = Endpoint.URL_POST_REMOVE_CART_ITEM;

    Dio dio = Dio();
    Response response;
    try {
      Options options = Options(headers: {
        'Accept': 'application/json; charset=UTF-8',
      }, followRedirects: false);

      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);

      if (token != null) {
        options.headers.addAll({
          'Authorization': "Bearer $token",
        });
      } else {
        String cartToken =
            await SharedPreferencesHelper.getStringValuesSF(CART_TOKEN_KEY);
        if (cartToken != null) {
          options.headers.addAll({
            CART_TOKEN_HEADER: cartToken,
          });
        }
      }

      response = await dio
          .post(url,
              data: {
                'productId': productId,
              },
              options: options)
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to remove cart item from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to remove cart item from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }
}
