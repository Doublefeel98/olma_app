import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class OrderRepository {
  Future<CheckoutResponse> checkOut({
    @required List<int> listCartItemIds,
    @required int deliveryAddressId,
    @required String paymentMethod,
    @required String shippingMethod,
    @required int fee,
    @required int insuranceFee,
  });

  Future<OrderResponse> getListOrder();
}

class OrderRepositoryImpl extends OrderRepository {
  @override
  Future<CheckoutResponse> checkOut(
      {List<int> listCartItemIds,
      int deliveryAddressId,
      String paymentMethod,
      String shippingMethod,
      int fee,
      int insuranceFee = 0}) async {
    String url = Endpoint.URL_POST_CHECKOUT;

    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .post(url,
              data: {
                'listCartItemIds': listCartItemIds,
                'deliveryAddressId': deliveryAddressId,
                'paymentMethod': paymentMethod,
                'shippingMethod': shippingMethod,
                'fee': fee,
                'insuranceFee': insuranceFee
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to check out order from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to check out order from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;
      return CheckoutResponse.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<OrderResponse> getListOrder() async {
    // TODO: implement getListOrder
    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .get(Endpoint.URL_GET_LIST_ORDER,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list order from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list order from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return OrderResponse.fromJson(data);
    } else {
      return null;
    }
  }
}
