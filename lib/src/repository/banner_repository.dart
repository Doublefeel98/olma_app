import 'package:dio/dio.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'dart:async';

abstract class BannerRepository {
  Future<Banner> getBannerHome();
}

class BannerRepositoryImpl extends BannerRepository {
  @override
  Future<Banner> getBannerHome() async {
    Dio dio = Dio();
    Response response;
    try {
      response = await dio
          .get(Endpoint.URL_GET_BANNER,
          options: Options(headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json; charset=UTF-8',
          }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get banner home from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get banner home from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return Banner.fromJson(data["mainBanner"]);
    } else {
      return null;
    }
  }
}
