import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class DeliveryAddressRepository {
  Future<List<DeliveryAddress>> getList();

  Future<bool> addDeliveryAddress(
      {@required String name,
      @required String phone,
      @required String cityId,
      @required String cityName,
      @required String districtId,
      @required String districtName,
      @required String wardId,
      @required String wardName,
      @required String address,
      bool isDefault = false});

  Future<bool> updateDeliveryAddress(int deliveryAddressId,
      {@required String name,
      @required String phone,
      @required String cityId,
      @required String cityName,
      @required String districtId,
      @required String districtName,
      @required String wardId,
      @required String wardName,
      @required String address,
      bool isDefault = false});

  Future<bool> updateDeliveryAddressDefault(int deliveryAddressId);
  Future<bool> deleteDeliveryAddress(int deliveryAddressId);

  Future<List<City>> getListCites();
  Future<List<District>> getListDistricsByCityCode(String cityCode);
  Future<List<Ward>> getListWardByDistrictCode(String districtCode);
}

class DeliveryAddressRepositoryImpl extends DeliveryAddressRepository {
  Future<List<DeliveryAddress>> getList() async {
    Dio dio = Dio();
    Response response;

    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .get(Endpoint.URL_GET_LIST_DELIVERY_ADDRESS,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list deliver address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list deliver address from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<DeliveryAddress> listDeliveryAddress = new List<DeliveryAddress>();

      if (data != null) {
        data.forEach((v) {
          listDeliveryAddress.add(new DeliveryAddress.fromJson(v));
        });
      }
      return listDeliveryAddress;
    } else {
      return null;
    }
  }

  Future<bool> addDeliveryAddress(
      {@required String name,
      @required String phone,
      @required String cityId,
      @required String cityName,
      @required String districtId,
      @required String districtName,
      @required String wardId,
      @required String wardName,
      @required String address,
      bool isDefault = false}) async {
    // set up POST request arguments
    String url = Endpoint.URL_POST_ADD_DELIVERY_ADDRESS;

    Dio dio = Dio();
    Response response;
    isDefault = isDefault ?? false;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .post(url,
              data: {
                'name': name,
                'phone': phone,
                'cityId': cityId,
                'cityName': cityName,
                'districtId': districtId,
                'districtName': districtName,
                'wardId': wardId,
                'wardName': wardName,
                'address': address,
                'isDefault': isDefault,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to add delivery address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to add delivery address from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateDeliveryAddress(int deliveryAddressId,
      {@required String name,
      @required String phone,
      @required String cityId,
      @required String cityName,
      @required String districtId,
      @required String districtName,
      @required String wardId,
      @required String wardName,
      @required String address,
      bool isDefault = false}) async {
    String url =
        Endpoint.URL_PUT_UPDATE_DELIVERY_ADDRESS + '/$deliveryAddressId';

    Dio dio = Dio();
    Response response;
    isDefault = isDefault ?? false;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .put(url,
              data: {
                'name': name,
                'phone': phone,
                'cityId': cityId,
                'cityName': cityName,
                'districtId': districtId,
                'districtName': districtName,
                'wardId': wardId,
                'wardName': wardName,
                'address': address,
                'isDefault': isDefault,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to update delivery address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to update delivery address from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateDeliveryAddressDefault(int deliveryAddressId) async {
    String url = Endpoint.URL_PUT_UPDATE_DELIVERY_ADDRESS_DEFAULT +
        '/$deliveryAddressId';

    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .put(url,
              data: {},
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception(
            'Fail to update delivery address default from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception(
          'Fail to update delivery address default from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteDeliveryAddress(int deliveryAddressId) async {
    String url =
        Endpoint.URL_DELETE_REMOVE_DELIVERY_ADDRESS + '/$deliveryAddressId';

    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .put(url,
              data: {},
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).then((value) {
        //return value.data['accessToken'];
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to delete delivery address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to delete delivery address from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<List<City>> getListCites() async {
    // TODO: implement getListCites
    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_LIST_ADDRESS_CITY,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list deliver address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list deliver address from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<City> listCites = new List<City>();

      if (data != null) {
        data.values.toList().forEach((element) {
          listCites.add(new City.fromJson(element));
        });
        // data.forEach((v) {
        //   listCites.add(new City.fromJson(v));
        // });
      }
      return listCites;
    } else {
      return null;
    }
  }

  @override
  Future<List<District>> getListDistricsByCityCode(String cityCode) async {
    // TODO: implement getListDistricsByCityCode
    Dio dio = Dio();
    Response response;

    String url = Endpoint.URL_GET_LIST_ADDRESS_DISTRICS
        .replaceFirst("cityCode", cityCode);

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list deliver address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list deliver address from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<District> listDistrics = new List<District>();

      if (data != null) {
        data.values.forEach((v) {
          listDistrics.add(new District.fromJson(v));
        });
      }
      return listDistrics;
    } else {
      return null;
    }
  }

  @override
  Future<List<Ward>> getListWardByDistrictCode(String districtCode) async {
    Dio dio = Dio();
    Response response;

    String url = Endpoint.URL_GET_LIST_ADDRESS_WARD
        .replaceFirst("districtCode", districtCode);

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list deliver address from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list deliver address from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<Ward> listWards = new List<Ward>();

      if (data != null) {
        data.values.forEach((v) {
          listWards.add(new Ward.fromJson(v));
        });
      }
      return listWards;
    } else {
      return null;
    }
  }
}
