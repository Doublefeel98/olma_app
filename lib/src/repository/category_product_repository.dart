import 'dart:async';

import 'package:dio/dio.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/models/category_product.dart';

abstract class CategoryProductRepository {
  Future<List<CategoryProduct>> getTree();

  Future<List<CategoryProduct>> getListHome();
}

class CategoryProductRepositoryImpl extends CategoryProductRepository {
  @override
  Future<List<CategoryProduct>> getTree() async {
    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_TREE_CATEGORY,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get tree category product from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get tree category product from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<CategoryProduct> listTree = new List<CategoryProduct>();

      if (data != null) {
        data.forEach((v) {
          listTree.add(new CategoryProduct.fromJson(v));
        });
      }
      return listTree;
    } else {
      return null;
    }
  }

  @override
  Future<List<CategoryProduct>> getListHome() async {
    // TODO: implement getListHome
    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_TREE_CATEGORY,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get tree category product from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get tree category product from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<CategoryProduct> listCategoryHome = new List<CategoryProduct>();

      if (data != null) {
        data.forEach((v) {
          listCategoryHome.add(new CategoryProduct.fromJson(v));
        });
      }
      return listCategoryHome;
    } else {
      return null;
    }
  }
}
