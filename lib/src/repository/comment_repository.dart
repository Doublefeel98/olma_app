import 'package:flutter/cupertino.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:dio/dio.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'dart:async';
import 'package:olmaapp/src/models/comment.dart';
import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class CommentRepository {
  Future<ListComment> getListCommentProduct(int id);
  Future<bool> postComment({
    @required int productId,
    @required String content,
    @required int rating,
  });
}

class CommentRepositoryImpl extends CommentRepository {
  @override
  Future<ListComment> getListCommentProduct(int id) async {
    // TODO: implement getListCommentProduct

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_LIST_COMMENT_PRODUCT + '$id',
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get list comment from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get list comment from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ListComment.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<bool> postComment({int productId, String content, int rating}) async {
    // TODO: implement postComment
    String url = Endpoint.URL_POST_COMMENT + '/$productId';

    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .post(url,
              data: {
                'content': content,
                'rating': rating,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to post comment from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to post comment from the Internet');
    }
    if (response != null) {
      return true;
    } else {
      return false;
    }
  }
}
