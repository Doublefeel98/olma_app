import 'package:dio/dio.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'dart:async';
import 'dart:convert';
import 'package:olmaapp/src/models/deal.dart';

abstract class DealRepository {
  Future<List<DealForHome>> getDealForHome();
  Future<DealForDetail> getDealForDetail(int dealId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1});
}

class DealRepositoryImpl extends DealRepository {
  @override
  Future<DealForDetail> getDealForDetail(int dealId,
      {int limit = 16,
      int lowestPrice = 0,
      int highestPrice = 0,
      int rating = 0,
      String order = "",
      int page = 1}) async {
    var url = Endpoint.URL_GET_LIST_DEAL_FOR_DETAIL +
        '?limit=$limit&lowestPrice=$lowestPrice&highestPrice=$highestPrice&rating=$rating&order=$order&page=$page&dealId=$dealId';

    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(url,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get deal detail from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get deal detail from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return DealForDetail.fromJson(data);
    } else {
      return null;
    }
  }

  @override
  Future<List<DealForHome>> getDealForHome() async {
    Dio dio = Dio();
    Response response;

    try {
      response = await dio
          .get(Endpoint.URL_GET_LIST_DEAL_FOR_HOME,
              options: Options(headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json; charset=UTF-8',
              }, followRedirects: false))
          .whenComplete(() {})
          .catchError((e) {
        print(e.response);
        throw Exception('Fail to get deal for home from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to get deal for home from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      List<DealForHome> dealForHomes = new List<DealForHome>();
      if (data != null) {
        data.forEach((v) {
          dealForHomes.add(new DealForHome.fromJson(v));
        });
      }

      return dealForHomes;
    } else {
      return null;
    }
  }
}
