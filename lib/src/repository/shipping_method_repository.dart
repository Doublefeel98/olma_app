import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/endpoint.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/shipping_method.dart';
import 'package:olmaapp/src/utils/shared_preferences_helper.dart';

abstract class ShippingMethodRepository {
  Future<ShippingMethod> calculateFee({
    @required String shippingMethod,
    @required int deliveryAddressId,
    @required List<int> listCartIds,
  });
}

class ShippingMethodRepositoryImpl extends ShippingMethodRepository {
  @override
  Future<ShippingMethod> calculateFee(
      {String shippingMethod,
      int deliveryAddressId,
      List<int> listCartIds}) async {
    String url = Endpoint.URL_POST_CALCULATE_FEE;

    Dio dio = Dio();
    Response response;
    try {
      String token =
          await SharedPreferencesHelper.getStringValuesSF(USER_TOKEN_KEY);
      response = await dio
          .get(url,
              queryParameters: {
                "shippingMethod": shippingMethod,
                "deliveryAddressId": deliveryAddressId,
                "listCartIds": listCartIds,
              },
              options: Options(headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Authorization': "Bearer $token",
              }, followRedirects: false))
          .whenComplete(() {
        print("complete:");
        //return '';
      }).catchError((e) {
        print(e.response);
        throw Exception('Fail to calculate fee from the Internet');
      });
    } catch (e) {
      print(e);
      throw Exception('Fail to calculate fee from the Internet');
    }
    if (response != null) {
      dynamic data = response.data;

      return ShippingMethod.fromJson(data);
    } else {
      return null;
    }
  }
}
