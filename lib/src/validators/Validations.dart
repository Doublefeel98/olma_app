class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  static final RegExp _passwordRegExp = RegExp(
    r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$',
  );

  static final RegExp _phoneRegExp = RegExp(
    r'(^(?:[+0]9)?[0-9]{10,11}$)',
  );

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    //return _passwordRegExp.hasMatch(password);
    return password.length >= 6;
  }

  static isValidPhone(String phone) {
    //return _passwordRegExp.hasMatch(password);
    return _phoneRegExp.hasMatch(phone);
  }

  static isValidConfirmPassword(String password, String confirmPassword) {
    return confirmPassword == password && isValidPassword(confirmPassword);
  }

  static isValidContentComment(String content) {
    return content.length > 25;
  }
}
