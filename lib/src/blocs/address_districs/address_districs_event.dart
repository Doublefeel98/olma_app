import 'package:equatable/equatable.dart';

abstract class AddressDistricsEvent extends Equatable {}

class FetchAddressDistricsEvent extends AddressDistricsEvent {
  final String cityCode;

  FetchAddressDistricsEvent(this.cityCode);
  @override
  // TODO: implement props
  List<Object> get props => null;
}
