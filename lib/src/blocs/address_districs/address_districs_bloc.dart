import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'bloc.dart';

class AddressDistricsBloc
    extends Bloc<AddressDistricsEvent, AddressDistricsState> {
  DeliveryAddressRepository deliveryAddressRepository;

  AddressDistricsBloc({@required this.deliveryAddressRepository});

  @override
  // TODO: implement initialState
  AddressDistricsState get initialState => AddressDistricsInitialState();

  @override
  Stream<AddressDistricsState> mapEventToState(
      AddressDistricsEvent event) async* {
    if (event is FetchAddressDistricsEvent) {
      yield AddressDistricsLoadingState();
      try {
        List<District> listDistrics = await deliveryAddressRepository
            .getListDistricsByCityCode(event.cityCode);
        yield AddressDistricsLoadedState(listDistrics: listDistrics);
      } catch (e) {
        yield AddressDistricsErrorState(message: e.toString());
      }
    }
  }
}
