import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class AddressDistricsState extends Equatable {}

class AddressDistricsInitialState extends AddressDistricsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressDistricsLoadingState extends AddressDistricsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressDistricsLoadedState extends AddressDistricsState {
  List<District> listDistrics;

  AddressDistricsLoadedState({@required this.listDistrics});

  @override
  // TODO: implement props
  List<Object> get props => [listDistrics];
}

class AddressDistricsErrorState extends AddressDistricsState {
  String message;

  AddressDistricsErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
