import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'bloc.dart';

class AddressCitesBloc extends Bloc<AddressCitesEvent, AddressCitesState> {
  DeliveryAddressRepository deliveryAddressRepository;

  AddressCitesBloc({@required this.deliveryAddressRepository});

  @override
  // TODO: implement initialState
  AddressCitesState get initialState => AddressCitesInitialState();

  @override
  Stream<AddressCitesState> mapEventToState(AddressCitesEvent event) async* {
    if (event is FetchAddressCitesEvent) {
      yield AddressCitesLoadingState();
      try {
        List<City> listCities = await deliveryAddressRepository.getListCites();
        yield AddressCitesLoadedState(listCities: listCities);
      } catch (e) {
        yield AddressCitesErrorState(message: e.toString());
      }
    }
  }
}
