import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class AddressCitesState extends Equatable {}

class AddressCitesInitialState extends AddressCitesState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressCitesLoadingState extends AddressCitesState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressCitesLoadedState extends AddressCitesState {
  List<City> listCities;

  AddressCitesLoadedState({@required this.listCities});

  @override
  // TODO: implement props
  List<Object> get props => [listCities];
}

class AddressCitesErrorState extends AddressCitesState {
  String message;

  AddressCitesErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
