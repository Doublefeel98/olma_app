import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/repository/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final String messageError = "Không tìm thấy tài khoản";
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => AuthenticationInitial();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStarted) {
      yield* _mapAuthenticationStartedToState();
    } else if (event is AuthenticationLoggedIn) {
      yield* _mapAuthenticationLoggedInToState();
    } else if (event is AuthenticationLoggedOut) {
      yield* _mapAuthenticationLoggedOutToState();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationStartedToState() async* {
    yield AuthenticationInProgress();
    final isSignedIn = await _userRepository.hasToken();
    if (isSignedIn) {
      final user = await _userRepository.getCurrentUser();
      if (user == null) {
        yield AuthenticationFailure(message: messageError);
      } else {
        yield AuthenticationSuccess(user);
      }
    } else {
      yield AuthenticationFailure(message: messageError);
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedInToState() async* {
    yield AuthenticationInProgress();
    final user = await _userRepository.getCurrentUser();
    if (user == null) {
      yield AuthenticationFailure(message: messageError);
    } else {
      yield AuthenticationSuccess(user);
    }
    yield AuthenticationSuccess(user);
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedOutToState() async* {
    yield AuthenticationFailure(message: messageError);
    _userRepository.signOut();
  }
}
