import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/user.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationSuccess extends AuthenticationState {
  final User user;

  AuthenticationSuccess(this.user);

  @override
  List<Object> get props => [User];

  @override
  String toString() => 'Authenticated { user: ${user.name} }';
}

class AuthenticationFailure extends AuthenticationState {
  String message;

  AuthenticationFailure({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}

class AuthenticationInProgress extends AuthenticationState {}
