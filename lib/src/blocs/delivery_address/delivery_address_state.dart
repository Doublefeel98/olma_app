import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';

abstract class DeliveryAddressState extends Equatable {}

class DeliveryAddressInitialState extends DeliveryAddressState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DeliveryAddressLoadingState extends DeliveryAddressState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DeliveryAddressLoadedState extends DeliveryAddressState {
  List<DeliveryAddress> listDeliveryAddress;

  DeliveryAddressLoadedState({@required this.listDeliveryAddress});

  @override
  // TODO: implement props
  List<Object> get props => [listDeliveryAddress];
}

class DeliveryAddressErrorState extends DeliveryAddressState {
  String message;

  DeliveryAddressErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
