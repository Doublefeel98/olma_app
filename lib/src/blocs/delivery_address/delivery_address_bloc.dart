import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';

import 'bloc.dart';

class DeliveryAddressBloc
    extends Bloc<DeliveryAddressEvent, DeliveryAddressState> {
  DeliveryAddressRepository deliveryAddressRepository;

  DeliveryAddressBloc({@required this.deliveryAddressRepository});

  @override
  // TODO: implement initialState
  DeliveryAddressState get initialState => DeliveryAddressInitialState();

  @override
  Stream<DeliveryAddressState> mapEventToState(
      DeliveryAddressEvent event) async* {
    if (event is FetchDeliveryAddressEvent) {
      yield DeliveryAddressLoadingState();
      try {
        List<DeliveryAddress> listDeliveryAddress =
            await deliveryAddressRepository.getList();
        yield DeliveryAddressLoadedState(
            listDeliveryAddress: listDeliveryAddress);
      } catch (e) {
        yield DeliveryAddressErrorState(message: e.toString());
      }
    }
  }
}
