import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/models/shipping_method.dart';

abstract class ShippingMethodState extends Equatable {}

class ShippingMethodInitialState extends ShippingMethodState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ShippingMethodLoadingState extends ShippingMethodState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ShippingMethodLoadedState extends ShippingMethodState {
  ShippingMethod shippingMethod;

  ShippingMethodLoadedState({@required this.shippingMethod});

  @override
  // TODO: implement props
  List<Object> get props => [shippingMethod];
}

class ShippingMethodErrorState extends ShippingMethodState {
  String message;

  ShippingMethodErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
