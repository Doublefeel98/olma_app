import 'package:equatable/equatable.dart';

abstract class ShippingMethodEvent extends Equatable {}

class FetchShippingMethodEvent extends ShippingMethodEvent {
  final int deliveryAddressId;
  final List<int> listCartIds;

  FetchShippingMethodEvent(this.deliveryAddressId, this.listCartIds);
  @override
  // TODO: implement props
  List<Object> get props => [deliveryAddressId, listCartIds];
}
