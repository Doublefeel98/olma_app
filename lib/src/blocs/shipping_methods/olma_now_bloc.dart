import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/shipping_method.dart';
import 'package:olmaapp/src/repository/shipping_method_repository.dart';

import 'bloc.dart';

class OlmaNowBloc extends Bloc<ShippingMethodEvent, ShippingMethodState> {
  ShippingMethodRepository shippingMethodRepository;

  OlmaNowBloc({@required this.shippingMethodRepository});

  @override
  // TODO: implement initialState
  ShippingMethodState get initialState => ShippingMethodInitialState();

  @override
  Stream<ShippingMethodState> mapEventToState(
      ShippingMethodEvent event) async* {
    if (event is FetchShippingMethodEvent) {
      yield ShippingMethodLoadingState();
      try {
        ShippingMethod shippingMethod =
            await shippingMethodRepository.calculateFee(
                shippingMethod: ShippingMethods.OlmaNow,
                deliveryAddressId: event.deliveryAddressId,
                listCartIds: event.listCartIds);
        yield ShippingMethodLoadedState(shippingMethod: shippingMethod);
      } catch (e) {
        yield ShippingMethodErrorState(message: e.toString());
      }
    }
  }
}
