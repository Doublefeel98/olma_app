import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class CartState extends Equatable {}

class CartInitialState extends CartState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CartLoadingState extends CartState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CartLoadedState extends CartState {
  Cart cart;

  CartLoadedState({@required this.cart});

  @override
  // TODO: implement props
  List<Object> get props => [cart];
}

class CartErrorState extends CartState {
  String message;

  CartErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}

class CartClearedState extends CartState {
  @override
  // TODO: implement props
  List<Object> get props => [null];
}
