import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'bloc.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartRepository cartRepository;

  CartBloc({@required this.cartRepository});

  @override
  // TODO: implement initialState
  CartState get initialState => CartInitialState();

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is FetchCartEvent) {
      yield CartLoadingState();
      try {
        Cart cart = await cartRepository.getCart(
            listCartItemIdSelecteds: event.listCartItemIdSelecteds);
        yield CartLoadedState(cart: cart);
      } catch (e) {
        yield CartErrorState(message: e.toString());
      }
    } else if (event is ClearCartEvent) {
      yield CartClearedState();
    }
  }
}
