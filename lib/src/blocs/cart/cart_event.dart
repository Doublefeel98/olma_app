import 'package:equatable/equatable.dart';

abstract class CartEvent extends Equatable {}

class FetchCartEvent extends CartEvent {
  final List<int> listCartItemIdSelecteds;

  FetchCartEvent({this.listCartItemIdSelecteds});
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class ClearCartEvent extends CartEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}
