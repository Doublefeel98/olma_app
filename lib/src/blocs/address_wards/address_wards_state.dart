import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class AddressWardsState extends Equatable {}

class AddressWardsInitialState extends AddressWardsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressWardsLoadingState extends AddressWardsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddressWardsLoadedState extends AddressWardsState {
  List<Ward> listWards;

  AddressWardsLoadedState({@required this.listWards});

  @override
  // TODO: implement props
  List<Object> get props => [listWards];
}

class AddressWardsErrorState extends AddressWardsState {
  String message;

  AddressWardsErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
