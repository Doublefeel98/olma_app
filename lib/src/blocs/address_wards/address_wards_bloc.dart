import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'bloc.dart';

class AddressWardsBloc extends Bloc<AddressWardsEvent, AddressWardsState> {
  DeliveryAddressRepository deliveryAddressRepository;

  AddressWardsBloc({@required this.deliveryAddressRepository});

  @override
  // TODO: implement initialState
  AddressWardsState get initialState => AddressWardsInitialState();

  @override
  Stream<AddressWardsState> mapEventToState(AddressWardsEvent event) async* {
    if (event is FetchAddressWardsEvent) {
      yield AddressWardsLoadingState();
      try {
        List<Ward> listWards = await deliveryAddressRepository
            .getListWardByDistrictCode(event.districtCode);
        yield AddressWardsLoadedState(listWards: listWards);
      } catch (e) {
        yield AddressWardsErrorState(message: e.toString());
      }
    }
  }
}
