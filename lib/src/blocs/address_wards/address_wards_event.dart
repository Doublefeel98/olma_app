import 'package:equatable/equatable.dart';

abstract class AddressWardsEvent extends Equatable {}

class FetchAddressWardsEvent extends AddressWardsEvent {
  final String districtCode;

  FetchAddressWardsEvent(this.districtCode);
  @override
  // TODO: implement props
  List<Object> get props => null;
}
