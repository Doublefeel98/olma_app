import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/plugins/count_down.dart';
import 'package:olmaapp/src/ui/plugins/sale_progress_bar.dart';
import 'package:olmaapp/src/ui/screens/product_detail/product_detail_screen.dart';
import 'package:olmaapp/src/ui/utils/money_formatter.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

enum ItemProductType {
  NORMAL,
  DEAL,
}

class ItemProduct extends StatefulWidget {
  final ProductItem productItem;
  final ItemProductType type;

  ItemProduct({this.productItem, this.type = ItemProductType.NORMAL});

  @override
  _ItemProductState createState() =>
      _ItemProductState(productItem: this.productItem, type: this.type);
}

class _ItemProductState extends State<ItemProduct> {
  final ProductItem productItem;
  final ItemProductType type;

  _ItemProductState({this.productItem, this.type});

  Widget buildProductPrice(int price, double fPriceNewProduct) {
    return Text(
      MoneyFormatter(amount: price).toString(),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: fPriceNewProduct,
        fontWeight: FontWeight.w600,
        color: ColorConstants.BLUE_PRIMARY_TEXT,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    double _pItemProduct;
    double _wItemProduct;
    double _fItemProduct;
    double _fPriceNewProduct;
    double _fPriceOldProduct;
    double _hTitleItemProduct;
    double _mrbChildItemProduct;
    double _fStatusItemProduct;
    switch (this.type) {
      case ItemProductType.NORMAL:
        _pItemProduct = sizeHelper.rW(2.5);
        _wItemProduct = sizeHelper.rW(50.0);
        _fItemProduct = sizeHelper.rW(4);
        _fPriceNewProduct = sizeHelper.rW(4.5);
        _fPriceOldProduct = sizeHelper.rW(3.5);
        _hTitleItemProduct = sizeHelper.rW(11);
        _mrbChildItemProduct = sizeHelper.rW(1);
        _fStatusItemProduct = sizeHelper.rW(3.8);
        break;
      case ItemProductType.DEAL:
        _pItemProduct = sizeHelper.rW(2.5);
        _wItemProduct = sizeHelper.rW(40.0);
        _fItemProduct = sizeHelper.rW(3.7);
        _fPriceNewProduct = sizeHelper.rW(4.2);
        _fPriceOldProduct = sizeHelper.rW(3.2);
        _hTitleItemProduct = sizeHelper.rW(9.5);
        _mrbChildItemProduct = sizeHelper.rW(0.8);
        _fStatusItemProduct = sizeHelper.rW(3.5);
        break;
      default:
    }

    String statusStr = '';
    Color colorStatusStr;

    switch (this.productItem.stockStatus) {
      case 'coming_soon':
        statusStr = 'Sắp ra mắt';
        colorStatusStr = ColorConstants.COMMING_SOON;
        break;
      case 'in_stock':
        statusStr = 'Hàng có sẵn';
        colorStatusStr = ColorConstants.IN_STOCK;
        break;
      case 'out_of_stock':
        statusStr = 'Đã hết hàng';
        colorStatusStr = ColorConstants.OUT_OF_STOCK;
        break;
      default:
        colorStatusStr = ColorConstants.OUT_OF_STOCK;
    }

    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                ProductDetailScreen(productId: productItem.id),
          ),
        );
      },
      child: FractionallySizedBox(
        widthFactor: this.type == ItemProductType.NORMAL ? 0.5 : null,
        child: Container(
          padding: EdgeInsets.all(_pItemProduct),
          width: this.type == ItemProductType.NORMAL
              ? double.infinity
              : _wItemProduct,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 5.0),
                child: AspectRatio(
                  aspectRatio: 3 / 2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                    child: CachedNetworkImage(
                      imageUrl: this.productItem.poster,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              Container(
                height: _hTitleItemProduct,
                child: Text(
                  this.productItem.name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: _fItemProduct,
                    fontWeight: FontWeight.w500,
                    color: ColorConstants.ITEM_PRODUCT_TEXT,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: _mrbChildItemProduct),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: this.productItem.productDiscount != null
                      ? <Widget>[
                          buildProductPrice(
                              this.productItem.productDiscount.promotionPrice,
                              _fPriceNewProduct),
                          Container(
                            child: Text(
                              MoneyFormatter(amount: this.productItem.price)
                                  .toString(),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: _fPriceOldProduct,
                                decoration: TextDecoration.lineThrough,
                                color: ColorConstants.ITEM_CATEGORY_HOME_TEXT,
                              ),
                            ),
                          ),
                        ]
                      : <Widget>[
                          buildProductPrice(
                              this.productItem.price, _fPriceNewProduct),
                        ],
                ),
              ),
              this.productItem.productDiscount == null
                  ? Container(
                      margin: EdgeInsets.only(bottom: _mrbChildItemProduct),
                      child: Row(
                        children: <Widget>[
                          Text(
                            statusStr,
                            style: TextStyle(
                              color: colorStatusStr,
                              fontSize: _fStatusItemProduct,
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(),
              this.productItem.productDiscount != null
                  ? Container(
                      margin: EdgeInsets.only(bottom: _mrbChildItemProduct),
                      child: Row(
                        children: <Widget>[
                          CountDown(
                            dueTimeStr: this
                                .productItem
                                .productDiscount
                                .endTime
                                .toString(),
                          )
                        ],
                      ),
                    )
                  : Container(),
              this.productItem.productDiscount != null
                  ? Container(
                      child: SaleProgressBar(
                        quantitySale: productItem.productDiscount.quantity,
                        quantity: productItem.productDiscount.quantitySold,
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
