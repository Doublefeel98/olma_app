import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'package:olmaapp/src/ui/widgets/btn_add_to_cart/btn_add_to_cart_ui.dart';

import 'bloc/add_to_cart_bloc.dart';

class BtnAddToCart extends StatelessWidget {
  final Product _product;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  BtnAddToCart(
      {Key key,
      @required Product product,
      @required GlobalKey<ScaffoldState> scaffoldKey})
      : assert(product != null && scaffoldKey != null),
        _product = product,
        _scaffoldKey = scaffoldKey,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _cartRepository = RepositoryProvider.of<CartRepository>(context);
    return BlocProvider<AddToCartBloc>(
      create: (context) => AddToCartBloc(cartRepository: _cartRepository),
      child: BtnAddToCartUI(product: _product, scaffoldKey: _scaffoldKey),
    );
  }
}
