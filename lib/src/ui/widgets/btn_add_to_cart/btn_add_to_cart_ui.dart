import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/cart_screen.dart';
import 'package:olmaapp/src/ui/shares/number_button.dart';
import 'package:olmaapp/src/ui/utils/money_formatter.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/btn_add_to_cart/bloc/bloc.dart';

class BtnAddToCartUI extends StatefulWidget {
  final Product _product;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  BtnAddToCartUI(
      {Key key,
      @required Product product,
      @required GlobalKey<ScaffoldState> scaffoldKey})
      : assert(product != null && scaffoldKey != null),
        _product = product,
        _scaffoldKey = scaffoldKey,
        super(key: key);
  @override
  _BtnAddToCartUIState createState() => _BtnAddToCartUIState();
}

class _BtnAddToCartUIState extends State<BtnAddToCartUI> {
  Product get _product => widget._product;
  GlobalKey<ScaffoldState> get _scaffoldKey => widget._scaffoldKey;

  int _countToBuy = 1;

  AddToCartBloc _addToCartBloc;
  CartBloc _cartBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addToCartBloc = BlocProvider.of<AddToCartBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<AddToCartBloc, AddToCartState>(
      listener: (context, state) {
        if (state is AddToCartErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        } else if (state is AddToCartLoadingState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang thêm sản phẩm vào giỏ hàng...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        } else if (state is AddToCartResponseState) {
          _scaffoldKey.currentState..hideCurrentSnackBar();
          _cartBloc.add(FetchCartEvent(
              listCartItemIdSelecteds: [state.cartResponse.data.id]));
          showDialog(
            context: context,
            builder: (_) => CupertinoAlertDialog(
              title: Text(
                "Thông báo",
                style: TextStyle(
                  fontSize: sizeHelper.rW(5),
                ),
              ),
              content: Column(
                children: <Widget>[
                  SizedBox(
                    height: sizeHelper.rW(3),
                  ),
                  Icon(
                    Icons.add_shopping_cart,
                    size: sizeHelper.rW(8),
                    color: Colors.green,
                  ),
                  SizedBox(
                    height: sizeHelper.rW(3),
                  ),
                  Text(
                    state.cartResponse.message,
                    style: TextStyle(
                      fontSize: sizeHelper.rW(4),
                    ),
                  ),
                ],
              ),
              actions: [
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => CartScreen(),
                    ));
                  },
                  child: Text(
                    "OK",
                    style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
          );
        }
      },
      child: Container(
        // height: double.infinity, //remove option _product
        height: sizeHelper.rH(25),
        child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  Flexible(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(sizeHelper.rW(2)),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.black38,
                            width: 0.5,
                          ),
                        ),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(sizeHelper.rW(1)),
                              child: CachedNetworkImage(
                                imageUrl: _product.poster,
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: sizeHelper.rW(2),
                          ),
                          Flexible(
                            flex: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _product.name,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  _product.productDiscount != null
                                      ? MoneyFormatter(
                                              amount: _product.productDiscount
                                                  .promotionPrice)
                                          .toString()
                                      : MoneyFormatter(amount: _product.price)
                                          .toString(),
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(5),
                                    fontWeight: FontWeight.w500,
                                    color: ColorConstants.BLUE_PRIMARY_TEXT,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(sizeHelper.rW(2)),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: Colors.black38,
                          width: 0.5,
                        ),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Số lượng (còn ${_product.quantity} sản phẩm)",
                          style: TextStyle(
                            fontSize: sizeHelper.rW(4),
                            color: ColorConstants.GREY_TEXT,
                          ),
                        ),
                        NumberButton(
                            onChange: (value) {
                              setState(() {
                                _countToBuy = value;
                              });
                            },
                            minValue: 1,
                            maxValue: _product.quantity),
                      ],
                    ),
                  ),
                  Container(
                    height: sizeHelper.rW(12),
                    width: double.infinity,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero,
                      ),
                      onPressed: () {
                        _addToCartBloc.add(PostAddToCartEvent(
                            productId: _product.id, quantity: _countToBuy));
                      },
                      child: Text('MUA NGAY',
                          style: TextStyle(
                              fontSize: sizeHelper.rW(4.2),
                              fontWeight: FontWeight.w400)),
                      color: ColorConstants.BLUE_PRIMARY,
                      textColor: Colors.white,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
