import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddToCartEvent extends Equatable {}

class PostAddToCartEvent extends AddToCartEvent {
  final int productId;
  final int quantity;

  PostAddToCartEvent({@required this.productId, @required this.quantity});
  @override
  // TODO: implement props
  List<Object> get props => null;
}
