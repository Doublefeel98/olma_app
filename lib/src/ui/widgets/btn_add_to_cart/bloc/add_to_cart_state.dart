import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class AddToCartState extends Equatable {}

class AddToCartInitialState extends AddToCartState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddToCartLoadingState extends AddToCartState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddToCartResponseState extends AddToCartState {
  CartResponse cartResponse;

  AddToCartResponseState({@required this.cartResponse});

  @override
  // TODO: implement props
  List<Object> get props => [cartResponse];
}

class AddToCartErrorState extends AddToCartState {
  String message;

  AddToCartErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
