import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'bloc.dart';

class AddToCartBloc extends Bloc<AddToCartEvent, AddToCartState> {
  CartRepository cartRepository;

  AddToCartBloc({@required this.cartRepository});

  @override
  // TODO: implement initialState
  AddToCartState get initialState => AddToCartInitialState();

  @override
  Stream<AddToCartState> mapEventToState(AddToCartEvent event) async* {
    if (event is PostAddToCartEvent) {
      yield AddToCartLoadingState();
      try {
        CartResponse cartResponse = await cartRepository.addToCart(
            productId: event.productId, quantity: event.quantity);

        if (cartResponse.success) {
          yield AddToCartResponseState(cartResponse: cartResponse);
        } else {
          yield AddToCartErrorState(message: cartResponse.toString());
        }
      } catch (e) {
        yield AddToCartErrorState(message: e.toString());
      }
    }
  }
}
