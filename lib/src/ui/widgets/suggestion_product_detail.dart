import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/item_product.dart';

class SuggestionProductDetail extends StatefulWidget {
  final List<ProductItem> _listProducts;

  SuggestionProductDetail({Key key, @required List<ProductItem> listProducts})
      : assert(listProducts != null),
        _listProducts = listProducts,
        super(key: key);

  @override
  _SuggestionProductDetailState createState() =>
      _SuggestionProductDetailState();
}

class _SuggestionProductDetailState extends State<SuggestionProductDetail> {
  List<ProductItem> get _listProducts => widget._listProducts;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    double _hListSuggestion = sizeHelper.rW(53);

    List<Widget> listProductSuggestion(List<ProductItem> listProducts) {
      return listProducts
          .map((item) => ItemProduct(
                productItem: item,
                type: ItemProductType.DEAL,
              ))
          .toList();
    }

    return SectionWrapper(
      title: "Sản phẩm tương tự",
      showReadMore: false,
      child: Container(
        height: _hListSuggestion,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: listProductSuggestion(_listProducts),
        ),
      ),
    );
  }
}
