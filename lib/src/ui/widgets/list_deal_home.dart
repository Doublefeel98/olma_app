//import 'package:flutter/material.dart';
//import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
//import 'package:olmaapp/src/ui/utils/size_helper.dart';
//import 'package:olmaapp/src/ui/widgets/item_product.dart';
//
//class ListDealHome extends StatefulWidget {
//  @override
//  _ListDealHomeState createState() => _ListDealHomeState();
//}
//
//class _ListDealHomeState extends State<ListDealHome> {
//  @override
//  Widget build(BuildContext context) {
//    List<ItemDealModel> _listDealsModel = [
//      ItemDealModel(
//          id: 0,
//          title: "CHIẾN DỊCH BỀN VỮNG VỪA KHỞI ĐỘNG",
//          listProducts: [
//            ItemProductModel(
//                id: 0,
//                title:
//                    "Tương ớt Sạch VietGap Phúc Lộc Thọ - Cay từ Đất, Chất từ Tâm",
//                priceNew: 125000,
//                priceOld: 140000,
//                havePromotion: false,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/5efc6b3b-3626-47cc-b408-a27515faa2cc.jpg"),
//            ItemProductModel(
//                id: 1,
//                title:
//                    "Mật hoa dừa Trà Vinh - Vị ngọt cho thanh xuân, dinh dưỡng cho sức khỏe",
//                priceNew: 125400,
//                priceOld: 132000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/f1c9406f-61de-4eab-836b-7e28facfe632.jpg"),
//            ItemProductModel(
//                id: 2,
//                title:
//                    "Mật ong hoa cà phê Đà Lạt - Trạm ghé của những chú Ong du mục",
//                priceNew: 128250,
//                priceOld: 135000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/47ad2e18-350b-4f9b-ae1e-298a746b1fb0.jpg"),
//            ItemProductModel(
//                id: 3,
//                title: "Hạt Cacao mật dừa Trà Vinh - Dinh dưỡng cả khi ăn vặt",
//                priceNew: 71250,
//                priceOld: 75000,
//                havePromotion: false,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/ebc866ef-1140-4100-873c-596f27da8bbb.jpg"),
//            ItemProductModel(
//                id: 4,
//                title: "Tôi thấy hoa vàng trên cỏ xanh",
//                priceNew: 112500,
//                priceOld: 125000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/cd4ac917-e4b4-4a13-9ad2-ae3985dee9d5.jpg"),
//          ]),
//      ItemDealModel(
//          id: 0,
//          title: "Chiến dịch bền vững mới sắp ra mắt".toUpperCase(),
//          listProducts: [
//            ItemProductModel(
//                id: 4,
//                title: "Tôi thấy hoa vàng trên cỏ xanh",
//                priceNew: 112500,
//                priceOld: 125000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/cd4ac917-e4b4-4a13-9ad2-ae3985dee9d5.jpg"),
//            ItemProductModel(
//                id: 3,
//                title: "Hạt Cacao mật dừa Trà Vinh - Dinh dưỡng cả khi ăn vặt",
//                priceNew: 71250,
//                priceOld: 75000,
//                havePromotion: false,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/ebc866ef-1140-4100-873c-596f27da8bbb.jpg"),
//            ItemProductModel(
//                id: 0,
//                title:
//                    "Tương ớt Sạch VietGap Phúc Lộc Thọ - Cay từ Đất, Chất từ Tâm",
//                priceNew: 125000,
//                priceOld: 140000,
//                havePromotion: false,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/5efc6b3b-3626-47cc-b408-a27515faa2cc.jpg"),
//            ItemProductModel(
//                id: 1,
//                title:
//                    "Mật hoa dừa Trà Vinh - Vị ngọt cho thanh xuân, dinh dưỡng cho sức khỏe",
//                priceNew: 125400,
//                priceOld: 132000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/f1c9406f-61de-4eab-836b-7e28facfe632.jpg"),
//            ItemProductModel(
//                id: 2,
//                title:
//                    "Mật ong hoa cà phê Đà Lạt - Trạm ghé của những chú Ong du mục",
//                priceNew: 128250,
//                priceOld: 135000,
//                havePromotion: true,
//                status: 'in_stock',
//                dueTimeStr: '2020-07-31T02:14:00.000000Z',
//                quantity: 60,
//                quantitySale: 100,
//                imgSrc:
//                    "https://olma.s3-ap-southeast-1.amazonaws.com/images/47ad2e18-350b-4f9b-ae1e-298a746b1fb0.jpg"),
//          ]),
//    ];
//
//    SizeHelper sizeHelper = SizeHelper(context);
//    double _hListDeal = sizeHelper.rW(55);
//
//    List<Widget> listProductDeal(List<ItemProductModel> listProducts) {
//      return listProducts
//          .map((item) => ItemProduct(
//                data: item,
//                type: ItemProductType.DEAL,
//              ))
//          .toList();
//    }
//
//    Widget itemDeal(ItemDealModel deal) {
//      return SectionWrapper(
//        title: deal.title,
//        child: Container(
//          height: _hListDeal,
//          child: ListView(
//            scrollDirection: Axis.horizontal,
//            children: listProductDeal(deal.listProducts),
//          ),
//        ),
//      );
//    }
//
//    final List<Widget> listDeals =
//        _listDealsModel.map((item) => itemDeal(item)).toList();
//
//    return Column(
//      children: listDeals,
//    );
//  }
//}
//
//class ItemDealModel {
//  final int id;
//  final String title;
//  final List<ItemProductModel> listProducts;
//
//  ItemDealModel({this.id, this.title, this.listProducts});
//}
