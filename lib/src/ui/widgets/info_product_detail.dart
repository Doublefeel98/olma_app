import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/plugins/rate.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/money_formatter.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class InfoProductDetail extends StatefulWidget {
  final Product _product;

  InfoProductDetail({Key key, @required Product product})
      : assert(product != null),
        _product = product,
        super(key: key);
  @override
  _InfoProductDetailState createState() => _InfoProductDetailState();
}

class _InfoProductDetailState extends State<InfoProductDetail> {
  Product get _product => widget._product;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return SectionWrapper(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
            child: RichText(
              text: TextSpan(
                children: <InlineSpan>[
                  WidgetSpan(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.7),
                          horizontal: sizeHelper.rW(2)),
                      margin: EdgeInsets.only(
                        right: sizeHelper.rW(2),
                      ),
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 60, 160, 245),
                          borderRadius: BorderRadius.all(
                              Radius.circular(sizeHelper.rW(0.5)))),
                      child: InkWell(
                        onTap: () {},
                        child: Text(
                          _product.brand.name,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: sizeHelper.rW(3.2),
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ),
                  TextSpan(
                    text: _product.name,
                    style: TextStyle(
                      color: ColorConstants.GREY_TEXT,
                      fontSize: sizeHelper.rW(5),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _product.productDiscount != null
                  ? <Widget>[
                      Text(
                        MoneyFormatter(
                                amount: _product.productDiscount.promotionPrice)
                            .toString(),
                        style: TextStyle(
                            color: ColorConstants.BLUE_PRIMARY,
                            fontSize: sizeHelper.rW(7),
                            fontWeight: FontWeight.w600),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: sizeHelper.rW(3)),
                      ),
                      Text(
                        MoneyFormatter(amount: _product.price).toString(),
                        style: TextStyle(
                          color: ColorConstants.GREY_TEXT,
                          fontSize: sizeHelper.rW(4),
                          fontWeight: FontWeight.w300,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                    ]
                  : <Widget>[
                      Text(
                        MoneyFormatter(amount: _product.price).toString(),
                        style: TextStyle(
                            color: ColorConstants.BLUE_PRIMARY,
                            fontSize: sizeHelper.rW(7),
                            fontWeight: FontWeight.w600),
                      ),
                    ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Rate(
                  initialRating: _product.avgRating.toDouble(),
                ),
                Text(
                  "${_product.totalRating} ĐÁNH GIÁ",
                  style: TextStyle(
                    color: ColorConstants.BLUE_PRIMARY_TEXT,
                    fontSize: sizeHelper.rW(3.5),
                  ),
                )
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
              child: Container(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(2)),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                  border: Border.all(
                    color: Color.fromARGB(50, 10, 110, 150),
                  ),
                  color: Color.fromARGB(5, 5, 150, 250),
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.check_circle,
                          color: Color.fromARGB(255, 30, 150, 60),
                          size: sizeHelper.rW(4),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: sizeHelper.rW(1)),
                        ),
                        Text(
                          "Olma cam kết".toUpperCase(),
                          style: TextStyle(
                            color: Color.fromARGB(255, 30, 150, 60),
                            fontSize: sizeHelper.rW(4),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    Html(
                      data: """
              <p><span class="ql-font-roboto" style="color: rgb(0, 138, 0); font-size: 14px;">Olma cam kết hoàn tiền 150% cho người tiêu dùng nếu sản phẩm không đúng như thông tin mà Olma đã cung cấp</span></p>
            """,
                      style: {
                        "p": Style(
                          fontSize: FontSize(sizeHelper.rW(4)),
                          textAlign: TextAlign.justify,
                        )
                      },
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
