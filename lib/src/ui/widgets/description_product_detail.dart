import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class DescriptionProductDetail extends StatefulWidget {
  final List<ProductDetail> _listProductDetails;

  DescriptionProductDetail(
      {Key key, @required List<ProductDetail> listProductDetails})
      : assert(listProductDetails != null),
        _listProductDetails = listProductDetails,
        super(key: key);

  @override
  _DescriptionProductDetailState createState() =>
      _DescriptionProductDetailState();
}

class _DescriptionProductDetailState extends State<DescriptionProductDetail> {
  List<ProductDetail> get _listProductDetails => widget._listProductDetails;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    List<Widget> showTableDetail() {
      List<Widget> result = _listProductDetails
          .map<Widget>(
            (item) => Row(
              children: <Widget>[
                Flexible(
                  flex: 3,
                  fit: FlexFit.tight,
                  child: Container(
                    color: Color.fromARGB(20, 10, 110, 150),
                    padding: EdgeInsets.all(sizeHelper.rW(2)),
                    child: Text(
                      item.name,
                      style: TextStyle(
                        fontSize: sizeHelper.rW(3),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 7,
                  fit: FlexFit.tight,
                  child: Container(
                    padding: EdgeInsets.all(sizeHelper.rW(2)),
                    child: Text(
                      item.content,
                      style: TextStyle(
                        fontSize: sizeHelper.rW(3),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
          .toList();

      return result;
    }

    return SectionWrapper(
      title: "Chi tiết sản phẩm",
      showReadMore: false,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Color.fromARGB(100, 200, 200, 200),
            ),
          ),
          child: Column(
            children: showTableDetail(),
          ),
        ),
      ),
    );
  }
}
