import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/comment.dart';
import 'package:olmaapp/src/repository/comment_repository.dart';
import 'bloc.dart';

class CommentsProductDetailBloc
    extends Bloc<CommentsProductDetailEvent, CommentsProductDetailState> {
  CommentRepository commentRepository;

  CommentsProductDetailBloc({@required this.commentRepository});

  @override
  // TODO: implement initialState
  CommentsProductDetailState get initialState =>
      CommentsProductDetailInitialState();

  @override
  Stream<CommentsProductDetailState> mapEventToState(
      CommentsProductDetailEvent event) async* {
    if (event is FetchCommentsProductDetailEvent) {
      yield CommentsProductDetailLoadingState();
      try {
        ListComment listComment =
            await commentRepository.getListCommentProduct(event.productId);
        yield CommentsProductDetailLoadedState(listComment: listComment);
      } catch (e) {
        yield CommentsProductDetailErrorState(message: e.toString());
      }
    }
  }
}
