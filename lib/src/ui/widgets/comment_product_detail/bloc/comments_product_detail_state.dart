import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/comment.dart';

abstract class CommentsProductDetailState extends Equatable {}

class CommentsProductDetailInitialState extends CommentsProductDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CommentsProductDetailLoadingState extends CommentsProductDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CommentsProductDetailLoadedState extends CommentsProductDetailState {
  ListComment listComment;

  CommentsProductDetailLoadedState({@required this.listComment});

  @override
  // TODO: implement props
  List<Object> get props => [listComment];
}

class CommentsProductDetailErrorState extends CommentsProductDetailState {
  String message;

  CommentsProductDetailErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
