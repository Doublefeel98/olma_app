import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CommentsProductDetailEvent extends Equatable {}

class FetchCommentsProductDetailEvent extends CommentsProductDetailEvent {
  final int productId;

  FetchCommentsProductDetailEvent({@required this.productId});

  @override
  List<Object> get props => [productId];

  @override
  String toString() => 'Product ID { email :$productId }';
}
