import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/comment.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/plugins/rate.dart';
import 'package:olmaapp/src/ui/screens/add_comment/add_comment_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/bloc/bloc.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class CommentsProductDetailUI extends StatefulWidget {
  final int _productId;

  CommentsProductDetailUI({Key key, @required int productId})
      : assert(productId != null),
        _productId = productId,
        super(key: key);

  @override
  _CommentsProductDetailUIState createState() =>
      _CommentsProductDetailUIState();
}

class _CommentsProductDetailUIState extends State<CommentsProductDetailUI> {
  CommentsProductDetailBloc _commentsProductDetailBloc;
  int get _productId => widget._productId;
  @override
  void initState() {
    super.initState();
    _commentsProductDetailBloc =
        BlocProvider.of<CommentsProductDetailBloc>(context);
    // _commentsProductDetailBloc
    //     .add(FetchCommentsProductDetailEvent(productId: _productId));
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<CommentsProductDetailBloc, CommentsProductDetailState>(
      listener: (context, state) {
        if (state is CommentsProductDetailErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<CommentsProductDetailBloc, CommentsProductDetailState>(
          builder: (context, state) {
        Widget widget;
        if (state is CommentsProductDetailLoadedState) {
          widget = buildCommentsProductDetail(state.listComment, sizeHelper);
        } else if (state is CommentsProductDetailErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildCommentsProductDetailContainer(widget);
      }),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildCommentsProductDetailContainer(Widget widget) {
    return widget;
  }

  Widget buildCommentsProductDetail(
      ListComment listComment, SizeHelper sizeHelper) {
    return SectionWrapper(
      title: "NHẬN XÉT TỪ KHÁCH HÀNG",
      showReadMore: true,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(2)),
        child: Container(
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    child: Text(
                      "${listComment.avgRating}/5",
                      style: TextStyle(
                        color: ColorConstants.BLUE_PRIMARY_TEXT,
                        fontSize: sizeHelper.rW(7),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Rate(
                          initialRating: listComment.avgRating,
                          align: MainAxisAlignment.center,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    child: Text(
                      "(${listComment.countRating} nhận xét)",
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(2.5),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
                    child: Column(
                      children: [
                        buildPercentComment(
                            "5",
                            listComment.countRatingFivePoint,
                            listComment.countRating,
                            sizeHelper),
                        buildPercentComment(
                            "4",
                            listComment.countRatingFourPoint,
                            listComment.countRating,
                            sizeHelper),
                        buildPercentComment(
                            "3",
                            listComment.countRatingThreePoint,
                            listComment.countRating,
                            sizeHelper),
                        buildPercentComment(
                            "2",
                            listComment.countRatingTwoPoint,
                            listComment.countRating,
                            sizeHelper),
                        buildPercentComment(
                            "1",
                            listComment.countRatingOnePoint,
                            listComment.countRating,
                            sizeHelper),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                AddCommentScreen(productId: _productId)));
                      },
                      child: Text(
                        "Gửi nhận xét của bạn",
                        style: TextStyle(
                          fontSize: sizeHelper.rW(3.2),
                          color: Colors.white,
                        ),
                      ),
                      color: Color.fromARGB(255, 100, 200, 240),
                      elevation: 0,
                    ),
                  ),
                ],
              ),
              Column(
                children: listComment.listComments.map((Comment comment) {
                  return buildComment(comment, sizeHelper);
                }).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPercentComment(
      String rating, int count, int countTotal, SizeHelper sizeHelper) {
    return Container(
      margin: EdgeInsets.only(bottom: sizeHelper.rW(1.5)),
      child: Row(
        children: [
          Text(
            rating,
            style: TextStyle(
                fontSize: sizeHelper.rW(3.5), color: ColorConstants.GREY_TEXT),
          ),
          Icon(
            Icons.star,
            size: sizeHelper.rW(4),
          ),
          Expanded(
            child: Container(
              child: LinearPercentIndicator(
                lineHeight: sizeHelper.rW(1.8),
                percent: countTotal > 0 ? count / countTotal : 0.0,
                linearStrokeCap: LinearStrokeCap.roundAll,
                backgroundColor: Color.fromARGB(255, 200, 200, 200),
                progressColor: Color.fromARGB(255, 255, 95, 55),
              ),
            ),
          ),
          Text(
            "$count đánh giá",
            style: TextStyle(
                fontSize: sizeHelper.rW(3.2), color: ColorConstants.GREY_TEXT),
          ),
        ],
      ),
    );
  }

  Widget buildComment(Comment comment, SizeHelper sizeHelper) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(3)),
      padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(3)),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(20, 0, 0, 0),
            blurRadius: 1,
            spreadRadius: 1,
            offset: Offset(1, 1),
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(
            sizeHelper.rW(1),
          ),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(1)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Rate(
                  initialRating: comment.rating.toDouble(),
                  size: 3.5,
                ),
                Text(
                  comment.time.toString(),
                  style: TextStyle(
                      color: ColorConstants.GREY_TEXT,
                      fontSize: sizeHelper.rW(3)),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(1.5)),
            child: Text(
              comment.user.name,
              style: TextStyle(
                fontSize: sizeHelper.rW(3.2),
              ),
            ),
          ),
          Text(
            comment.content,
            style: TextStyle(
              fontSize: sizeHelper.rW(3.5),
              fontWeight: FontWeight.w300,
            ),
          )
        ],
      ),
    );
  }
}
