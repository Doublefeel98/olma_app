import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/screens/detail_product_detail_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class DetailProductDetail extends StatefulWidget {
  final List<ProductTab> _listProductTabs;

  DetailProductDetail({Key key, @required List<ProductTab> listProductTabs})
      : assert(listProductTabs != null),
        _listProductTabs = listProductTabs,
        super(key: key);
  @override
  _DetailProductDetailState createState() => _DetailProductDetailState();
}

class _DetailProductDetailState extends State<DetailProductDetail> {
  List<ProductTab> get _listProductTabs => widget._listProductTabs;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    List<Widget> showButtonDetail() {
      List<Widget> result = _listProductTabs
          .map<Widget>(
            (item) => InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => DetailProductDetailScreen(
                      title: item.name,
                      content: item.content,
                    ),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(3)),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromARGB(255, 255, 255, 255),
                      Color.fromARGB(255, 240, 240, 240),
                    ],
                  ),
                  borderRadius:
                      BorderRadius.all(Radius.circular(sizeHelper.rW(1.5))),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 0,
                      blurRadius: 1,
                      offset: Offset(0, 0),
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      item.name,
                      style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                    ),
                    Icon(
                      Icons.chevron_right,
                      size: sizeHelper.rW(5),
                    )
                  ],
                ),
              ),
            ),
          )
          .toList();

      return result;
    }

    return SectionWrapper(
      title: "Mô tả sản phẩm",
      showReadMore: false,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
        child: Column(
          children: showButtonDetail(),
        ),
      ),
    );
  }
}
