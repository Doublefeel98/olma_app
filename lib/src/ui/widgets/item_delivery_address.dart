import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class ItemDeliveryAddress extends StatelessWidget {
  final DeliveryAddress deliveryAddress;
  final Function onTap;

  const ItemDeliveryAddress(
      {Key key, @required this.deliveryAddress, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
          color: Colors.black26,
          width: 0.5,
        ))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  deliveryAddress.name,
                  style: TextStyle(
                    fontSize: sizeHelper.rW(3.5),
                    fontWeight: FontWeight.w500,
                  ),
                ),
                deliveryAddress.isDefault
                    ? Text(
                        "Mặc định",
                        style: TextStyle(
                          fontSize: sizeHelper.rW(3.5),
                          fontWeight: FontWeight.w500,
                          color: ColorConstants.BLUE_PRIMARY_TEXT,
                        ),
                      )
                    : Container(),
              ],
            ),
            SizedBox(
              height: sizeHelper.rW(1),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  deliveryAddress.phone,
                  style: TextStyle(
                    fontSize: sizeHelper.rW(3.5),
                    fontWeight: FontWeight.w400,
                  ),
                ),
                deliveryAddress.isDefault
                    ? Icon(
                        Icons.check,
                        color: Colors.green,
                      )
                    : Container()
              ],
            ),
            SizedBox(
              height: sizeHelper.rW(1),
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text:
                      "${deliveryAddress.address}, ${deliveryAddress.wardName}, ${deliveryAddress.districtName}, ${deliveryAddress.cityName}",
                  style: TextStyle(
                    color: ColorConstants.GREY_TEXT,
                    fontSize: sizeHelper.rW(3.2),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
