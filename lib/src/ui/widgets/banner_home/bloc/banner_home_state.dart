import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';

abstract class BannerHomeState extends Equatable {}

class BannerHomeInitialState extends BannerHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class BannerHomeLoadingState extends BannerHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class BannerHomeLoadedState extends BannerHomeState {

  Banner banner;

  BannerHomeLoadedState({@required this.banner});

  @override
  // TODO: implement props
  List<Object> get props => [banner];
}

class BannerHomeErrorState extends BannerHomeState {

  String message;

  BannerHomeErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}