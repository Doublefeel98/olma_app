import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/repository/banner_repository.dart';
import 'bloc.dart';

class BannerHomeBloc extends Bloc<BannerHomeEvent, BannerHomeState> {

  BannerRepository bannerRepository;

  BannerHomeBloc({@required this.bannerRepository});

  @override
  // TODO: implement initialState
  BannerHomeState get initialState => BannerHomeInitialState();

  @override
  Stream<BannerHomeState> mapEventToState(BannerHomeEvent event) async* {
    if (event is FetchBannerHomeEvent) {
      yield BannerHomeLoadingState();
      try {
        Banner banner = await bannerRepository.getBannerHome();
        yield BannerHomeLoadedState(banner: banner);
      } catch (e) {
        yield BannerHomeErrorState(message: e.toString());
      }
    }
  }
}