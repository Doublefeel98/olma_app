import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart' hide Banner;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/banner_home/bloc/bloc.dart';
import 'package:shimmer/shimmer.dart';

class BannerHomePrimary extends StatefulWidget {
  @override
  _BannerHomePrimaryState createState() => _BannerHomePrimaryState();
}

class _BannerHomePrimaryState extends State<BannerHomePrimary> {
  int _currentIndex = 0;
  BannerHomeBloc _bannerHomeBloc;

  @override
  void initState() {
    super.initState();
    _bannerHomeBloc = BlocProvider.of<BannerHomeBloc>(context);
    //_bannerHomeBloc.add(FetchBannerHomeEvent());
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<BannerHomeBloc, BannerHomeState>(
      listener: (context, state) {
        if (state is BannerHomeErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<BannerHomeBloc, BannerHomeState>(
          builder: (context, state) {
        Widget widget;
        if (state is BannerHomeLoadedState) {
          widget = buildBannerImages(state.banner, sizeHelper);
        } else if (state is BannerHomeErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading(sizeHelper);
        }
        return buildBannerHomeContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildLoading(SizeHelper sizeHelper) {
    return Shimmer.fromColors(
        period: Duration(seconds: 1),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: sizeHelper.rW(2),
            ),
            Center(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  boxShadow: [
                    BoxShadow(
                      color: ColorConstants.BANNER_HOME_PRIMARY_SHAWDOW,
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, 0),
                    )
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  child: Container(
                    color: Colors.white,
                    width: sizeHelper.rW(78),
                    height: sizeHelper.rH(18),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
            ),
            Center(
              child: Container(
                width: 40.0,
                height: 3.0,
                color: Colors.white,
              ),
            )
          ],
        ),
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100]);
  }

  Widget itemImageSlider(BannerImages item) => Container(
        child: Container(
          padding: EdgeInsets.all(5),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(
                  color: ColorConstants.BANNER_HOME_PRIMARY_SHAWDOW,
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: Offset(0, 0),
                )
              ],
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: CachedNetworkImage(
                  imageUrl: item.image,
                  width: double.infinity,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
                //Image.network(item.image, fit: BoxFit.cover, width: 1000.0),
                ),
          ),
        ),
      );

  Widget itemIndicatorSlider(int index, SizeHelper sizeHelper) => Container(
        width: sizeHelper.rW(3),
        height: sizeHelper.rW(3 / 7),
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 2.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          color: _currentIndex == index
              ? Color.fromRGBO(0, 0, 0, 0.9)
              : Color.fromRGBO(0, 0, 0, 0.4),
        ),
      );

  List<Widget> imageSliders(Banner banner) {
    return banner.bannerImages.map((item) => itemImageSlider(item)).toList();
  }

  List<Widget> indicatorSliders(Banner banner, SizeHelper sizeHelper) {
    return banner.bannerImages.map((item) {
      int index = banner.bannerImages.indexOf(item);
      return itemIndicatorSlider(index, sizeHelper);
    }).toList();
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildBannerImages(Banner banner, SizeHelper sizeHelper) {
    return Column(
      children: <Widget>[
        CarouselSlider(
          items: imageSliders(banner),
          options: CarouselOptions(
              aspectRatio: 8 / 3,
              initialPage: 0,
              enableInfiniteScroll: true,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              enlargeCenterPage: true,
              autoPlayCurve: Curves.fastOutSlowIn,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentIndex = index;
                });
              }),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: indicatorSliders(banner, sizeHelper),
        ),
      ],
    );
  }

  Widget buildBannerHomeContainer(Widget content, SizeHelper sizeHelper) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            height: sizeHelper.rH(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.elliptical(200.0, 20.0),
                bottomRight: Radius.elliptical(200.0, 20.0),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    ColorConstants.BLUE_PRIMARY,
                    ColorConstants.GREY_APP_BG
                  ]),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 5.0),
            child: content,
          ),
        ],
      ),
    );
  }
}
