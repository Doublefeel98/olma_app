import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/banner_repository.dart';
import 'package:olmaapp/src/ui/widgets/banner_home/banner_home_primary.dart';
import 'package:olmaapp/src/ui/widgets/banner_home/bloc/bloc.dart';
import 'package:provider/provider.dart';

class BannerHome extends StatelessWidget {
  BannerHomeBloc _bannerHomeBloc;
  @override
  Widget build(BuildContext context) {
    final _bannerRepository = RepositoryProvider.of<BannerRepository>(context);
    return BlocProvider<BannerHomeBloc>(
      create: (context) {
        _bannerHomeBloc = BannerHomeBloc(bannerRepository: _bannerRepository);
        _bannerHomeBloc.add(FetchBannerHomeEvent());
        return _bannerHomeBloc;
      },
      child: BannerHomePrimary(),
    );
  }
}
