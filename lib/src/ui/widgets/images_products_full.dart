import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class ImagesProductFull extends StatefulWidget {
  final List<ProductImage> images;
  final int initialPage;

  ImagesProductFull({this.images, this.initialPage});

  @override
  _ImagesProductFullState createState() => _ImagesProductFullState(
      images: this.images, initialPage: this.initialPage);
}

class _ImagesProductFullState extends State<ImagesProductFull> {
  int _currentIndex;
  final int initialPage;
  final List<ProductImage> images;

  CarouselController _carouselController = CarouselController();
  ScrollController _scrollController;

  _ImagesProductFullState({this.images, this.initialPage}) {
    _currentIndex = this.initialPage;
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    double _sizeItemIndicator = sizeHelper.rW(15);

    double position = this._currentIndex * _sizeItemIndicator +
        _sizeItemIndicator / 2 -
        sizeHelper.rW(100) / 2;
    this._scrollController = ScrollController(initialScrollOffset: position);

    Widget itemImageSlider(ProductImage item) => Container(
          child: CachedNetworkImage(
            imageUrl: item.link,
            width: double.infinity,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        );

    Widget itemIndicatorSlider(ProductImage item) {
      int index = this.images.indexOf(item);
      return InkWell(
        onTap: () {
          _carouselController.animateToPage(index,
              duration: Duration(milliseconds: 200), curve: Curves.linear);
        },
        child: Container(
          width: _sizeItemIndicator,
          height: _sizeItemIndicator,
          margin: EdgeInsets.only(left: sizeHelper.rW(1.5)),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: _currentIndex == index
                ? Border.all(color: ColorConstants.BLUE_PRIMARY)
                : Border.all(color: Colors.transparent),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: CachedNetworkImage(
              imageUrl: item.link,
              width: double.infinity,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      );
    }

    final List<Widget> imageSliders =
        this.images.map((item) => itemImageSlider(item)).toList();

    final List<Widget> indicatorSliders = this.images.map((item) {
      return itemIndicatorSlider(item);
    }).toList();

    return Container(
      height: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: CarouselSlider(
                items: imageSliders,
                carouselController: _carouselController,
                options: CarouselOptions(
                  aspectRatio: 1,
                  initialPage: this.initialPage,
                  viewportFraction: 1.0,
                  enableInfiniteScroll: false,
                  onPageChanged: (index, reason) {
                    double position = index * _sizeItemIndicator +
                        _sizeItemIndicator / 2 -
                        sizeHelper.rW(100) / 2;
                    _scrollController.animateTo(position,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.linear);
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
              ),
            ),
          ),
          Container(
            height: _sizeItemIndicator,
            margin: EdgeInsets.only(bottom: 5),
            child: ListView(
              scrollDirection: Axis.horizontal,
              controller: _scrollController,
              children: indicatorSliders,
            ),
          ),
        ],
      ),
    );
  }
}
