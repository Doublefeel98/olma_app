import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/screens/deal_detail/deal_detail_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/item_product.dart';
import 'package:olmaapp/src/ui/widgets/list_deal_home/bloc/bloc.dart';
import 'package:shimmer/shimmer.dart';

class ListDealHome extends StatefulWidget {
  @override
  _ListDealHomeState createState() => _ListDealHomeState();
}

class _ListDealHomeState extends State<ListDealHome> {
  ListDealHomeBloc _listDealHomeBloc;

  @override
  void initState() {
    super.initState();
    _listDealHomeBloc = BlocProvider.of<ListDealHomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<ListDealHomeBloc, ListDealHomeState>(
      listener: (context, state) {
        if (state is ListDealHomeErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<ListDealHomeBloc, ListDealHomeState>(
          builder: (context, state) {
        Widget widget;
        if (state is ListDealHomeLoadedState) {
          widget = buildListDeals(state.listDealForHomes, sizeHelper);
        } else if (state is ListDealHomeErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading(sizeHelper);
        }
        return buildDealContainer(widget);
      }),
    );
  }

  Widget buildLoading(SizeHelper sizeHelper) {
    double _pItemProduct = sizeHelper.rW(2.5);
    double _wItemProduct = sizeHelper.rW(40.0);
    double _hListDeal = sizeHelper.rW(55);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [0, 1, 2]
          .map(
            (_) => Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
              child: Shimmer.fromColors(
                period: Duration(seconds: 1),
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: sizeHelper.rW(60),
                      height: sizeHelper.rW(3.5),
                      margin: EdgeInsets.all(sizeHelper.rW(2)),
                      color: Colors.white,
                    ),
                    Container(
                      height: _hListDeal,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [0, 1, 2]
                            .map(
                              (_) => Container(
                                padding: EdgeInsets.all(_pItemProduct),
                                width: _wItemProduct,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(3)),
                                      child: AspectRatio(
                                        aspectRatio: 3 / 2,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(3)),
                                          child: Container(
                                            width: double.infinity,
                                            height: double.infinity,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: sizeHelper.rW(2.5),
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(1.5)),
                                      color: Colors.white,
                                    ),
                                    Container(
                                      height: sizeHelper.rW(2.5),
                                      width: sizeHelper.rW(30),
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(1.5)),
                                      color: Colors.white,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(1.5)),
                                      height: sizeHelper.rW(3),
                                      width: sizeHelper.rW(20),
                                      color: Colors.white,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(2)),
                                      height: sizeHelper.rW(1.5),
                                      width: sizeHelper.rW(35),
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
          .toList(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildDealContainer(Widget widget) {
    return widget;
  }

  Widget buildListDeals(
      List<DealForHome> listDealForHomes, SizeHelper sizeHelper) {
    return Column(
      children: listDealForHomes
          .map((item) => buildItemDeal(item, sizeHelper))
          .toList(),
    );
  }

  Widget buildItemDeal(DealForHome deal, SizeHelper sizeHelper) {
    double _hListDeal = sizeHelper.rW(55);
    return SectionWrapper(
      title: deal.name,
      onTapReadMore: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => DealDetailScreen(dealId: deal.id),
          ),
        );
      },
      child: Container(
        height: _hListDeal,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: listProductDeal(deal.listDealDetails),
        ),
      ),
    );
  }

  List<Widget> listProductDeal(List<ProductItem> listDealDetails) {
    return listDealDetails
        .map((item) =>
            ItemProduct(productItem: item, type: ItemProductType.DEAL))
        .toList();
  }
}
