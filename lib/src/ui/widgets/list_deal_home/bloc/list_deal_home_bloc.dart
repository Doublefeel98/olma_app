import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/repository/deal_repository.dart';

import 'bloc.dart';

class ListDealHomeBloc extends Bloc<ListDealHomeEvent, ListDealHomeState> {
  DealRepository dealRepository;

  ListDealHomeBloc({@required this.dealRepository});

  @override
  // TODO: implement initialState
  ListDealHomeState get initialState => ListDealHomeInitialState();

  @override
  Stream<ListDealHomeState> mapEventToState(ListDealHomeEvent event) async* {
    if (event is FetchListDealHomeEvent) {
      yield ListDealHomeLoadingState();
      try {
        List<DealForHome> listDealForHomes =
            await dealRepository.getDealForHome();
        yield ListDealHomeLoadedState(listDealForHomes: listDealForHomes);
      } catch (e) {
        yield ListDealHomeErrorState(message: e.toString());
      }
    }
  }
}
