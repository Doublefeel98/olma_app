import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';

abstract class ListDealHomeState extends Equatable {}

class ListDealHomeInitialState extends ListDealHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListDealHomeLoadingState extends ListDealHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListDealHomeLoadedState extends ListDealHomeState {
  List<DealForHome> listDealForHomes;

  ListDealHomeLoadedState({@required this.listDealForHomes});

  @override
  // TODO: implement props
  List<Object> get props => [listDealForHomes];
}

class ListDealHomeErrorState extends ListDealHomeState {
  String message;

  ListDealHomeErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
