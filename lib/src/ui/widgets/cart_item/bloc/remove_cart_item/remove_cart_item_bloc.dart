import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'bloc.dart';

class RemoveCartItemBloc
    extends Bloc<RemoveCartItemEvent, RemoveCartItemState> {
  CartRepository cartRepository;

  RemoveCartItemBloc({@required this.cartRepository});

  @override
  // TODO: implement initialState
  RemoveCartItemState get initialState => RemoveCartItemInitialState();

  @override
  Stream<RemoveCartItemState> mapEventToState(
      RemoveCartItemEvent event) async* {
    if (event is PostRemoveCartItemEvent) {
      yield RemoveCartItemLoadingState();
      try {
        bool result =
            await cartRepository.removeCartItem(productId: event.productId);
        if (result) {
          yield RemoveCartItemResponseState();
        } else {
          yield RemoveCartItemErrorState(
              message: "Xóa sản phẩm ra giỏ hàng thất bại");
        }
      } catch (e) {
        yield RemoveCartItemErrorState(message: e.toString());
      }
    }
  }
}
