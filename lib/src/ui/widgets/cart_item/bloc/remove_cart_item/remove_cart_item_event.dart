import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RemoveCartItemEvent extends Equatable {}

class PostRemoveCartItemEvent extends RemoveCartItemEvent {
  final int productId;

  PostRemoveCartItemEvent({@required this.productId});
  @override
  // TODO: implement props
  List<Object> get props => null;
}
