import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class RemoveCartItemState extends Equatable {}

class RemoveCartItemInitialState extends RemoveCartItemState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class RemoveCartItemLoadingState extends RemoveCartItemState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class RemoveCartItemResponseState extends RemoveCartItemState {
  RemoveCartItemResponseState();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class RemoveCartItemErrorState extends RemoveCartItemState {
  String message;

  RemoveCartItemErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
