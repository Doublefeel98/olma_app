import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/cart.dart';

abstract class UpdateCartItemState extends Equatable {}

class UpdateCartItemInitialState extends UpdateCartItemState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class UpdateCartItemLoadingState extends UpdateCartItemState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class UpdateCartItemResponseState extends UpdateCartItemState {
  CartResponse cartResponse;

  UpdateCartItemResponseState({@required this.cartResponse});

  @override
  // TODO: implement props
  List<Object> get props => [cartResponse];
}

class UpdateCartItemErrorState extends UpdateCartItemState {
  String message;

  UpdateCartItemErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
