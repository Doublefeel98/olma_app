import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'bloc.dart';

class UpdateCartItemBloc
    extends Bloc<UpdateCartItemEvent, UpdateCartItemState> {
  CartRepository cartRepository;

  UpdateCartItemBloc({@required this.cartRepository});

  @override
  // TODO: implement initialState
  UpdateCartItemState get initialState => UpdateCartItemInitialState();

  @override
  Stream<UpdateCartItemState> mapEventToState(
      UpdateCartItemEvent event) async* {
    if (event is PostUpdateCartItemEvent) {
      yield UpdateCartItemLoadingState();
      try {
        CartResponse cartResponse = await cartRepository.updateQuantityCartItem(
            productId: event.productId, quantity: event.quantity);

        if (cartResponse.success) {
          yield UpdateCartItemResponseState(cartResponse: cartResponse);
        } else {
          yield UpdateCartItemErrorState(message: cartResponse.toString());
        }
      } catch (e) {
        yield UpdateCartItemErrorState(message: e.toString());
      }
    }
  }
}
