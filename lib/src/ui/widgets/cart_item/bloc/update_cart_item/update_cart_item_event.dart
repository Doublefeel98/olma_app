import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class UpdateCartItemEvent extends Equatable {}

class PostUpdateCartItemEvent extends UpdateCartItemEvent {
  final int productId;
  final int quantity;

  PostUpdateCartItemEvent({@required this.productId, @required this.quantity});
  @override
  // TODO: implement props
  List<Object> get props => null;
}
