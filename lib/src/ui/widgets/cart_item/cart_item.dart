import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/cart_repository.dart';
import 'package:olmaapp/src/ui/widgets/cart_item/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/cart_item/cart_item_ui.dart';

typedef void CartItemSelectedChangeCallback(bool result, CartItem cartItem);

class CartItemWidget extends StatelessWidget {
  final CartItem _cartItem;
  final GlobalKey<ScaffoldState> _scaffoldKey;
  final CartItemSelectedChangeCallback _onChane;

  CartItemWidget(
      {Key key,
      @required CartItem cartItem,
      @required GlobalKey<ScaffoldState> scaffoldKey,
      @required CartItemSelectedChangeCallback onChane})
      : assert(cartItem != null && scaffoldKey != null),
        _cartItem = cartItem,
        _scaffoldKey = scaffoldKey,
        _onChane = onChane,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _cartRepository = RepositoryProvider.of<CartRepository>(context);
    return MultiBlocProvider(
        providers: [
          BlocProvider<RemoveCartItemBloc>(
            create: (BuildContext context) => RemoveCartItemBloc(
              cartRepository: _cartRepository,
            ),
          ),
          BlocProvider<UpdateCartItemBloc>(
            create: (BuildContext context) => UpdateCartItemBloc(
              cartRepository: _cartRepository,
            ),
          ),
        ],
        child: CartItemUI(
            cartItem: _cartItem,
            scaffoldKey: _scaffoldKey,
            onChange: _onChane));
  }
}
