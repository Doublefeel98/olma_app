import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/product_detail/product_detail_screen.dart';
import 'package:olmaapp/src/ui/shares/number_button.dart';
import 'package:olmaapp/src/ui/utils/money_formatter.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/btn_add_to_cart/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/cart_item/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/cart_item/cart_item.dart';

class CartItemUI extends StatefulWidget {
  final CartItem _cartItem;
  final GlobalKey<ScaffoldState> _scaffoldKey;
  final CartItemSelectedChangeCallback _onChange;

  CartItemUI(
      {Key key,
      @required CartItem cartItem,
      @required GlobalKey<ScaffoldState> scaffoldKey,
      CartItemSelectedChangeCallback onChange})
      : assert(cartItem != null && scaffoldKey != null),
        _cartItem = cartItem,
        _scaffoldKey = scaffoldKey,
        _onChange = onChange,
        super(key: key);
  @override
  _CartItemUIState createState() => _CartItemUIState();
}

class _CartItemUIState extends State<CartItemUI> {
  CartItem get _cartItem => widget._cartItem;
  GlobalKey<ScaffoldState> get _scaffoldKey => widget._scaffoldKey;

  int _countToBuy = 1;

  UpdateCartItemBloc _updateCartItemBloc;
  RemoveCartItemBloc _removeCartItemBloc;
  CartBloc _cartBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _updateCartItemBloc = BlocProvider.of<UpdateCartItemBloc>(context);
    _removeCartItemBloc = BlocProvider.of<RemoveCartItemBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);

    _countToBuy = _cartItem.quantity;
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<RemoveCartItemBloc, RemoveCartItemState>(
      listener: (context, state) {
        if (state is RemoveCartItemErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        } else if (state is AddToCartLoadingState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang cập nhật giỏ hàng...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        } else if (state is RemoveCartItemResponseState) {
          _scaffoldKey.currentState..hideCurrentSnackBar();
          _cartBloc.add(FetchCartEvent());
        }
      },
      child: BlocListener<UpdateCartItemBloc, UpdateCartItemState>(
        listener: (context, state) {
          if (state is UpdateCartItemErrorState) {
            _scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text(state.message), Icon(Icons.error)],
                  ),
                  backgroundColor: Colors.red,
                ),
              );
          } else if (state is AddToCartLoadingState) {
            _scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Đang cập nhật giỏ hàng...'),
                      CircularProgressIndicator(),
                    ],
                  ),
                ),
              );
          } else if (state is UpdateCartItemResponseState) {
            _scaffoldKey.currentState..hideCurrentSnackBar();
            _cartBloc.add(FetchCartEvent());
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(
              vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(3)),
          padding: EdgeInsets.all(sizeHelper.rW(2)),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(20, 0, 0, 0),
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(1, 1),
              ),
            ],
            borderRadius: BorderRadius.all(
              Radius.circular(
                sizeHelper.rW(1),
              ),
            ),
          ),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(right: sizeHelper.rW(1)),
                  child: SizedBox(
                    width: sizeHelper.rW(5),
                    height: sizeHelper.rW(5),
                    child: Transform.scale(
                      scale: 0.8,
                      child: Checkbox(
                        value: _cartItem.selected,
                        onChanged: (bool value) {
                          widget._onChange(value, _cartItem);
                        },
                      ),
                    ),
                  ),
                ),
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => ProductDetailScreen(
                                productId: _cartItem.productId),
                          ),
                        );
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: sizeHelper.rW(2)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(sizeHelper.rW(1)),
                          child: CachedNetworkImage(
                            imageUrl: _cartItem.productPoster,
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                        ),
                      ),
                    )),
                Flexible(
                  flex: 6,
                  fit: FlexFit.tight,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ProductDetailScreen(
                                    productId: _cartItem.productId),
                              ),
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                            child: Text(
                              _cartItem.productName,
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                          child: Row(
                            children: <Widget>[
                              Text(
                                MoneyFormatter(
                                        amount:
                                            _cartItem.productDiscount != null
                                                ? _cartItem.productDiscount
                                                    .promotionPrice
                                                : _cartItem.productPrice)
                                    .toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                  fontWeight: FontWeight.w600,
                                  color: ColorConstants.BLUE_PRIMARY_TEXT,
                                ),
                              ),
                              SizedBox(
                                width: sizeHelper.rW(2),
                              ),
                              _cartItem.productDiscount != null
                                  ? Text(
                                      MoneyFormatter(
                                              amount: _cartItem.productPrice)
                                          .toString(),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: sizeHelper.rW(3.5),
                                        decoration: TextDecoration.lineThrough,
                                        color: ColorConstants
                                            .ITEM_CATEGORY_HOME_TEXT,
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              NumberButton(
                                onChange: (value) {
                                  _updateCartItemBloc.add(
                                      PostUpdateCartItemEvent(
                                          productId: _cartItem.productId,
                                          quantity: value));
                                },
                                minValue: 1,
                                maxValue: _cartItem.productQuantity,
                                initValue: _countToBuy,
                              ),
                              InkWell(
                                onTap: () {
                                  _removeCartItemBloc.add(
                                      PostRemoveCartItemEvent(
                                          productId: _cartItem.productId));
                                },
                                child: Text(
                                  "Xóa",
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(3),
                                    color: Color.fromARGB(255, 255, 0, 0),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
