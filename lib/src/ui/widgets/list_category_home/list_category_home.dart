import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/category_product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/categories_screen.dart';
import 'package:olmaapp/src/ui/screens/category_detail/category_detail_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/list_category_home/bloc/bloc.dart';
import 'package:shimmer/shimmer.dart';

class ListCategoryHome extends StatefulWidget {
  @override
  _ListCategoryHomeState createState() => _ListCategoryHomeState();
}

class _ListCategoryHomeState extends State<ListCategoryHome> {
  ListCategoryHomeBloc _listCategoryHomeBloc;

  @override
  void initState() {
    super.initState();
    _listCategoryHomeBloc = BlocProvider.of<ListCategoryHomeBloc>(context);
    //_listCategoryHomeBloc.add(FetchListCategoryHomeEvent());
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<ListCategoryHomeBloc, ListCategoryHomeState>(
      listener: (context, state) {
        if (state is ListCategoryHomeErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<ListCategoryHomeBloc, ListCategoryHomeState>(
          builder: (context, state) {
        Widget widget;
        if (state is ListCategoryHomeLoadedState) {
          widget = buildListCategoryHome(state.listCategoryHome, sizeHelper);
        } else if (state is ListCategoryHomeErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading(sizeHelper);
        }
        return buildCategoryHomeContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildCategoryHomeContainer(Widget widget, SizeHelper sizeHelper) {
    double _hListCategories = sizeHelper.rW(25);
    return SectionWrapper(
      title: "DANH MỤC",
      onTapReadMore: () => {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => CategoriesScreen()))
      },
      child: Container(
        height: _hListCategories,
        child: widget,
      ),
    );
  }

  Widget buildLoading(SizeHelper sizeHelper) {
    double _wItemCategory = sizeHelper.rW(20);
    double _pTopItemCategory = sizeHelper.rW(2);
    double _pHorizontalItemCategory = sizeHelper.rW(0.5);
    double _hIconItemCategory = sizeHelper.rW(10);
    double _wIconItemCategory = sizeHelper.rW(10);
    double _pItemCategory = sizeHelper.rW(1);

    return Shimmer.fromColors(
        period: Duration(seconds: 1),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [0, 1, 2, 3, 4]
              .map((_) => Padding(
                    padding: EdgeInsets.all(_pItemCategory),
                    child: Container(
                      padding: EdgeInsets.only(
                          top: _pTopItemCategory,
                          left: _pHorizontalItemCategory,
                          right: _pHorizontalItemCategory),
                      width: _wItemCategory,
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: _hIconItemCategory,
                            width: _wIconItemCategory,
                            padding: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(sizeHelper.rW(2))),
                              color: Colors.white,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1.5),
                              horizontal: sizeHelper.rW(2),
                            ),
                            height: sizeHelper.rW(1.5),
                            color: Colors.white,
                          ),
                           Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(4),
                            ),
                            height: sizeHelper.rW(1.5),
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ))
              .toList(),
        ),
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100]);
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildListCategoryHome(
      List<CategoryProduct> listCategoryHome, SizeHelper sizeHelper) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: listCategoryHome
          .map((item) => buildItemCategory(
              item, listCategoryHome.indexOf(item), sizeHelper))
          .toList(),
    );
  }

  Widget buildItemCategory(
      CategoryProduct categoryProduct, int index, SizeHelper sizeHelper) {
    double _pItemCategory = sizeHelper.rW(1);
    double _wItemCategory = sizeHelper.rW(20);
    double _pTopItemCategory = sizeHelper.rW(2);
    double _pHorizontalItemCategory = sizeHelper.rW(0.5);
    double _hIconItemCategory = sizeHelper.rW(10);
    double _wIconItemCategory = sizeHelper.rW(10);
    double _fItemCategory = sizeHelper.rW(2.7);
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                CategoryDetailScreen(categoryId: categoryProduct.id),
          ),
        );
      },
      child: Padding(
        padding: EdgeInsets.all(_pItemCategory),
        child: Container(
          padding: EdgeInsets.only(
              top: _pTopItemCategory,
              left: _pHorizontalItemCategory,
              right: _pHorizontalItemCategory),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
            // color: ColorConstants.ITEM_CATEGORY_HOME_BG[
            //         index % ColorConstants.ITEM_CATEGORY_HOME_BG.length]
            //     .withAlpha(30),
          ),
          width: _wItemCategory,
          child: Column(
            children: <Widget>[
              Container(
                height: _hIconItemCategory,
                width: _wIconItemCategory,
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                              ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                          .withAlpha(50),
                      ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                              ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                          .withAlpha(200),
                    ],
                  ),
                ),
                child: CachedNetworkImage(
                  imageUrl: categoryProduct.imageUrl,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
              Container(
                padding: EdgeInsets.all(sizeHelper.rW(1)),
                child: Text(
                  categoryProduct.name,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: _fItemCategory,
                    color: ColorConstants.ITEM_CATEGORY_HOME_TEXT,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
