import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/category_product_repository.dart';
import 'package:olmaapp/src/ui/widgets/list_category_home/bloc/bloc.dart';
import 'package:provider/provider.dart';

import 'list_category_home.dart';

class CategoryHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _categoryProductRepository =
        RepositoryProvider.of<CategoryProductRepository>(context);
    return BlocProvider<ListCategoryHomeBloc>(
      create: (context) => ListCategoryHomeBloc(
          categoryProductRepository: _categoryProductRepository),
      child: ListCategoryHome(),
    );
  }
}
