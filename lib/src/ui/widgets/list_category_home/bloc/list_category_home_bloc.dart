import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/category_product.dart';
import 'package:olmaapp/src/repository/category_product_repository.dart';

import 'bloc.dart';

class ListCategoryHomeBloc
    extends Bloc<ListCategoryHomeEvent, ListCategoryHomeState> {
  CategoryProductRepository categoryProductRepository;

  ListCategoryHomeBloc({@required this.categoryProductRepository});

  @override
  // TODO: implement initialState
  ListCategoryHomeState get initialState => ListCategoryHomeInitialState();

  @override
  Stream<ListCategoryHomeState> mapEventToState(
      ListCategoryHomeEvent event) async* {
    if (event is FetchListCategoryHomeEvent) {
      yield ListCategoryHomeLoadingState();
      try {
        List<CategoryProduct> listCategoryHome =
            await categoryProductRepository.getListHome();
        yield ListCategoryHomeLoadedState(listCategoryHome: listCategoryHome);
      } catch (e) {
        yield ListCategoryHomeErrorState(message: e.toString());
      }
    }
  }
}
