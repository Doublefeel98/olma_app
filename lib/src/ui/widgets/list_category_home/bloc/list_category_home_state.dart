import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/category_product.dart';

abstract class ListCategoryHomeState extends Equatable {}

class ListCategoryHomeInitialState extends ListCategoryHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListCategoryHomeLoadingState extends ListCategoryHomeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListCategoryHomeLoadedState extends ListCategoryHomeState {
  List<CategoryProduct> listCategoryHome;

  ListCategoryHomeLoadedState({@required this.listCategoryHome});

  @override
  // TODO: implement props
  List<Object> get props => [listCategoryHome];
}

class ListCategoryHomeErrorState extends ListCategoryHomeState {
  String message;

  ListCategoryHomeErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
