import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/cart_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class CartButton extends StatefulWidget {
  @override
  _CartButtonState createState() => _CartButtonState();
}

class _CartButtonState extends State<CartButton> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<CartBloc, CartState>(
      listener: (context, state) {
        if (state is CartErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
        if (state is CartLoadedState) {
          return buildContainer(state.cart.totalProduct, sizeHelper);
        } else {
          return buildContainer(0, sizeHelper);
        }
      }),
    );
  }

  Widget buildContainer(int totalProduct, SizeHelper sizeHelper) {
    return Stack(
      alignment: Alignment.center,
      children: [
        IconButton(
          icon: Icon(
            Icons.shopping_cart,
            color: Colors.white,
            size: sizeHelper.rW(6),
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => CartScreen(),
            ));
          },
        ),
        Positioned(
          child: Transform.translate(
            offset: Offset(
              sizeHelper.rW(0.5),
              sizeHelper.rW(-2.5),
            ),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: ColorConstants.BLUE_PRIMARY,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(1000),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(1000),
                child: Container(
                  width: sizeHelper.rW(3.5),
                  height: sizeHelper.rW(3.5),
                  alignment: Alignment.center,
                  color: Colors.yellow,
                  child: Text(
                    totalProduct.toString(),
                    style: TextStyle(
                        fontSize: sizeHelper.rW(2),
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
