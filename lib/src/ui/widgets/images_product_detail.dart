import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/images_product_detail_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class ImagesProductDetail extends StatefulWidget {
  final List<ProductImage> _listProductImages;

  ImagesProductDetail({Key key, @required List<ProductImage> listProductImages})
      : assert(listProductImages != null),
        _listProductImages = listProductImages,
        super(key: key);

  @override
  _ImagesProductDetailState createState() => _ImagesProductDetailState();
}

class _ImagesProductDetailState extends State<ImagesProductDetail> {
  int _currentIndex = 0;
  CarouselController _carouselController = CarouselController();
  ScrollController _scrollController = ScrollController();

  List<ProductImage> get _listProductImages => widget._listProductImages;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    Widget itemImageSlider(ProductImage item) => InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ImagesProductDetailScreen(
                  images: _listProductImages,
                  initialPage: _listProductImages.indexOf(item),
                ),
              ),
            );
          },
          child: Container(
            child: CachedNetworkImage(
              imageUrl: item.link,
              width: double.infinity,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        );

    Widget itemIndicatorSlider(ProductImage item) {
      int index = _listProductImages.indexOf(item);
      return InkWell(
        onTap: () {
          _carouselController.animateToPage(index,
              duration: Duration(milliseconds: 200), curve: Curves.linear);
        },
        child: Container(
          width: sizeHelper.rW(15),
          height: sizeHelper.rW(15),
          margin: EdgeInsets.only(left: sizeHelper.rW(1.5)),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: _currentIndex == index
                ? Border.all(color: ColorConstants.BLUE_PRIMARY)
                : Border.all(color: Colors.transparent),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: CachedNetworkImage(
              imageUrl: item.link,
              width: double.infinity,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      );
    }

    final List<Widget> imageSliders =
        _listProductImages.map((item) => itemImageSlider(item)).toList();

    final List<Widget> indicatorSliders = _listProductImages.map((item) {
      return itemIndicatorSlider(item);
    }).toList();

    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
            child: CarouselSlider(
              items: imageSliders,
              carouselController: _carouselController,
              options: CarouselOptions(
                aspectRatio: 1,
                initialPage: 0,
                viewportFraction: 1.0,
                enableInfiniteScroll: false,
                onPageChanged: (index, reason) {
                  double position = index * sizeHelper.rW(15) +
                      sizeHelper.rW(15) / 2 -
                      sizeHelper.rW(100) / 2;
                  _scrollController.animateTo(position,
                      duration: Duration(milliseconds: 200),
                      curve: Curves.linear);
                  setState(() {
                    _currentIndex = index;
                  });
                },
              ),
            ),
          ),
          Container(
            height: sizeHelper.rW(15),
            child: ListView(
              scrollDirection: Axis.horizontal,
              controller: _scrollController,
              children: indicatorSliders,
            ),
          ),
        ],
      ),
    );
  }
}
