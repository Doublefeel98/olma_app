import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/widgets/item_product.dart';

class ListYourProductHome extends StatefulWidget {
  final List<ProductItem> _listProducts;

  ListYourProductHome({Key key, @required List<ProductItem> listProducts})
      : assert(listProducts != null),
        _listProducts = listProducts,
        super(key: key);

  @override
  _ListYourProductHomeState createState() => _ListYourProductHomeState();
}

class _ListYourProductHomeState extends State<ListYourProductHome> {
  List<ProductItem> get _listProducts => widget._listProducts;

  @override
  Widget build(BuildContext context) {
    return SectionWrapper(
      title: "SẢN PHẦM DÀNH CHO BẠN",
      showReadMore: false,
      child: Container(
        child: Wrap(
          children: listYourProduct(_listProducts),
        ),
      ),
    );
  }

  List<Widget> listYourProduct(List<ProductItem> listProducts) {
    return listProducts
        .map((item) => ItemProduct(
              productItem: item,
              type: ItemProductType.NORMAL,
            ))
        .toList();
  }
}
