import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart' hide Banner;
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class BannerImage extends StatefulWidget {
  final Banner banner;

  const BannerImage({Key key, @required this.banner}) : super(key: key);

  @override
  _BannerImageState createState() => _BannerImageState();
}

class _BannerImageState extends State<BannerImage> {
  int _currentIndex = 0;

  Banner get _banner => widget.banner;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            height: sizeHelper.rH(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.elliptical(200.0, 20.0),
                bottomRight: Radius.elliptical(200.0, 20.0),
              ),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    ColorConstants.BLUE_PRIMARY,
                    ColorConstants.GREY_APP_BG
                  ]),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Column(
              children: <Widget>[
                CarouselSlider(
                  items: imageSliders(_banner),
                  options: CarouselOptions(
                      aspectRatio: 8 / 3,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      enlargeCenterPage: true,
                      autoPlayCurve: Curves.fastOutSlowIn,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentIndex = index;
                        });
                      }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: indicatorSliders(_banner, sizeHelper),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget itemImageSlider(BannerImages item) => Container(
        child: Container(
          padding: EdgeInsets.all(5),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(
                  color: ColorConstants.BANNER_HOME_PRIMARY_SHAWDOW,
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: Offset(0, 0),
                )
              ],
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: CachedNetworkImage(
                  imageUrl: item.image,
                  width: double.infinity,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
                //Image.network(item.image, fit: BoxFit.cover, width: 1000.0),
                ),
          ),
        ),
      );

  Widget itemIndicatorSlider(int index, SizeHelper sizeHelper) => Container(
        width: sizeHelper.rW(3),
        height: sizeHelper.rW(3 / 7),
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 2.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(1)),
          color: _currentIndex == index
              ? Color.fromRGBO(0, 0, 0, 0.9)
              : Color.fromRGBO(0, 0, 0, 0.4),
        ),
      );

  List<Widget> imageSliders(Banner banner) {
    return banner.bannerImages.map((item) => itemImageSlider(item)).toList();
  }

  List<Widget> indicatorSliders(Banner banner, SizeHelper sizeHelper) {
    return banner.bannerImages.map((item) {
      int index = banner.bannerImages.indexOf(item);
      return itemIndicatorSlider(index, sizeHelper);
    }).toList();
  }
}
