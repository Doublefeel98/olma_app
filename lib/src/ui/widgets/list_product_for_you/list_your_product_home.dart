import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/item_product.dart';

import 'bloc/bloc.dart';

class ListYourProductHome extends StatefulWidget {
  @override
  _ListYourProductHomeState createState() => _ListYourProductHomeState();
}

class _ListYourProductHomeState extends State<ListYourProductHome> {
  _ListYourProductHomeState() {}

  ListProductForYouBloc _listProductForYouBloc;

  List<ProductItem> _listProductItem;

  int page = 1;

  @override
  void initState() {
    super.initState();
    _listProductForYouBloc = BlocProvider.of<ListProductForYouBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<ListProductForYouBloc, ListProductForYouState>(
      listener: (context, state) {
        if (state is ListProductForYouLoadedState) {
          setState(() {
            _listProductItem = state.listProductForYou.listProducts;
          });
        } else if (state is ListProductForYouLoadedMoreState) {
          setState(() {
            page++;
            _listProductItem = state.listProducts;
          });
        }
      },
      child: BlocBuilder<ListProductForYouBloc, ListProductForYouState>(
          builder: (context, state) {
        Widget widget;
        if (state is ListProductForYouLoadedState ||
            state is ListProductForYouLoadedMoreState) {
          widget = buildListProductForYou(sizeHelper);
        } else if (state is ListProductForYouLoadingMoreState) {
          if (_listProductItem == null) {}
          widget = buildListProductForYou(sizeHelper);
        } else if (state is ListProductForYouErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildContainer(widget);
      }),
    );
  }

  List<Widget> listYourProduct() {
    if (_listProductItem != null) {
      List<Widget> _list = _listProductItem
          .map((item) => ItemProduct(
                productItem: item,
                type: ItemProductType.NORMAL,
              ))
          .toList();
      return _list;
    }
    return <Widget>[Container()];
  }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildListProductForYou(SizeHelper sizeHelper,
      {bool isLoadingMore = false}) {
    return SectionWrapper(
      title: "SẢN PHẦM DÀNH CHO BẠN",
      showReadMore: false,
      child: Container(
        child: Wrap(
          children: listYourProduct(),
        ),
      ),
    );
  }
}
