import 'package:equatable/equatable.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class ListProductForYouEvent extends Equatable {}

class FetchListProductForYouEvent extends ListProductForYouEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoadMoreListProductForYouEvent extends ListProductForYouEvent {
  final int page;
  final List<ProductItem> listProducts;

  LoadMoreListProductForYouEvent({this.page, this.listProducts});

  @override
  // TODO: implement props
  List<Object> get props => [page];
}
