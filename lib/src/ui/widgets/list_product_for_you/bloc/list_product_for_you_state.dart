import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class ListProductForYouState extends Equatable {}

class ListProductForYouInitialState extends ListProductForYouState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListProductForYouLoadingState extends ListProductForYouState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListProductForYouLoadedState extends ListProductForYouState {
  final ListProductForYou listProductForYou;

  ListProductForYouLoadedState({@required this.listProductForYou});

  @override
  // TODO: implement props
  List<Object> get props => [listProductForYou];
}

class ListProductForYouLoadingMoreState extends ListProductForYouState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListProductForYouLoadedMoreState extends ListProductForYouState {
  final List<ProductItem> listProducts;

  final bool isFinish;

  final int page;

  ListProductForYouLoadedMoreState(
      {@required this.listProducts,
      @required this.page,
      this.isFinish = false});

  @override
  // TODO: implement props
  List<Object> get props => [listProducts, page, isFinish];
}

class ListProductForYouErrorState extends ListProductForYouState {
  final String message;

  ListProductForYouErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
