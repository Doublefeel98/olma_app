import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/repository/product_repository.dart';

import 'bloc.dart';

class ListProductForYouBloc
    extends Bloc<ListProductForYouEvent, ListProductForYouState> {
  ProductRepository productRepository;

  ListProductForYouBloc({@required this.productRepository});

  @override
  // TODO: implement initialState
  ListProductForYouState get initialState => ListProductForYouInitialState();

  @override
  Stream<ListProductForYouState> mapEventToState(
      ListProductForYouEvent event) async* {
    if (event is FetchListProductForYouEvent) {
      yield ListProductForYouLoadingState();
      try {
        ListProductForYou listProductForYou = await productRepository
            .getListProductForYou(order: OrderBy.QUANTITY_SOLD);
        yield ListProductForYouLoadedState(
            listProductForYou: listProductForYou);
      } catch (e) {
        yield ListProductForYouErrorState(message: e.toString());
      }
    } else if (event is LoadMoreListProductForYouEvent) {
      yield ListProductForYouLoadingMoreState();
      try {
        ListProductForYou listProductForYou =
            await productRepository.getListProductForYou(
                order: OrderBy.QUANTITY_SOLD, page: event.page);
        List<ProductItem> listProductItem = event.listProducts;

        bool isFinish = false;
        if (listProductForYou.listProducts.length > 0) {
          listProductItem.addAll(listProductForYou.listProducts);
        } else {
          isFinish = true;
        }
        yield ListProductForYouLoadedMoreState(
            listProducts: listProductItem,
            page: event.page,
            isFinish: isFinish);
      } catch (e) {
        yield ListProductForYouErrorState(message: e.toString());
      }
    }
  }
}
