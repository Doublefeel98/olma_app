import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/ui/widgets/list_product_for_you/bloc/bloc.dart';

import 'list_your_product_home.dart';

class ListProductForYou extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _productRepository =
        RepositoryProvider.of<ProductRepository>(context);
    return BlocProvider<ListProductForYouBloc>(
      create: (context) =>
          ListProductForYouBloc(productRepository: _productRepository),
      child: ListYourProductHome(),
    );
  }
}
