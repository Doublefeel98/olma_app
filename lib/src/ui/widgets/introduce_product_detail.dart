import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class IntroduceProductDetail extends StatefulWidget {
  final String _productInfo;

  IntroduceProductDetail({Key key, @required String productInfo})
      : assert(productInfo != null),
        _productInfo = productInfo,
        super(key: key);

  @override
  _IntroduceProductDetailState createState() => _IntroduceProductDetailState();
}

class _IntroduceProductDetailState extends State<IntroduceProductDetail> {
  String get _productInfo => widget._productInfo;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return SectionWrapper(
      title: "Giới thiệu sản phẩm",
      showReadMore: false,
      child: Html(
        data: """
              $_productInfo
            """,
        style: {
          "p": Style(
            fontSize: FontSize(sizeHelper.rW(4)),
            textAlign: TextAlign.justify,
          )
        },
      ),
    );
  }
}
