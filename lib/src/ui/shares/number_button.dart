import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

typedef void NumberButtonChangeCallback(int count);

class NumberButton extends StatefulWidget {
  final int initValue;
  final int minValue;
  final int maxValue;
  final NumberButtonChangeCallback onChange;

  const NumberButton(
      {Key key,
      this.initValue = 1,
      this.minValue = 1,
      this.maxValue = 10000,
      this.onChange})
      : super(key: key);
  @override
  _NumberButtonState createState() => _NumberButtonState();
}

class _NumberButtonState extends State<NumberButton> {
  int _value;
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._value = widget.initValue;
    this._controller.text = widget.initValue.toString();
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black12,
        ),
        borderRadius: BorderRadius.circular(
          sizeHelper.rW(0.5),
        ),
      ),
      child: Row(
        children: [
          InkWell(
            onTap: _minusButtonTap,
            child: Container(
              width: sizeHelper.rW(5),
              height: sizeHelper.rW(5),
              child: Icon(
                Icons.remove,
                size: sizeHelper.rW(4.5),
                color: ColorConstants.GREY_TEXT,
              ),
            ),
          ),
          SizedBox(
            width: sizeHelper.rW(10),
            height: sizeHelper.rW(5),
            child: Container(
              color: Color.fromARGB(25, 100, 100, 100),
              child: TextFormField(
                textAlign: TextAlign.center,
                textAlignVertical: TextAlignVertical.center,
                keyboardType: TextInputType.number,
                controller: _controller,
                readOnly: true,
                style: TextStyle(
                  fontSize: sizeHelper.rW(3),
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: _plusButtonTap,
            child: Container(
              width: sizeHelper.rW(5),
              height: sizeHelper.rW(5),
              child: Icon(
                Icons.add,
                size: sizeHelper.rW(4.5),
                color: ColorConstants.GREY_TEXT,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _plusButtonTap() {
    if (_value >= widget.maxValue) return;

    int newValue = _value + 1;
    _controller.text = (newValue).toString();
    setState(() {
      _value = newValue;
    });
    widget.onChange(newValue);
  }

  _minusButtonTap() {
    if (_value <= widget.minValue) return;

    int newValue = _value - 1;
    _controller.text = (newValue).toString();
    setState(() {
      _value = newValue;
    });
    widget.onChange(newValue);
  }
}
