import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class SectionWrapper extends StatelessWidget {
  final String title;
  final bool showReadMore;
  final Widget child;
  final bool hasTitleMarginBottom;
  final bool hasMarginBottom;
  final Function onTapReadMore;
  final String textReadMore;

  SectionWrapper({
    Key key,
    this.child,
    this.title = '',
    this.showReadMore = true,
    this.hasTitleMarginBottom = true,
    this.hasMarginBottom = true,
    this.onTapReadMore = null,
    this.textReadMore = "XEM THÊM",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    double _fTitleSection = sizeHelper.rT(3.5);
    double _fReadMoreSeaction = sizeHelper.rW(3.0);
    double _fIcontReadMoreSection = sizeHelper.rT(4.4);

    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(3)),
      margin: this.hasMarginBottom
          ? EdgeInsets.only(bottom: sizeHelper.rW(3))
          : EdgeInsets.zero,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          title != ''
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2.5)),
                  margin: this.hasTitleMarginBottom
                      ? EdgeInsets.only(bottom: sizeHelper.rW(3))
                      : EdgeInsets.zero,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          title.toUpperCase(),
                          style: TextStyle(
                            fontSize: _fTitleSection,
                            fontWeight: FontWeight.w500,
                            color: ColorConstants.BLUE_PRIMARY_TEXT_LIGHT,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      showReadMore
                          ? InkWell(
                              onTap: onTapReadMore,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    this.textReadMore,
                                    style: TextStyle(
                                      fontSize: _fReadMoreSeaction,
                                      fontWeight: FontWeight.w400,
                                      color: ColorConstants.GREY_TEXT,
                                    ),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_right,
                                    color: ColorConstants.GREY_TEXT,
                                    size: _fIcontReadMoreSection,
                                  )
                                ],
                              ),
                            )
                          : Container(),
                    ],
                  ),
                )
              : Container(),
          child
        ],
      ),
    );
  }
}
