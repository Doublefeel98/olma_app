import 'package:flutter/material.dart';

class ColorConstants{
  static const GREY_APP_BG = Color.fromARGB(22, 222, 222, 222);
  static const BLUE_PRIMARY = Color.fromARGB(255, 8, 109, 152);
  static const BLUE_PRIMARY_MEDIUM = Color.fromARGB(170, 8, 109, 152);
  static const BLUE_PRIMARY_LIGHT = Color.fromARGB(100, 8, 109, 152);
  static const BANNER_HOME_PRIMARY_SHAWDOW = Color.fromARGB(100, 50, 50, 50);
  static const BLUE_PRIMARY_TEXT = Color.fromARGB(200, 8, 109, 152);
  static const BLUE_PRIMARY_TEXT_LIGHT = Color.fromARGB(200, 8, 109, 152);
  static const GREY_TEXT = Color.fromARGB(200, 20, 20, 20);
  static const ITEM_CATEGORY_HOME_SHAWDOW = Color.fromARGB(70, 0, 0, 0);
  static const ITEM_CATEGORY_HOME_TEXT = Color.fromARGB(200, 0, 0, 0);
  static const ITEM_CATEGORY_HOME_BG = [
    Color.fromARGB(255, 150, 220, 150),
    Color.fromARGB(255, 150, 150, 220),
    Color.fromARGB(255, 230, 224, 87),
    Color.fromARGB(255, 80, 180, 230),
    Color.fromARGB(255, 219, 88, 180),
    Color.fromARGB(255, 33, 73, 220),
    Color.fromARGB(255, 107, 196, 51),
    Color.fromARGB(255, 222, 118, 27),
    Color.fromARGB(255, 27, 222, 202),
  ];
  static const COMMING_SOON = Color.fromARGB(255, 49, 150, 218);
  static const IN_STOCK = Color.fromARGB(255, 34, 144, 0);
  static const OUT_OF_STOCK = Color.fromARGB(255, 189, 79, 64);

  static const ITEM_PRODUCT_TEXT = Color.fromARGB(200, 0, 0, 0);

}