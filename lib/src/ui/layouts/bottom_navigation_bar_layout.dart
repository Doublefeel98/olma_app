import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';

class BottomNavigationBarLayout {
  static BottomNavigationBar _instace;

  static BottomNavigationBar getInstace(BuildContext context, int currentIndex,
      Function setStateCurrentIndexApp) {
    if (_instace == null || currentIndex != _instace.currentIndex) {
      _instace = BottomNavigationBar(
        selectedItemColor: ColorConstants.BLUE_PRIMARY,
        selectedFontSize: 11,
        unselectedFontSize: 10,
        unselectedItemColor: Color.fromARGB(175, 0, 0, 0),
        iconSize: 23,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Trang chủ'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            title: Text('Khuyến mãi'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.share),
            title: Text('Olma share'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            title: Text('Thông báo'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Tôi'),
          ),
        ],
        currentIndex: currentIndex,
        onTap: (index) {
          setStateCurrentIndexApp(index);
        },
      );
    }

    return _instace;
  }
}
