import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/search/search_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';

class AppBarLayout {
  static AppBar _instace;

  static AppBar getInstace(BuildContext context, int index) {
    SizeHelper sizeHelper = SizeHelper(context);

    Widget getBodyAppBar(int index) {
      Widget widget;
      switch (index) {
        case 0:
          widget = InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SearchScreen(),
              ));
            },
            child: Container(
              height: sizeHelper.rW(10),
              margin: EdgeInsets.only(left: sizeHelper.rW(2)),
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(
                  vertical: 0.0, horizontal: sizeHelper.rW(2)),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.all(Radius.circular(sizeHelper.rW(0.5)))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.search,
                    size: sizeHelper.rW(4.5),
                    color: Colors.black54,
                  ),
                  Padding(padding: EdgeInsets.only(right: sizeHelper.rW(1.5))),
                  Text(
                    "Tìm kiếm trên Olma",
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
            ),
          );
          break;

        case 1:
          widget = Container(
            padding: EdgeInsets.only(left: sizeHelper.rW(2)),
            child: Text(
              "Chiến dịch khuyến mãi",
              style: TextStyle(fontSize: sizeHelper.rW(4.5)),
            ),
          );
          break;

        case 2:
          widget = Container(
            padding: EdgeInsets.only(left: sizeHelper.rW(2)),
            child: Text(
              "Olma share",
              style: TextStyle(fontSize: sizeHelper.rW(4.5)),
            ),
          );
          break;

        case 3:
          widget = Container(
            padding: EdgeInsets.only(left: sizeHelper.rW(2)),
            child: Text(
              "Thông báo",
              style: TextStyle(fontSize: sizeHelper.rW(4.5)),
            ),
          );
          break;

        case 4:
          widget = Container(
            padding: EdgeInsets.only(left: sizeHelper.rW(2)),
            child: Text(
              "Cá nhân",
              style: TextStyle(fontSize: sizeHelper.rW(4.5)),
            ),
          );
          break;

        default:
      }
      return widget;
    }

    _instace = AppBar(
      backgroundColor: ColorConstants.BLUE_PRIMARY,
      titleSpacing: 0.0,
      elevation: 0.0,
      title: getBodyAppBar(index),
      actions: [CartButton()],
    );

    return _instace;
  }
}
