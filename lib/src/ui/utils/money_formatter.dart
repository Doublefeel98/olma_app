import 'package:flutter_money_formatter/flutter_money_formatter.dart';

class MoneyFormatter {
  final int amount;
  final String symbol;
  FlutterMoneyFormatter _money;

  MoneyFormatter({this.amount, this.symbol = '₫'}) {
    this._money = FlutterMoneyFormatter(
      amount: double.parse(this.amount.toString()),
      settings: MoneyFormatterSettings(
        symbol: this.symbol,
        thousandSeparator: '.',
        decimalSeparator: ',',
        symbolAndNumberSeparator: '',
        fractionDigits: 0,
        compactFormatType: CompactFormatType.long
      ),
    );
  }

  @override
  String toString() {
    return this._money.output.symbolOnRight;
  }
}
