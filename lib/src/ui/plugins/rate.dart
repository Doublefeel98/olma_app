import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class Rate extends StatefulWidget {
  final double initialRating;
  final MainAxisAlignment align;
  final double size;

  Rate(
      {this.initialRating,
      this.size = 5,
      this.align = MainAxisAlignment.start});

  @override
  _RateState createState() => _RateState(
      initialRating: this.initialRating, align: this.align, size: this.size);
}

class _RateState extends State<Rate> {
  final double initialRating;
  final MainAxisAlignment align;
  final double size;

  _RateState({this.initialRating, this.align, this.size});

  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    List<Widget> showStars() {
      List<Widget> stars = [];
      for (var i = 0; i < 5; i++) {
        stars.add(Padding(
          padding: EdgeInsets.symmetric(
              vertical: sizeHelper.rW(0.5), horizontal: sizeHelper.rW(0.1)),
          child: Icon(
            Icons.star,
            color: i < this.initialRating
                ? Color.fromARGB(255, 250, 220, 50)
                : Color.fromARGB(255, 150, 150, 150),
            size: sizeHelper.rW(size),
          ),
        ));
      }
      return stars;
    }

    return Container(
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: this.align,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(0.5),
                    horizontal: sizeHelper.rW(0.1)),
                child: Icon(
                  Icons.star,
                  color: Color.fromARGB(255, 150, 150, 150),
                  size: sizeHelper.rW(size),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(0.5),
                    horizontal: sizeHelper.rW(0.1)),
                child: Icon(
                  Icons.star,
                  color: Color.fromARGB(255, 150, 150, 150),
                  size: sizeHelper.rW(size),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(0.5),
                    horizontal: sizeHelper.rW(0.1)),
                child: Icon(
                  Icons.star,
                  color: Color.fromARGB(255, 150, 150, 150),
                  size: sizeHelper.rW(size),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(0.5),
                    horizontal: sizeHelper.rW(0.1)),
                child: Icon(
                  Icons.star,
                  color: Color.fromARGB(255, 150, 150, 150),
                  size: sizeHelper.rW(size),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: sizeHelper.rW(0.5),
                    horizontal: sizeHelper.rW(0.1)),
                child: Icon(
                  Icons.star,
                  color: Color.fromARGB(255, 150, 150, 150),
                  size: sizeHelper.rW(size),
                ),
              ),
            ],
          ),
          Container(
            child: ClipRect(
              child: Align(
                alignment: Alignment.topLeft,
                widthFactor: this.initialRating / 5,
                child: Row(
                  mainAxisAlignment: this.align,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.5),
                          horizontal: sizeHelper.rW(0.1)),
                      child: Icon(
                        Icons.star,
                        color: Color.fromARGB(255, 250, 220, 50),
                        size: sizeHelper.rW(size),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.5),
                          horizontal: sizeHelper.rW(0.1)),
                      child: Icon(
                        Icons.star,
                        color: Color.fromARGB(255, 250, 220, 50),
                        size: sizeHelper.rW(size),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.5),
                          horizontal: sizeHelper.rW(0.1)),
                      child: Icon(
                        Icons.star,
                        color: Color.fromARGB(255, 250, 220, 50),
                        size: sizeHelper.rW(size),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.5),
                          horizontal: sizeHelper.rW(0.1)),
                      child: Icon(
                        Icons.star,
                        color: Color.fromARGB(255, 250, 220, 50),
                        size: sizeHelper.rW(size),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(0.5),
                          horizontal: sizeHelper.rW(0.1)),
                      child: Icon(
                        Icons.star,
                        color: Color.fromARGB(255, 250, 220, 50),
                        size: sizeHelper.rW(size),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
