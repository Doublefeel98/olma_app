import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class SaleProgressBar extends StatelessWidget {
  final int quantity;
  final int quantitySale;

  SaleProgressBar({this.quantity, this.quantitySale});

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return Container(
      height: sizeHelper.rW(3.5),
      width: double.infinity,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Stack(
          children: <Widget>[
            Container(
              color: Color.fromARGB(20, 255, 94, 54),
            ),
            FractionallySizedBox(
              widthFactor:
                  this.quantitySale > 0 ? this.quantity / this.quantitySale : 0,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromARGB(150, 255, 94, 54),
                      Color.fromARGB(250, 255, 94, 54)
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Đã bán ${this.quantity}/${this.quantitySale}",
                    style: TextStyle(
                      color: ColorConstants.GREY_TEXT,
                      fontSize: sizeHelper.rW(2.3),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
