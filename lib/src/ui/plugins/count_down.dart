import 'dart:async';
import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class CountDown extends StatefulWidget {
  final String dueTimeStr;
  CountDown({this.dueTimeStr});

  @override
  _CountDownState createState() => _CountDownState(dueTimeStr: this.dueTimeStr);
}

class _CountDownState extends State<CountDown> {
  final String dueTimeStr;
  Timer _timer;
  int _period;
  Duration _periodDate;

  _CountDownState({this.dueTimeStr}) {
    DateTime now = DateTime.now();
    DateTime dueDate = DateTime.tryParse(dueTimeStr);
    this._period = dueDate.difference(now).inSeconds;
    this._periodDate = Duration(seconds: this._period);
    startTimer();
  }

  void startTimer() {
    this._timer = new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) => setState(
        () {
          if (this._period < 1) {
            timer.cancel();
          } else {
            this._period = this._period - 1;
            this._periodDate = Duration(seconds: this._period);
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    this._timer.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    int days = this._periodDate.inDays;
    int hours = this._periodDate.inHours.remainder(24);
    int minutes = this._periodDate.inMinutes.remainder(60);
    int seconds = this._periodDate.inSeconds.remainder(60);

    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    SizeHelper sizeHelper = SizeHelper(context);

    return Container(
      child: Text(
        "Còn ${twoDigits(days)} ngày ${twoDigits(hours)}:${twoDigits(minutes)}:${twoDigits(seconds)}",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ColorConstants.GREY_TEXT,
          fontSize: sizeHelper.rW(2.8),
        ),
      ),
    );
  }
}
