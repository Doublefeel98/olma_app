import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/product_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/btn_add_to_cart/btn_add_to_cart.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/comments_product_detail_ui.dart';
import 'package:olmaapp/src/ui/widgets/description_product_detail.dart';
import 'package:olmaapp/src/ui/widgets/detail_product_detail.dart';
import 'package:olmaapp/src/ui/widgets/images_product_detail.dart';
import 'package:olmaapp/src/ui/widgets/info_product_detail.dart';
import 'package:olmaapp/src/ui/widgets/introduce_product_detail.dart';
import 'package:olmaapp/src/ui/widgets/suggestion_product_detail.dart';

class ProductDetailUI extends StatefulWidget {
  final int _productId;

  ProductDetailUI({Key key, @required int productId})
      : assert(productId != null),
        _productId = productId,
        super(key: key);
  @override
  _ProductDetailUIState createState() => _ProductDetailUIState();
}

class _ProductDetailUIState extends State<ProductDetailUI> {
  ProductDetailBloc _productDetailBloc;
  CommentsProductDetailBloc _commentsProductDetailBloc;

  int get _productId => widget._productId;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _productDetailBloc = BlocProvider.of<ProductDetailBloc>(context);
    _commentsProductDetailBloc =
        BlocProvider.of<CommentsProductDetailBloc>(context);
    // _productDetailBloc.add(FetchProductDetailEvent(productId: _productId));
    _commentsProductDetailBloc
        .add(FetchCommentsProductDetailEvent(productId: _productId));
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<ProductDetailBloc, ProductDetailState>(
      listener: (context, state) {
        if (state is ProductDetailErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<ProductDetailBloc, ProductDetailState>(
          builder: (context, state) {
        Widget widget;
        if (state is ProductDetailLoadedState) {
          widget = buildProductDetail(state.product, sizeHelper);
        } else if (state is ProductDetailErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildProductDetailContainer(widget);
      }),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildProductDetailContainer(Widget widget) {
    return widget;
  }

  Widget buildProductDetail(Product product, SizeHelper sizeHelper) {
    String btnText = product.stockStatusStr;
    bool isEnableButton =
        product.quantity > 0 && product.stockStatus == StockStatus.InStock;

    Color colorBtnAddCart = ColorConstants.BLUE_PRIMARY;

    if (product.quantity <= 0 ||
        product.stockStatus == StockStatus.OutOfStock) {
      btnText = "HẾT HÀNG";
      colorBtnAddCart = ColorConstants.OUT_OF_STOCK;
    } else if (product.stockStatus == StockStatus.ComingSoon) {
      colorBtnAddCart = ColorConstants.COMMING_SOON;
    } else if (product.stockStatus == StockStatus.InStock) {
      btnText = 'MUA NGAY';
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          product.name,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          CartButton(),
        ],
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: ListView(
              children: <Widget>[
                ImagesProductDetail(
                    listProductImages: product.listProductImages),
                InfoProductDetail(product: product),
                IntroduceProductDetail(productInfo: product.info),
                SuggestionProductDetail(listProducts: product.similarProducts),
                DescriptionProductDetail(
                    listProductDetails: []
                      ..add(new ProductDetail(
                          name: "Thương hiệu", content: product.brand.name))
                      ..add(
                          new ProductDetail(name: "SKU", content: product.sku))
                      ..addAll(product.listProductDetails)),
                DetailProductDetail(
                  listProductTabs: product.listProductTabs,
                ),
                CommentsProductDetailUI(
                  productId: _productId,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: sizeHelper.rW(12),
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.zero,
                ),
                onPressed: isEnableButton
                    ? () {
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext bc) {
                              return BtnAddToCart(
                                  product: product, scaffoldKey: _scaffoldKey);
                            });
                      }
                    : null,
                child: Text(btnText.toUpperCase(),
                    style: TextStyle(
                        fontSize: sizeHelper.rW(4.2),
                        fontWeight: FontWeight.w400)),
                color: colorBtnAddCart,
                textColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildOption() {
    // Flexible(
    //   flex: 5,
    //   child: ListView(
    //     children: [
    //       Container(
    //         padding: EdgeInsets.all(
    //             sizeHelper.rW(2)),
    //         decoration: BoxDecoration(
    //           border: Border(
    //             bottom: BorderSide(
    //               color: Colors.black12,
    //               width: 0.5,
    //             ),
    //           ),
    //         ),
    //         child: Column(
    //           crossAxisAlignment:
    //               CrossAxisAlignment.start,
    //           children: [
    //             Text(
    //               "Màu sắc",
    //               style: TextStyle(
    //                 fontSize: sizeHelper.rW(4),
    //                 color: ColorConstants
    //                     .GREY_TEXT,
    //               ),
    //             ),
    //             SizedBox(
    //               height: sizeHelper.rW(3),
    //             ),
    //             Container(
    //               width: double.infinity,
    //               child: Wrap(
    //                 children: [
    //                   Container(
    //                     padding: EdgeInsets
    //                         .symmetric(
    //                       vertical:
    //                           sizeHelper.rW(1),
    //                       horizontal:
    //                           sizeHelper.rW(4),
    //                     ),
    //                     margin: EdgeInsets.only(
    //                       right:
    //                           sizeHelper.rW(2),
    //                       bottom:
    //                           sizeHelper.rW(2),
    //                     ),
    //                     decoration: BoxDecoration(
    //                         color:
    //                             Colors.black12,
    //                         borderRadius:
    //                             BorderRadius.circular(
    //                                 sizeHelper
    //                                     .rW(1))),
    //                     child: Text(
    //                       "Xanh",
    //                       style: TextStyle(
    //                           fontSize:
    //                               sizeHelper
    //                                   .rW(3.5)),
    //                     ),
    //                   ),
    //                   Container(
    //                     padding: EdgeInsets
    //                         .symmetric(
    //                       vertical:
    //                           sizeHelper.rW(1),
    //                       horizontal:
    //                           sizeHelper.rW(4),
    //                     ),
    //                     margin: EdgeInsets.only(
    //                       right:
    //                           sizeHelper.rW(2),
    //                       bottom:
    //                           sizeHelper.rW(2),
    //                     ),
    //                     decoration: BoxDecoration(
    //                         color:
    //                             Colors.black12,
    //                         borderRadius:
    //                             BorderRadius.circular(
    //                                 sizeHelper
    //                                     .rW(1))),
    //                     child: Text(
    //                       "Đỏ",
    //                       style: TextStyle(
    //                           fontSize:
    //                               sizeHelper
    //                                   .rW(3.5)),
    //                     ),
    //                   ),
    //                   Container(
    //                     padding: EdgeInsets
    //                         .symmetric(
    //                       vertical:
    //                           sizeHelper.rW(1),
    //                       horizontal:
    //                           sizeHelper.rW(4),
    //                     ),
    //                     margin: EdgeInsets.only(
    //                       right:
    //                           sizeHelper.rW(2),
    //                       bottom:
    //                           sizeHelper.rW(2),
    //                     ),
    //                     decoration: BoxDecoration(
    //                         color:
    //                             Colors.black12,
    //                         borderRadius:
    //                             BorderRadius.circular(
    //                                 sizeHelper
    //                                     .rW(1))),
    //                     child: Text(
    //                       "Vàng",
    //                       style: TextStyle(
    //                           fontSize:
    //                               sizeHelper
    //                                   .rW(3.5)),
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             )
    //           ],
    //         ),
    //       ),
    //     ],
    //   ),
    // ),
  }
}
