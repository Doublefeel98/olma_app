import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/comment_repository.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/ui/screens/product_detail/product_detail_ui.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/bloc/bloc.dart';

import 'bloc/bloc.dart';

class ProductDetailScreen extends StatelessWidget {
  final int _productId;

  ProductDetailScreen({Key key, @required int productId})
      : assert(productId != null),
        _productId = productId,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _productRepository =
        RepositoryProvider.of<ProductRepository>(context);
    final _commentRepository =
        RepositoryProvider.of<CommentRepository>(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProductDetailBloc>(
          create: (BuildContext context) =>
              ProductDetailBloc(productRepository: _productRepository)
                ..add(FetchProductDetailEvent(productId: _productId)),
        ),
        // BlocProvider<CommentsProductDetailBloc>(
        //   create: (BuildContext context) =>
        //       CommentsProductDetailBloc(commentRepository: _commentRepository)
        //         ..add(FetchCommentsProductDetailEvent(productId: _productId)),
        // )
      ],
      child: ProductDetailUI(productId: _productId),
    );
  }
}
