import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ProductDetailEvent extends Equatable {
  const ProductDetailEvent();

  @override
  List<Object> get props => [];
}

class FetchProductDetailEvent extends ProductDetailEvent {
  final int productId;

  const FetchProductDetailEvent({@required this.productId});

  @override
  List<Object> get props => [productId];

  @override
  String toString() => 'Product ID { email :$productId }';
}
