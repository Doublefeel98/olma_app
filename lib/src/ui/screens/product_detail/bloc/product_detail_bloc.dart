import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/repository/banner_repository.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'bloc.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  ProductRepository productRepository;

  ProductDetailBloc({@required this.productRepository});

  @override
  // TODO: implement initialState
  ProductDetailState get initialState => ProductDetailInitialState();

  @override
  Stream<ProductDetailState> mapEventToState(ProductDetailEvent event) async* {
    if (event is FetchProductDetailEvent) {
      yield ProductDetailLoadingState();
      try {
        Product product =
            await productRepository.getProductDetail(event.productId);
        yield ProductDetailLoadedState(product: product);
      } catch (e) {
        yield ProductDetailErrorState(message: e.toString());
      }
    }
  }
}
