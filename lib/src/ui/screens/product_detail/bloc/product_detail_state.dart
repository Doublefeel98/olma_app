import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/product.dart';

abstract class ProductDetailState extends Equatable {}

class ProductDetailInitialState extends ProductDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ProductDetailLoadingState extends ProductDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ProductDetailLoadedState extends ProductDetailState {
  Product product;

  ProductDetailLoadedState({@required this.product});

  @override
  // TODO: implement props
  List<Object> get props => [product];
}

class ProductDetailErrorState extends ProductDetailState {
  String message;

  ProductDetailErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
