import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/order_repository.dart';
import 'package:olmaapp/src/ui/screens/purchase/purchase_ui.dart';

import 'bloc/bloc.dart';

enum PurChaseScreenType {
  IN_PROGRESS,
  IN_TRANSPORT,
  DONE,
  CANCEL,
}

class PurchaseScreen extends StatelessWidget {
  final PurChaseScreenType type;

  const PurchaseScreen({Key key, @required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _orderRepository = RepositoryProvider.of<OrderRepository>(context);
    return BlocProvider<PurchaseBloc>(
      create: (context) => PurchaseBloc(orderRepository: _orderRepository)
        ..add(FetchPurchaseEvent()),
      child: PurchaseUI(type: type),
    );
  }
}
