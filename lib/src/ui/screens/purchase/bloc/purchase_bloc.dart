import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/repository/order_repository.dart';
import 'bloc.dart';

class PurchaseBloc extends Bloc<PurchaseEvent, PurchaseState> {
  OrderRepository orderRepository;

  PurchaseBloc({@required this.orderRepository});

  @override
  // TODO: implement initialState
  PurchaseState get initialState => PurchaseInitialState();

  @override
  Stream<PurchaseState> mapEventToState(PurchaseEvent event) async* {
    if (event is FetchPurchaseEvent) {
      yield PurchaseLoadingState();
      try {
        OrderResponse orderResponse = await orderRepository.getListOrder();
        if (orderResponse.success) {
          yield PurchaseLoadedState(orderResponse: orderResponse);
        } else {
          yield PurchaseErrorState(message: orderResponse.message);
        }
      } catch (e) {
        yield PurchaseErrorState(message: e.toString());
      }
    }
  }
}
