import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/models/promotion.dart';

abstract class PurchaseState extends Equatable {}

class PurchaseInitialState extends PurchaseState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PurchaseLoadingState extends PurchaseState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PurchaseLoadedState extends PurchaseState {
  OrderResponse orderResponse;

  PurchaseLoadedState({@required this.orderResponse});

  @override
  // TODO: implement props
  List<Object> get props => [orderResponse];
}

class PurchaseErrorState extends PurchaseState {
  String message;

  PurchaseErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
