import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/detail_purchase_screen.dart';
import 'package:olmaapp/src/ui/screens/purchase/purchase_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

import 'bloc/bloc.dart';

class PurchaseUI extends StatefulWidget {
  final PurChaseScreenType type;

  const PurchaseUI({Key key, @required this.type}) : super(key: key);
  @override
  _PurchaseUIState createState() => _PurchaseUIState(
        type: this.type,
      );
}

class _PurchaseUIState extends State<PurchaseUI> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final PurChaseScreenType type;
  String _title;

  _PurchaseUIState({this.type});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switch (this.type) {
      case PurChaseScreenType.IN_PROGRESS:
        this._title = "Đơn hàng đang xử lý";
        break;
      case PurChaseScreenType.IN_TRANSPORT:
        this._title = "Đơn hàng đang vận chuyển";
        break;
      case PurChaseScreenType.DONE:
        this._title = "Đơn hàng thành công";
        break;
      case PurChaseScreenType.CANCEL:
        this._title = "Đơn hàng đã hủy";
        break;
      default:
        this._title = "Đơn hàng";
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<PurchaseBloc, PurchaseState>(
      listener: (context, state) {
        if (state is PurchaseErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child:
          BlocBuilder<PurchaseBloc, PurchaseState>(builder: (context, state) {
        Widget widget;
        if (state is PurchaseLoadedState) {
          widget = buildListPurchase(state.orderResponse.data, sizeHelper);
        } else if (state is PurchaseErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildPurchaseContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildListPurchase(List<Order> orders, SizeHelper sizeHelper) {
    List<Order> listOrder;
    switch (this.type) {
      case PurChaseScreenType.IN_PROGRESS:
        listOrder = orders
            .where((order) => [
                  OrderStatuses.Pending,
                  OrderStatuses.Processing,
                  OrderStatuses.Processed,
                  OrderStatuses.WaitingConfirm,
                ].contains(order.status))
            .toList();
        break;
      case PurChaseScreenType.IN_TRANSPORT:
        listOrder = orders
            .where((order) => [
                  OrderStatuses.Shipping,
                  OrderStatuses.Shiped,
                ].contains(order.status))
            .toList();
        break;
      case PurChaseScreenType.DONE:
        listOrder = orders
            .where((order) => [
                  OrderStatuses.Complete,
                ].contains(order.status))
            .toList();
        break;
      case PurChaseScreenType.CANCEL:
        listOrder = orders
            .where((order) => [
                  OrderStatuses.Voided,
                  OrderStatuses.Failed,
                  OrderStatuses.Expired,
                  OrderStatuses.Denied,
                  OrderStatuses.Chargeback,
                  OrderStatuses.CanceledReversal,
                  OrderStatuses.Canceled,
                ].contains(order.status))
            .toList();
        break;
      default:
        listOrder = orders;
    }
    return Column(
      children:
          listOrder.map((order) => buildItemOrder(order, sizeHelper)).toList(),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildItemOrder(Order order, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailPurchaseScreen(
                  order: order,
                )));
      },
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
          color: Colors.black26,
          width: 0.5,
        ))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: "Mã đơn hàng: ",
                  style: TextStyle(
                    fontSize: sizeHelper.rW(3.5),
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: order.invoiceNo,
                  style: TextStyle(
                    color: ColorConstants.GREY_TEXT,
                    fontSize: sizeHelper.rW(3.2),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ]),
            ),
            SizedBox(
              height: sizeHelper.rW(1),
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: "Tổng tiền: ",
                  style: TextStyle(
                    fontSize: sizeHelper.rW(3.5),
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: MoneyFormatter(amount: order.totalMoney).toString(),
                  style: TextStyle(
                    color: ColorConstants.GREY_TEXT,
                    fontSize: sizeHelper.rW(3.2),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ]),
            ),
            SizedBox(
              height: sizeHelper.rW(1),
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                  text: "Thời gian đặt mua: ",
                  style: TextStyle(
                    fontSize: sizeHelper.rW(3.5),
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
                TextSpan(
                  text: DateFormat("dd/MM/yyyy HH:mm:ss")
                      .format(order.createdAt.toLocal()),
                  style: TextStyle(
                    color: ColorConstants.GREY_TEXT,
                    fontSize: sizeHelper.rW(3.2),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPurchaseContainer(Widget widget, SizeHelper sizeHelper) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          this._title,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [],
      ),
      body: ListView(
        children: [
          SectionWrapper(
            title: "Danh sách đơn hàng",
            showReadMore: false,
            child: widget,
          ),
        ],
      ),
    );
  }
}
