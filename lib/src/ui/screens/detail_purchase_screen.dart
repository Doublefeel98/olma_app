import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

class DetailPurchaseScreen extends StatefulWidget {
  final Order order;

  const DetailPurchaseScreen({Key key, @required this.order}) : super(key: key);
  @override
  _DetailPurchaseScreenState createState() => _DetailPurchaseScreenState();
}

class _DetailPurchaseScreenState extends State<DetailPurchaseScreen> {
  Order get _order => widget.order;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Chi tiết đơn hàng",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [],
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              vertical: sizeHelper.rW(2),
              horizontal: sizeHelper.rW(3),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Mã đơn hàng: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: _order.invoiceNo,
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Tổng số lượng: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: _order.totalProduction.toString(),
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Tổng tiền: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text:
                          MoneyFormatter(amount: _order.totalMoney).toString(),
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Thời gian đặt mua: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: DateFormat("dd/MM/yyyy HH:mm:ss")
                          .format(_order.createdAt.toLocal()),
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Địa chỉ nhận hàng: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text:
                          "${_order.deliveryAddress.address}, ${_order.deliveryAddress.wardName}, ${_order.deliveryAddress.districtName}, ${_order.deliveryAddress.cityName}",
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Trạng thái: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: _order.statusStr,
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Phương thức thanh toán: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: _order.paymentMethodStr,
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
