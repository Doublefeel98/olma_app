import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/models/promotion.dart';
import 'package:olmaapp/src/ui/screens/list_product_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class PromotionDetailUI extends StatefulWidget {
  final int promotionId;

  const PromotionDetailUI({Key key, this.promotionId}) : super(key: key);

  @override
  _PromotionDetailUIState createState() => _PromotionDetailUIState();
}

class _PromotionDetailUIState extends State<PromotionDetailUI> {
  int get _promotionId => widget.promotionId;
  PromotionDetailBloc _categoryDetailBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _categoryDetailBloc = BlocProvider.of<PromotionDetailBloc>(context);
    _categoryDetailBloc.add(FetchPromotionDetailEvent(
        promotionId: _promotionId, order: OrderBy.QUANTITY_SOLD));
  }

  bool _isFinish = false;

  bool _isFirstTime = true;

  int _page = 1;

  List<ProductItem> _listProducts;

  bool _isLoadingMore = false;

  PromotionForDetail _promotionForDetail;

  String _order = OrderBy.QUANTITY_SOLD;

  bool _isInit = true;

  bool _isReload = true;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocConsumer<PromotionDetailBloc, PromotionDetailState>(
        listenWhen: (previous, current) {
      // return true/false to determine whether or not
      // to invoke listener with state
    }, listener: (context, state) {
      // do stuff here based on BlocA's state
      if (state is PromotionDetailErrorState) {
        _scaffoldKey.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      } else if (state is PromotionDetailLoadingMoreState) {
        setState(() {
          _isLoadingMore = true;
        });
      } else if (state is PromotionDetailLoadingState) {
        setState(() {
          _isReload = true;
        });
      } else if (state is PromotionDetailLoadedState) {
        this.setState(() {
          _promotionForDetail = state.promotionForDetail;
          _listProducts = state.promotionForDetail.listProducts.data;
          _isFirstTime = false;
          _page = 1;
          _order = state.order;

          _isLoadingMore = false;
          _isInit = false;
          _isReload = false;
          _isFinish = state.promotionForDetail.listProducts.lastPage == _page;
        });
      } else if (state is PromotionDetailLoadedMoreState) {
        this.setState(() {
          _page = state.page;
          _isFinish = state.isFinish;
          _listProducts = state.listProductItems;
          _isLoadingMore = false;
        });
      }
    }, buildWhen: (previous, current) {
      // return true/false to determine whether or not
      // to rebuild the widget with state
      print("previous");
      print(previous);
      print("current");
      print(current);
    }, builder: (context, state) {
      // return widget here based on BlocA's state
      // Widget widget;
      // if (state is PromotionDetailInitialState ||
      //     state is PromotionDetailLoadingState) {
      //   widget = buildLoading();
      // } else if (state is PromotionDetailErrorState) {
      //   widget = buildErrorUi(state.message);
      // } else {
      //   widget = buildPromotionDetail(sizeHelper);
      // }
      // return buildContainer(widget);

      if (state is PromotionDetailErrorState) {
        return buildErrorUi(state.message);
      }

      return ListProductScreen(
        isInit: _isInit,
        scaffoldKey: _scaffoldKey,
        title: _promotionForDetail != null ? _promotionForDetail.name : null,
        listProductItem: _listProducts,
        total: _promotionForDetail != null
            ? _promotionForDetail.listProducts.total
            : 0,
        type: ListProductType.PROMOTION,
        isFinish: _isFinish,
        isFirstTime: _isFirstTime,
        page: _page,
        isLoadingMore: _isLoadingMore,
        id: _promotionId,
        order: _order,
        isReload: _isReload,
      );
    });
  }

  // Widget buildPromotionDetail(SizeHelper sizeHelper) {
  //   return ListProductScreen(
  //     title: _promotionForDetail.name,
  //     listProductItem: _listProducts,
  //     total: _promotionForDetail.listProducts.total,
  //     type: ListProductType.PROMOTION,
  //     isFinish: _isFinish,
  //     isFirstTime: _isFirstTime,
  //     page: _page,
  //     isLoading: _isLoading,
  //     id: _promotionId,
  //     order: _order,
  //   );
  // }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}
