import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/promotion_repository.dart';
import 'package:olmaapp/src/ui/screens/promotion_detail/promotion_detail_ui.dart';

import 'bloc/bloc.dart';

class PromotionDetailScreen extends StatelessWidget {
  final int _promotionId;

  PromotionDetailScreen({Key key, @required int promotionId})
      : assert(promotionId != null),
        _promotionId = promotionId,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _promotionRepository =
        RepositoryProvider.of<PromotionRepository>(context);
    return BlocProvider<PromotionDetailBloc>(
      create: (context) =>
          PromotionDetailBloc(promotionRepository: _promotionRepository),
      child: PromotionDetailUI(
        promotionId: _promotionId,
      ),
    );
  }
}
