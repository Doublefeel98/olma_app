import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class PromotionDetailEvent extends Equatable {}

class FetchPromotionDetailEvent extends PromotionDetailEvent {
  final int promotionId;
  final String order;

  FetchPromotionDetailEvent({@required this.promotionId, @required this.order});

  @override
  List<Object> get props => [promotionId];

  @override
  String toString() => '{ promotionId: $promotionId }';
}

class LoadMorePromotionDetailEvent extends PromotionDetailEvent {
  final int page;
  final List<ProductItem> listProductItems;
  final int promotionId;
  final String order;

  LoadMorePromotionDetailEvent(
      {@required this.page,
      @required this.listProductItems,
      @required this.promotionId,
      @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [page];
}
