import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/models/promotion.dart';

abstract class PromotionDetailState extends Equatable {}

class PromotionDetailInitialState extends PromotionDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PromotionDetailLoadingState extends PromotionDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PromotionDetailLoadedState extends PromotionDetailState {
  final PromotionForDetail promotionForDetail;
  final String order;

  PromotionDetailLoadedState(
      {@required this.promotionForDetail, @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [promotionForDetail];
}

class PromotionDetailLoadingMoreState extends PromotionDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PromotionDetailLoadedMoreState extends PromotionDetailState {
  final List<ProductItem> listProductItems;

  final bool isFinish;

  final int page;

  PromotionDetailLoadedMoreState(
      {@required this.listProductItems,
      @required this.page,
      this.isFinish = false});

  @override
  // TODO: implement props
  List<Object> get props => [listProductItems, page, isFinish];
}

class PromotionDetailErrorState extends PromotionDetailState {
  final String message;

  PromotionDetailErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
