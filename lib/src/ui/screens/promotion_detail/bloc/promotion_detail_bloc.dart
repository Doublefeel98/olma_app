import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/models/promotion.dart';
import 'package:olmaapp/src/repository/promotion_repository.dart';

import 'bloc.dart';

class PromotionDetailBloc
    extends Bloc<PromotionDetailEvent, PromotionDetailState> {
  PromotionRepository promotionRepository;

  PromotionDetailBloc({@required this.promotionRepository});

  @override
  // TODO: implement initialState
  PromotionDetailState get initialState => PromotionDetailInitialState();

  @override
  Stream<PromotionDetailState> mapEventToState(
      PromotionDetailEvent event) async* {
    if (event is FetchPromotionDetailEvent) {
      yield PromotionDetailLoadingState();
      try {
        PromotionForDetail promotionForDetail = await promotionRepository
            .getPromotionForDetail(event.promotionId, order: event.order);
        yield PromotionDetailLoadedState(
            promotionForDetail: promotionForDetail, order: event.order);
      } catch (e) {
        yield PromotionDetailErrorState(message: e.toString());
      }
    } else if (event is LoadMorePromotionDetailEvent) {
      yield PromotionDetailLoadingMoreState();
      try {
        PromotionForDetail promotionForDetail =
            await promotionRepository.getPromotionForDetail(event.promotionId,
                order: event.order, page: event.page);
        List<ProductItem> listProductItem = event.listProductItems;

        listProductItem.addAll(promotionForDetail.listProducts.data);

        bool isFinish = false;
        if (event.page == promotionForDetail.listProducts.lastPage) {
          isFinish = true;
        }
        yield PromotionDetailLoadedMoreState(
            listProductItems: listProductItem,
            page: event.page,
            isFinish: isFinish);
      } catch (e) {
        yield PromotionDetailErrorState(message: e.toString());
      }
    }
  }
}
