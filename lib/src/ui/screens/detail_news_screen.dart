import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/category_news.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailNewsScreen extends StatefulWidget {
  News news;

  DetailNewsScreen({@required this.news});

  @override
  _DetailNewsScreenState createState() => _DetailNewsScreenState();
}

class _DetailNewsScreenState extends State<DetailNewsScreen> {
  News get news => widget.news;
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          news.title,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          CartButton(),
        ],
      ),
      body: WebView(
        initialUrl: Uri.dataFromString("""
                <html>
                <head>
                  <link rel="stylesheet" href="https://olma.vn/client/dist/css/desktop/main.css"/>
                </head>
                <body style="padding: 0 15px; overflow-y: auto">
                  <div style="display: flex; justify-content: space-between;padding:15px 0">
                    <p style="font-size: 17px;color: rgba(8,109,152,.8)">${news.categoryNews.name}</p>
                    <p style="font-size: 17px;">${news.createdAt}</p>
                  </div>
                  <p style="font-size: 27px;">${news.title}</p>
                  <div class="ql-editor" style="text-align: justify;overflow-y: unset;padding: 0;height: unset; margin-bottom: 10px;">
                    ${news.content}
                  </div>
                  <p style="font-size: 16px; font-weight: 500; text-align: right;margin-bottom: 15px;">${news.author.name}</p>
                </body>
                </html>
                """, mimeType: 'text/html', encoding: utf8).toString(),
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
