import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterState get initialState => RegisterState.initial();

  @override
  Stream<Transition<RegisterEvent, RegisterState>> transformEvents(
    Stream<RegisterEvent> events,
    TransitionFunction<RegisterEvent, RegisterState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! RegisterNameChanged &&
          event is! RegisterEmailChanged &&
          event is! RegisterPasswordChanged &&
          event is! RegisterConfirmPasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is RegisterNameChanged ||
          event is RegisterEmailChanged ||
          event is RegisterPasswordChanged ||
          event is RegisterConfirmPasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterNameChanged) {
      yield* _mapRegisterNameChangedToState(event.name);
    } else if (event is RegisterEmailChanged) {
      yield* _mapRegisterEmailChangedToState(event.email);
    } else if (event is RegisterPasswordChanged) {
      yield* _mapRegisterPasswordChangedToState(event.password);
    } else if (event is RegisterConfirmPasswordChanged) {
      yield* _mapRegisterConfirmPasswordChangedToState(
          event.password, event.confirmPassword);
    } else if (event is RegisterSubmitted) {
      yield* _mapRegisterSubmittedToState(
          event.name, event.email, event.password, event.confirmPassword);
    }
  }

  Stream<RegisterState> _mapRegisterNameChangedToState(String name) async* {
    yield state.update(
      isNameValid: name.isNotEmpty,
    );
  }

  Stream<RegisterState> _mapRegisterEmailChangedToState(String email) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<RegisterState> _mapRegisterPasswordChangedToState(
      String password) async* {
    yield state.update(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }

  Stream<RegisterState> _mapRegisterConfirmPasswordChangedToState(
      String password, String confirmPassword) async* {
    yield state.update(
      isConfirmPasswordValid:
          Validators.isValidConfirmPassword(password, confirmPassword),
    );
  }

  Stream<RegisterState> _mapRegisterSubmittedToState(String name, String email,
      String password, String confirmPassword) async* {
    yield RegisterState.loading();
    try {
      ResultUserResponse response = await _userRepository.signUp(
        name: name,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      );
      if (response.success) {
        yield RegisterState.success(response.message);
      } else {
        yield RegisterState.failure(response.message);
      }
    } catch (e) {
      yield RegisterState.failure(e.toString());
    }
  }
}
