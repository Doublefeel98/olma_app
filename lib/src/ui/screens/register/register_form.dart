import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/dialog_alert.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  RegisterBloc _registerBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool get isPopulated =>
      _nameController.text.isNotEmpty &&
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _nameController.addListener(_onNameChanged);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onConfirmPasswordChanged);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  void _onNameChanged() {
    _registerBloc.add(
      RegisterNameChanged(name: _nameController.text),
    );
  }

  void _onEmailChanged() {
    _registerBloc.add(
      RegisterEmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _registerBloc.add(
      RegisterPasswordChanged(password: _passwordController.text),
    );
  }

  void _onConfirmPasswordChanged() {
    _registerBloc.add(
      RegisterConfirmPasswordChanged(
          password: _passwordController.text,
          confirmPassword: _confirmPasswordController.text),
    );
  }

  void _onFormSubmitted() {
    _registerBloc.add(
      RegisterSubmitted(
        name: _nameController.text,
        email: _emailController.text,
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state.isSubmitting) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang đăng kí tài khoản...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          _scaffoldKey.currentState..hideCurrentSnackBar();
          DialogAlert.showMyDialog(
              context: context,
              title: "Thông báo",
              content: state.message,
              onPress: () {
                Navigator.of(context).pop();
              });
        }
        if (state.isFailure) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(state.message),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child:
          BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Đăng ký",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          body: Stack(
            children: [
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      ColorConstants.BLUE_PRIMARY,
                      ColorConstants.BLUE_PRIMARY_MEDIUM,
                      ColorConstants.BLUE_PRIMARY_LIGHT
                    ],
                  ),
                ),
              ),
              Container(
                child: SingleChildScrollView(
                  padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(7)),
                        alignment: Alignment.center,
                        child: CachedNetworkImage(
                          imageUrl:
                              "https://olma.s3-ap-southeast-1.amazonaws.com/images/olma_logo2.png",
                          width: sizeHelper.rW(15),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Tên người dùng",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              controller: _nameController,
                              validator: (_) {
                                return !state.isNameValid
                                    ? 'Tên không hợp lệ'
                                    : null;
                              },
                              textAlignVertical: TextAlignVertical.center,
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.text,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.person,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập tên của bạn",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Email",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              controller: _emailController,
                              validator: (_) {
                                return !state.isPasswordValid
                                    ? 'Email không hợp lệ'
                                    : null;
                              },
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.emailAddress,
                              textAlignVertical: TextAlignVertical.center,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.email,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập email của bạn",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Mật khẩu",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              controller: _passwordController,
                              validator: (_) {
                                return !state.isPasswordValid
                                    ? 'Mật khẩu tối thiểu 6 kí tự'
                                    : null;
                              },
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.vpn_key,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập mật khẩu của bạn",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Nhập lại mật khẩu",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              controller: _confirmPasswordController,
                              validator: (_) {
                                return !state.isConfirmPasswordValid
                                    ? 'Mật khẩu xác nhận không trùng khớp'
                                    : null;
                              },
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.vpn_key,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập lại mật khẩu",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        height: sizeHelper.rW(11),
                        child: Expanded(
                          child: RaisedButton(
                            onPressed: isRegisterButtonEnabled(state)
                                ? _onFormSubmitted
                                : null,
                            child: Text(
                              "Đăng ký".toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: sizeHelper.rW(4)),
                            ),
                            color: ColorConstants.BLUE_PRIMARY,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(sizeHelper.rW(1))),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
