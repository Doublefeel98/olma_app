import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/product_detail/product_detail_screen.dart';
import 'package:olmaapp/src/ui/screens/result_search/result_search_screen.dart';
import 'package:olmaapp/src/ui/screens/search/bloc/bloc.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';

class SearchUI extends StatefulWidget {
  @override
  _SearchUIState createState() => _SearchUIState();
}

class _SearchUIState extends State<SearchUI> {
  TextEditingController _searchController = new TextEditingController();

  ListSuggestSearchBloc _listSuggestSearchBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _listSuggestSearchBloc = BlocProvider.of<ListSuggestSearchBloc>(context);
    _searchController.addListener(_onSearchChange);
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  Future<void> _onSearchChange() {
    String keyword = _searchController.text;
    if (keyword.isNotEmpty) {
      Timer(Duration(milliseconds: 300), () {
        _listSuggestSearchBloc
            .add(FetchListSuggestSearchEvent(keyword: _searchController.text));
      });
    } else {
      setState(() {
        _suggestion = [];
      });
    }
    setState(() {
      _keyword = keyword;
    });
  }

  List<ProductItem> _suggestion = [];
  String _keyword = "";

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<ListSuggestSearchBloc, ListSuggestSearchState>(
      listener: (context, state) {
        if (state is ListSuggestSearchErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        } else if (state is ListSuggestSearchLoadedState) {
          setState(() {
            _suggestion = state.listProductSuggest;
          });
        }
      },
      child: BlocBuilder<ListSuggestSearchBloc, ListSuggestSearchState>(
          builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Container(
              height: sizeHelper.rW(10),
              padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.all(Radius.circular(sizeHelper.rW(0.5)))),
              child: Theme(
                data: Theme.of(context).copyWith(primaryColor: Colors.black),
                child: TextFormField(
                  autofocus: true,
                  autocorrect: true,
                  controller: _searchController,
                  style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (value) {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ResultSearchScreen(
                          search: value,
                        ),
                      ),
                    );
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(width: 0.0, style: BorderStyle.none),
                    ),
                    hintText: 'Tìm kiếm trên Olma',
                    hintStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w400,
                    ),
                    icon: Icon(
                      Icons.search,
                      size: sizeHelper.rW(4.5),
                      color: Colors.black54,
                    ),
                    contentPadding: EdgeInsets.only(left: -10),
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              CartButton(),
            ],
          ),
          body: Container(
            child: ListView(
              children: _suggestion
                  .map(
                    (productItem) => InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                ProductDetailScreen(productId: productItem.id),
                          ),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          vertical: sizeHelper.rW(3),
                          horizontal: sizeHelper.rW(4),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.black12,
                              width: 0.5,
                            ),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text(
                                productItem.name,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                            Icon(
                              Icons.open_in_new,
                              size: sizeHelper.rW(3),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        );
      }),
    );
  }
}
