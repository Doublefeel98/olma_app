import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/ui/screens/search/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/search/search_ui.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _productRepository =
        RepositoryProvider.of<ProductRepository>(context);
    return BlocProvider<ListSuggestSearchBloc>(
      create: (context) =>
          ListSuggestSearchBloc(productRepository: _productRepository),
      child: SearchUI(),
    );
  }
}
