import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/repository/product_repository.dart';

import 'bloc.dart';

class ListSuggestSearchBloc
    extends Bloc<ListSuggestSearchEvent, ListSuggestSearchState> {
  ProductRepository productRepository;

  ListSuggestSearchBloc({@required this.productRepository});

  @override
  // TODO: implement initialState
  ListSuggestSearchState get initialState => ListSuggestSearchInitialState();

  @override
  Stream<ListSuggestSearchState> mapEventToState(
      ListSuggestSearchEvent event) async* {
    if (event is FetchListSuggestSearchEvent) {
      yield ListSuggestSearchLoadingState();
      try {
        List<ProductItem> listProductSuggest =
            await productRepository.getListProductSuggestFilter(event.keyword);
        yield ListSuggestSearchLoadedState(
            listProductSuggest: listProductSuggest);
      } catch (e) {
        yield ListSuggestSearchErrorState(message: e.toString());
      }
    }
  }
}
