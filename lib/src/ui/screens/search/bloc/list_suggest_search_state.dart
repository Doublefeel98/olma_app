import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class ListSuggestSearchState extends Equatable {}

class ListSuggestSearchInitialState extends ListSuggestSearchState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListSuggestSearchLoadingState extends ListSuggestSearchState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ListSuggestSearchLoadedState extends ListSuggestSearchState {
  final List<ProductItem> listProductSuggest;

  ListSuggestSearchLoadedState({@required this.listProductSuggest});

  @override
  // TODO: implement props
  List<Object> get props => [listProductSuggest];
}

class ListSuggestSearchErrorState extends ListSuggestSearchState {
  final String message;

  ListSuggestSearchErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
