import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class ListSuggestSearchEvent extends Equatable {}

class FetchListSuggestSearchEvent extends ListSuggestSearchEvent {
  final String keyword;

  FetchListSuggestSearchEvent({@required this.keyword});
  @override
  // TODO: implement props
  List<Object> get props => null;
}
