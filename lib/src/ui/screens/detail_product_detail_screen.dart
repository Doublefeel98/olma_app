import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailProductDetailScreen extends StatefulWidget {
  final String title;
  final String content;

  DetailProductDetailScreen({Key key, this.title, this.content})
      : super(key: key);
  @override
  _DetailProductDetailScreenState createState() =>
      _DetailProductDetailScreenState(title: this.title, content: this.content);
}

class _DetailProductDetailScreenState extends State<DetailProductDetailScreen> {
  final String title;
  final String content;

  _DetailProductDetailScreenState({this.title, this.content});
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          this.title,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: Container(
        child: WebView(
          initialUrl: Uri.dataFromString("""
          <html>
          <head>
            <link rel="stylesheet" href="https://olma.vn/client/dist/css/desktop/main.css"/>
          </head>
          <body>
            <div class="ql-editor">$content</div>
          </body>
          </html>
          """, mimeType: 'text/html', encoding: utf8).toString(),
          javascriptMode: JavascriptMode.unrestricted,
        ),
      ),
    );
  }
}
