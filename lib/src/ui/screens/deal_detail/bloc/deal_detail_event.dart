import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class DealDetailEvent extends Equatable {}

class FetchDealDetailEvent extends DealDetailEvent {
  final int dealId;
  final String order;

  FetchDealDetailEvent({@required this.dealId, @required this.order});

  @override
  List<Object> get props => [dealId];

  @override
  String toString() => '{ dealId: $dealId }';
}

class LoadMoreDealDetailEvent extends DealDetailEvent {
  final int page;
  final List<ProductItem> listProductItems;

  final int dealId;

  final String order;

  LoadMoreDealDetailEvent(
      {@required this.page,
      @required this.listProductItems,
      @required this.dealId,
      @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [page];
}
