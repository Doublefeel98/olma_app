import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class DealDetailState extends Equatable {}

class DealDetailInitialState extends DealDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DealDetailLoadingState extends DealDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DealDetailLoadedState extends DealDetailState {
  final DealForDetail dealForDetail;
  final String order;

  DealDetailLoadedState({@required this.dealForDetail, @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [dealForDetail];
}

class DealDetailLoadingMoreState extends DealDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DealDetailLoadedMoreState extends DealDetailState {
  final List<ProductItem> listProductItems;

  final bool isFinish;

  final int page;

  DealDetailLoadedMoreState(
      {@required this.listProductItems,
      @required this.page,
      this.isFinish = false});

  @override
  // TODO: implement props
  List<Object> get props => [listProductItems, page, isFinish];
}

class DealDetailErrorState extends DealDetailState {
  final String message;

  DealDetailErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
