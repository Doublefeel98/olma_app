import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/repository/deal_repository.dart';

import 'bloc.dart';

class DealDetailBloc extends Bloc<DealDetailEvent, DealDetailState> {
  DealRepository dealRepository;

  DealDetailBloc({@required this.dealRepository});

  @override
  // TODO: implement initialState
  DealDetailState get initialState => DealDetailInitialState();

  @override
  Stream<DealDetailState> mapEventToState(DealDetailEvent event) async* {
    if (event is FetchDealDetailEvent) {
      yield DealDetailLoadingState();
      try {
        DealForDetail dealForDetail = await dealRepository
            .getDealForDetail(event.dealId, order: event.order);
        yield DealDetailLoadedState(
            dealForDetail: dealForDetail, order: event.order);
      } catch (e) {
        yield DealDetailErrorState(message: e.toString());
      }
    } else if (event is LoadMoreDealDetailEvent) {
      yield DealDetailLoadingMoreState();
      try {
        DealForDetail dealForDetail = await dealRepository.getDealForDetail(
            event.dealId,
            order: event.order,
            page: event.page);
        List<ProductItem> listProductItem = event.listProductItems;

        listProductItem.addAll(dealForDetail.listProducts.data);

        bool isFinish = false;
        if (event.page == dealForDetail.listProducts.lastPage) {
          isFinish = true;
        }
        yield DealDetailLoadedMoreState(
            listProductItems: listProductItem,
            page: event.page,
            isFinish: isFinish);
      } catch (e) {
        yield DealDetailErrorState(message: e.toString());
      }
    }
  }
}
