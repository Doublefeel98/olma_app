import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/deal.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/screens/deal_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/list_product_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class DealDetailUI extends StatefulWidget {
  final int dealId;

  const DealDetailUI({Key key, this.dealId}) : super(key: key);

  @override
  _DealDetailUIState createState() => _DealDetailUIState();
}

class _DealDetailUIState extends State<DealDetailUI> {
  int get _dealId => widget.dealId;
  DealDetailBloc _dealDetailBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dealDetailBloc = BlocProvider.of<DealDetailBloc>(context);
    _dealDetailBloc.add(
        FetchDealDetailEvent(dealId: _dealId, order: OrderBy.QUANTITY_SOLD));
  }

  bool _isFinish = false;

  bool _isFirstTime = true;

  int _page = 1;

  List<ProductItem> _listProducts;

  bool _isLoadingMore = false;

  DealForDetail _dealForDetail;

  String _order = OrderBy.QUANTITY_SOLD;

  bool _isInit = true;

  bool _isReload = true;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocConsumer<DealDetailBloc, DealDetailState>(
        listenWhen: (previous, current) {
      // return true/false to determine whether or not
      // to invoke listener with state
    }, listener: (context, state) {
      // do stuff here based on BlocA's state
      if (state is DealDetailErrorState) {
        _scaffoldKey.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      } else if (state is DealDetailLoadingMoreState) {
        setState(() {
          _isLoadingMore = true;
        });
      } else if (state is DealDetailLoadingState) {
        setState(() {
          _isReload = true;
        });
      } else if (state is DealDetailLoadedState) {
        this.setState(() {
          _dealForDetail = state.dealForDetail;
          _listProducts = state.dealForDetail.listProducts.data;
          _isFirstTime = false;
          _page = 1;
          _order = state.order;
          _isLoadingMore = false;
          _isInit = false;
          _isReload = false;
          _isFinish = state.dealForDetail.listProducts.lastPage == _page;
        });
      } else if (state is DealDetailLoadedMoreState) {
        this.setState(() {
          _page = state.page;
          _isFinish = state.isFinish;
          _listProducts = state.listProductItems;
          _isLoadingMore = false;
        });
      }
    }, buildWhen: (previous, current) {
      // return true/false to determine whether or not
      // to rebuild the widget with state
      print("previous");
      print(previous);
      print("current");
      print(current);
    }, builder: (context, state) {
      // return widget here based on BlocA's state
      // Widget widget;
      // if (state is DealDetailInitialState || state is DealDetailLoadingState) {
      //   widget = buildLoading();
      // } else if (state is DealDetailErrorState) {
      //   widget = buildErrorUi(state.message);
      // } else {
      //   widget = buildDealDetail(sizeHelper);
      // }
      // return buildContainer(widget);

      if (state is DealDetailErrorState) {
        return buildErrorUi(state.message);
      }

      return ListProductScreen(
        isReload: _isReload,
        scaffoldKey: _scaffoldKey,
        title: _dealForDetail != null ? _dealForDetail.name : "",
        listProductItem: _listProducts,
        total: _dealForDetail != null ? _dealForDetail.listProducts.total : 0,
        type: ListProductType.DEAL,
        banner: _dealForDetail != null ? _dealForDetail.banner : null,
        isFinish: _isFinish,
        isFirstTime: _isFirstTime,
        page: _page,
        isLoadingMore: _isLoadingMore,
        id: _dealId,
        order: _order,
        isInit: _isInit,
      );
    });
  }

  // Widget buildDealDetail(SizeHelper sizeHelper) {
  //   return ListProductScreen(
  //     title: _dealForDetail.name,
  //     listProductItem: _listProducts,
  //     total: _dealForDetail.listProducts.total,
  //     type: ListProductType.DEAL,
  //     banner: _dealForDetail.banner,
  //     isFinish: _isFinish,
  //     isFirstTime: _isFirstTime,
  //     page: _page,
  //     isLoading: _isLoading,
  //     id: _dealId,
  //     order: _order,
  //   );
  // }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}
