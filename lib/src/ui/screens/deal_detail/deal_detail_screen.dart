import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/deal_repository.dart';
import 'package:olmaapp/src/ui/screens/deal_detail/deal_detail_ui.dart';

import 'bloc/bloc.dart';

class DealDetailScreen extends StatelessWidget {
  final int _dealId;

  DealDetailScreen({Key key, @required int dealId})
      : assert(dealId != null),
        _dealId = dealId,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _dealRepository = RepositoryProvider.of<DealRepository>(context);
    return BlocProvider<DealDetailBloc>(
      create: (context) => DealDetailBloc(dealRepository: _dealRepository),
      child: DealDetailUI(
        dealId: _dealId,
      ),
    );
  }
}
