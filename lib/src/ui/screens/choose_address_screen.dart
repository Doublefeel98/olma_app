import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/delivery_address/bloc.dart';
import 'package:olmaapp/src/blocs/delivery_address/delivery_address_bloc.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/add_address/add_address_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';
import 'package:olmaapp/src/ui/widgets/item_delivery_address.dart';

class ChooseAddressScreen extends StatefulWidget {
  @override
  _ChooseAddressScreenState createState() => _ChooseAddressScreenState();
}

class _ChooseAddressScreenState extends State<ChooseAddressScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<DeliveryAddressBloc, DeliveryAddressState>(
      listener: (context, state) {
        if (state is DeliveryAddressErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<DeliveryAddressBloc, DeliveryAddressState>(
          builder: (context, state) {
        Widget widget;
        if (state is DeliveryAddressLoadedState) {
          widget =
              buildListDeliveryAddress(state.listDeliveryAddress, sizeHelper);
        } else if (state is DeliveryAddressErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }

        return buildDeliveryAddressContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildDeliveryAddressContainer(Widget widget, SizeHelper sizeHelper) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Chọn địa chỉ nhận hàng",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [],
      ),
      body: ListView(
        children: [
          SectionWrapper(
            title: "Danh sách địa chỉ",
            textReadMore: "THÊM ĐỊA CHỈ MỚI",
            onTapReadMore: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => AddAddressScreen()));
            },
            child: widget,
          ),
        ],
      ),
    );
  }

  Widget buildListDeliveryAddress(
      List<DeliveryAddress> listDeliveryAddress, SizeHelper sizeHelper) {
    return Column(
      children: listDeliveryAddress
          .map((deliveryAddress) => ItemDeliveryAddress(
              deliveryAddress: deliveryAddress,
              onTap: () {
                Navigator.pop(context, deliveryAddress);
              }))
          .toList(),
    );
  }
}
