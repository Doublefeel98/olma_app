import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/authentication_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/authentication_event.dart';
import 'package:olmaapp/src/blocs/authentication/authentication_state.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/blocs/delivery_address/bloc.dart';
import 'package:olmaapp/src/blocs/shipping_methods/bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/models/shipping_method.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/choose_address_screen.dart';
import 'package:olmaapp/src/ui/screens/login/login.dart';
import 'package:olmaapp/src/ui/screens/product_detail/product_detail_screen.dart';
import 'package:olmaapp/src/ui/screens/purchase/purchase_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

import 'bloc/bloc.dart';

class CheckoutUI extends StatefulWidget {
  final List<CartItem> listCartItemSelected;

  const CheckoutUI({Key key, this.listCartItemSelected}) : super(key: key);
  @override
  _CheckoutUIState createState() => _CheckoutUIState();
}

class _CheckoutUIState extends State<CheckoutUI> {
  final globalKey = GlobalKey<ScaffoldState>();

  String _selectTransport;
  String _selectPayment = PaymentMethods.CashPaymentOnDelivery;

  int _totalPriceCart;
  int _total;

  DeliveryAddressBloc _deliveryAddressBloc;

  DeliveryAddress _deliveryAddress;

  OlmaNowBloc _olmaNowBloc;
  GHNBloc _ghnBloc;
  GHTKBloc _ghtkBloc;
  CartBloc _cartBloc;
  ShippingMethod _shippingMethod;

  CheckoutBloc _checkoutBloc;

  AuthenticationBloc _authenticationBloc;

  List<CartItem> get _listCartItemSelected => widget.listCartItemSelected;

  bool get isPopulated =>
      _listCartItemSelected != null &&
      _listCartItemSelected.length > 0 &&
      _deliveryAddress != null &&
      _selectTransport != null &&
      _shippingMethod != null;

  bool isCheckoutButtonEnabled(CheckoutState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  void _onCheckouted() {
    _checkoutBloc.add(
      CheckoutSubmitted(
        deliveryAddressId: _deliveryAddress.id,
        fee: _shippingMethod.fee.fee,
        listCartItemIds: _listCartItemSelected.map((e) => e.id).toList(),
        paymentMethod: _selectPayment,
        shippingMethod: _selectTransport,
        insuranceFee: _shippingMethod.fee.insuranceFee,
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _cartBloc = BlocProvider.of<CartBloc>(context);

    _totalPriceCart = _listCartItemSelected.fold(
        0,
        (previousValue, element) =>
            (previousValue + element.totalPriceCartItem));
    _total = 0;
    _selectTransport = null;
    _selectPayment = PaymentMethods.CashPaymentOnDelivery;

    _checkoutBloc = BlocProvider.of<CheckoutBloc>(context);

    _deliveryAddressBloc = BlocProvider.of<DeliveryAddressBloc>(context);

    //set mac dinh la COD
    _checkoutBloc.add(CheckoutPaymentMethodChanged(
        paymentMethod: PaymentMethods.CashPaymentOnDelivery));

    DeliveryAddressState state = _deliveryAddressBloc.state;
    if (state is DeliveryAddressLoadedState) {
      if (state.listDeliveryAddress.length > 0) {
        _deliveryAddress = state.listDeliveryAddress.firstWhere(
          (element) => element.isDefault,
          orElse: () => state.listDeliveryAddress[0],
        );
        _checkoutBloc.add(CheckoutDeliveryAddressIdChanged(
            deliveryAddressId: _deliveryAddress.id));
      }
    }

    _olmaNowBloc = BlocProvider.of<OlmaNowBloc>(context);
    _ghnBloc = BlocProvider.of<GHNBloc>(context);
    _ghtkBloc = BlocProvider.of<GHTKBloc>(context);
    calculateFee();
  }

  void _deliveryAddressChange(DeliveryAddress deliveryAddress) {
    setState(() {
      _deliveryAddress = deliveryAddress;
    });
    _checkoutBloc.add(CheckoutDeliveryAddressIdChanged(
        deliveryAddressId: _deliveryAddress.id));
    calculateFee();
  }

  void calculateFee() {
    if (_deliveryAddress != null && _listCartItemSelected.length > 0) {
      _olmaNowBloc.add(FetchShippingMethodEvent(_deliveryAddress.id,
          _listCartItemSelected.map((e) => e.id).toList()));
      _ghtkBloc.add(FetchShippingMethodEvent(_deliveryAddress.id,
          _listCartItemSelected.map((e) => e.id).toList()));
      _ghnBloc.add(FetchShippingMethodEvent(_deliveryAddress.id,
          _listCartItemSelected.map((e) => e.id).toList()));
    }
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildCartItem(CartItem cartItem, SizeHelper sizeHelper) {
    return InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) =>
                  ProductDetailScreen(productId: cartItem.productId),
            ),
          );
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(2)),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.black26,
                width: 0.5,
              ),
            ),
          ),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  flex: 2,
                  fit: FlexFit.tight,
                  child: Container(
                    margin: EdgeInsets.only(right: sizeHelper.rW(2)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(sizeHelper.rW(1)),
                      child: CachedNetworkImage(
                        imageUrl: cartItem.productPoster,
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 8,
                  fit: FlexFit.tight,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                          child: Text(
                            cartItem.productName,
                            style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                MoneyFormatter(amount: 31200).toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                  fontWeight: FontWeight.w600,
                                  color: ColorConstants.BLUE_PRIMARY_TEXT,
                                ),
                              ),
                              Text(
                                "x${cartItem.quantity}",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(3.5),
                                  color: ColorConstants.ITEM_CATEGORY_HOME_TEXT,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget buildCartContainer(SizeHelper sizeHelper) {
    return SectionWrapper(
      title: "Danh sách sản phẩm",
      showReadMore: false,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        child: buildListCart(sizeHelper),
      ),
    );
  }

  Widget buildListCart(SizeHelper sizeHelper) {
    return Column(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _listCartItemSelected
              .map((cartItem) => buildCartItem(cartItem, sizeHelper))
              .toList(),
        ),
        SizedBox(
          height: sizeHelper.rW(2),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Tạm tính: ",
              style: TextStyle(
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w500,
              ),
            ),
            Text(
              MoneyFormatter(amount: _totalPriceCart).toString(),
              style: TextStyle(
                fontSize: sizeHelper.rW(4),
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget buildDeliveryAddressUI(SizeHelper sizeHelper) {
    return BlocListener<DeliveryAddressBloc, DeliveryAddressState>(
      listener: (context, state) {
        if (state is DeliveryAddressErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        } else if (state is DeliveryAddressLoadedState) {
          if (state.listDeliveryAddress.length > 0) {
            DeliveryAddress deliveryAddress =
                state.listDeliveryAddress.firstWhere(
              (element) => element.isDefault,
              orElse: () => state.listDeliveryAddress[0],
            );
            _deliveryAddressChange(deliveryAddress);
          }
        }
      },
      child: BlocBuilder<DeliveryAddressBloc, DeliveryAddressState>(
          builder: (context, state) {
        Widget widget;
        if (state is DeliveryAddressLoadedState) {
          if (state.listDeliveryAddress.length == 0) {
            widget = Center(
              child: Text("Chưa có địa chỉ nào"),
            );
          } else {
            widget = buildDeliveryAddress(sizeHelper);
          }
        } else if (state is DeliveryAddressErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }

        return buildDeliveryAddressContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildDeliveryAddress(SizeHelper sizeHelper) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          _deliveryAddress.name,
          style: TextStyle(
            fontSize: sizeHelper.rW(3.5),
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: sizeHelper.rW(1),
        ),
        Text(
          _deliveryAddress.phone,
          style: TextStyle(
            fontSize: sizeHelper.rW(3.5),
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(
          height: sizeHelper.rW(1),
        ),
        RichText(
          text: TextSpan(children: [
            TextSpan(
              text:
                  "${_deliveryAddress.address}, ${_deliveryAddress.wardName}, ${_deliveryAddress.districtName}, ${_deliveryAddress.cityName}",
              style: TextStyle(
                color: ColorConstants.GREY_TEXT,
                fontSize: sizeHelper.rW(3.2),
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
        )
      ],
    );
  }

  Widget buildDeliveryAddressContainer(Widget widget, SizeHelper sizeHelper) {
    return SectionWrapper(
      title: "Địa chỉ nhận hàng",
      textReadMore: "CHỌN ĐỊA CHỈ",
      onTapReadMore: () {
        dynamic result = Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => ChooseAddressScreen()));
        if (result is DeliveryAddress) {
          _deliveryAddressChange(result);
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        child: widget,
      ),
    );
  }

  Widget buildShippingMethod(String name, String value, SizeHelper sizeHelper) {
    Widget buildItemShippingMethod(
        bool isEnable, Widget widget, int fee, ShippingMethod shippingMethod) {
      return Row(children: <Widget>[
        Expanded(
            child: RadioListTile(
          groupValue: isEnable ? _selectTransport : null,
          title: Text(
            name,
            style: TextStyle(
                fontSize: sizeHelper.rW(3.5),
                fontWeight: FontWeight.w500,
                color: isEnable
                    ? Colors.black
                    : Colors.grey // neu don vi khong kha dung
                ),
          ),
          value: value,
          onChanged: isEnable
              ? (String value) {
                  setState(() {
                    _selectTransport = value;
                    _total = _totalPriceCart + fee;
                    _shippingMethod = shippingMethod;
                  });
                  _checkoutBloc.add(
                      CheckoutShippingMethodChanged(shippingMethod: value));
                }
              : null,
        )),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: widget,
          ),
        )
      ]);
    }

    void showError(BuildContext context, ShippingMethodState state) {
      if (state is ShippingMethodErrorState) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      }
    }

    Widget buildUI(ShippingMethodState state) {
      Widget widget;
      bool isEnable = false;
      int fee = 0;
      ShippingMethod shippingMethod;
      if (_deliveryAddress != null) {
        if (state is ShippingMethodLoadedState) {
          shippingMethod = state.shippingMethod;
          if (state.shippingMethod.success) {
            isEnable = state.shippingMethod.success;
            fee = state.shippingMethod.fee.fee;
            if (isEnable) {
              widget = Text(
                MoneyFormatter(amount: fee).toString(),
                style: TextStyle(
                  fontSize: sizeHelper.rW(3.5),
                  fontWeight: FontWeight.w400,
                ),
              );
            } else {
              widget = Text(
                "Không hỗ trợ",
                style: TextStyle(
                  fontSize: sizeHelper.rW(3.5),
                  fontWeight: FontWeight.w400,
                ),
              );
            }
          } else {
            widget = Text(
              "Không hỗ trợ",
              style: TextStyle(
                fontSize: sizeHelper.rW(3.5),
                fontWeight: FontWeight.w400,
              ),
            );
          }
        } else if (state is ShippingMethodErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = Center(
            child: Container(
              width: sizeHelper.rW(5),
              height: sizeHelper.rW(5),
              child: CircularProgressIndicator(),
            ),
          );
        }
      } else {
        widget = Container();
      }

      return buildItemShippingMethod(isEnable, widget, fee, shippingMethod);
    }

    switch (value) {
      case ShippingMethods.OlmaNow:
        return BlocListener<OlmaNowBloc, ShippingMethodState>(
          listener: (context, state) {
            showError(context, state);
          },
          child: BlocBuilder<OlmaNowBloc, ShippingMethodState>(
              builder: (context, state) {
            return buildUI(state);
          }),
        );
        break;
      case ShippingMethods.GHTK:
        return BlocListener<GHTKBloc, ShippingMethodState>(
          listener: (context, state) {
            showError(context, state);
          },
          child: BlocBuilder<GHTKBloc, ShippingMethodState>(
              builder: (context, state) {
            return buildUI(state);
          }),
        );
        break;
      case ShippingMethods.GHN:
        return BlocListener<GHNBloc, ShippingMethodState>(
          listener: (context, state) {
            showError(context, state);
          },
          child: BlocBuilder<GHNBloc, ShippingMethodState>(
              builder: (context, state) {
            return buildUI(state);
          }),
        );
        break;
      default:
        return Container();
    }
  }

  Widget buildShippingMethodsUI(SizeHelper sizeHelper) {
    return SectionWrapper(
      title: "Đơn vị vận chuyển",
      showReadMore: false,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        child: Theme(
          data: Theme.of(context)
              .copyWith(disabledColor: ColorConstants.BLUE_PRIMARY),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildShippingMethod(
                  "Olma now", ShippingMethods.OlmaNow, sizeHelper),
              buildShippingMethod(
                  "Giao hàng nhanh", ShippingMethods.GHN, sizeHelper),
              buildShippingMethod(
                  "Giao hàng tiết kiệm", ShippingMethods.GHTK, sizeHelper)
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPaymentMethodsUI(SizeHelper sizeHelper) {
    return SectionWrapper(
      title: "Phương thức thanh toán",
      showReadMore: false,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(2),
          horizontal: sizeHelper.rW(3),
        ),
        child: Theme(
          data: Theme.of(context)
              .copyWith(disabledColor: ColorConstants.BLUE_PRIMARY),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RadioListTile(
                groupValue: _selectPayment,
                title: Text(
                  "Thanh toán sau khi đã nhận hàng",
                  style: TextStyle(
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w500,
                      color: Colors.black),
                ),
                value: PaymentMethods.CashPaymentOnDelivery,
                onChanged: (value) {
                  setState(() {});
                },
              ),
              RadioListTile(
                groupValue: _selectPayment,
                title: Text(
                  "Thanh toán bằng thẻ quốc tế Visa, Master, JCB (chưa hỗ trợ)",
                  style: TextStyle(
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w500,
                      color: Colors.grey),
                ),
                value: PaymentMethods.VisaMasterJCB,
                onChanged: null,
              ),
              RadioListTile(
                groupValue: _selectPayment,
                title: Text(
                  "Thẻ ATM nội địa/Internet Banking (chưa hỗ trợ)",
                  style: TextStyle(
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w500,
                      color: Colors.grey),
                ),
                value: PaymentMethods.ATM_OR_InternetBanking,
                onChanged: null,
              ),
              RadioListTile(
                groupValue: _selectPayment,
                title: Text(
                  "Thanh toán bằng ví MoMo (chưa hỗ trợ)",
                  style: TextStyle(
                      fontSize: sizeHelper.rW(3.5),
                      fontWeight: FontWeight.w500,
                      color: Colors.grey),
                ),
                value: PaymentMethods.MoMoWallet,
                onChanged: null,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        if (state is AuthenticationLoggedOut) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => LoginScreen()));
        }
      },
      child: BlocListener<CheckoutBloc, CheckoutState>(
        listener: (context, state) {
          if (state.isSubmitting) {
            globalKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Đang đặt hàng...'),
                      CircularProgressIndicator(),
                    ],
                  ),
                ),
              );
          }
          if (state.isSuccess) {
            globalKey.currentState..hideCurrentSnackBar();
            DialogAlert.showMyDialog(
                context: context,
                title: "Thông báo",
                content: state.message,
                onPress: () {
                  _cartBloc.add(FetchCartEvent());
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => PurchaseScreen(
                          type: PurChaseScreenType.IN_PROGRESS)));
                });
          }
          if (state.isFailure) {
            globalKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(state.message),
                      Icon(Icons.error),
                    ],
                  ),
                  backgroundColor: Colors.red,
                ),
              );
          }
        },
        child:
            BlocBuilder<CheckoutBloc, CheckoutState>(builder: (context, state) {
          return Scaffold(
            key: globalKey,
            appBar: AppBar(
              backgroundColor: ColorConstants.BLUE_PRIMARY,
              elevation: 0.0,
              titleSpacing: 0.0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.white,
                iconSize: sizeHelper.rW(6),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text(
                "Thanh toán",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: sizeHelper.rW(4),
                  fontWeight: FontWeight.w400,
                ),
              ),
              actions: [],
            ),
            body: Column(
              children: [
                Flexible(
                  child: ListView(
                    children: [
                      buildDeliveryAddressUI(sizeHelper),
                      buildCartContainer(sizeHelper),
                      buildShippingMethodsUI(sizeHelper),
                      buildPaymentMethodsUI(sizeHelper)
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(
                          horizontal: sizeHelper.rW(3),
                          vertical: sizeHelper.rW(3),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Tổng cộng: ",
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: sizeHelper.rW(4),
                                        fontWeight: FontWeight.w500),
                                  ),
                                  TextSpan(
                                    text: _total > _totalPriceCart
                                        ? MoneyFormatter(amount: _total)
                                            .toString()
                                        : "_________",
                                    style: TextStyle(
                                      color: ColorConstants.BLUE_PRIMARY_TEXT,
                                      fontSize: sizeHelper.rW(4),
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: sizeHelper.rW(12),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero,
                          ),
                          child: Text('Đặt hàng ngay'.toUpperCase(),
                              style: TextStyle(
                                  fontSize: sizeHelper.rW(4.2),
                                  fontWeight: FontWeight.w400)),
                          color: ColorConstants.BLUE_PRIMARY,
                          textColor: Colors.white,
                          onPressed: isCheckoutButtonEnabled(state)
                              ? _onCheckouted
                              : null,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
