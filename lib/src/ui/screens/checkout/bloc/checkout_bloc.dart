import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/order.dart';
import 'package:olmaapp/src/repository/order_repository.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  final OrderRepository _orderRepository;

  CheckoutBloc({@required OrderRepository orderRepository})
      : assert(orderRepository != null),
        _orderRepository = orderRepository;

  @override
  CheckoutState get initialState => CheckoutState.initial();

  @override
  Stream<Transition<CheckoutEvent, CheckoutState>> transformEvents(
    Stream<CheckoutEvent> events,
    TransitionFunction<CheckoutEvent, CheckoutState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! CheckoutShippingMethodChanged &&
          event is! CheckoutPaymentMethodChanged);
    });
    final debounceStream = events.where((event) {
      return (event is CheckoutShippingMethodChanged ||
          event is CheckoutPaymentMethodChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<CheckoutState> mapEventToState(
    CheckoutEvent event,
  ) async* {
    if (event is CheckoutDeliveryAddressIdChanged) {
      yield* _mapCheckoutDeliveryAddressIdChangedToState(
          event.deliveryAddressId);
    } else if (event is CheckoutPaymentMethodChanged) {
      yield* _mapCheckoutPaymentMethodChangedToState(event.paymentMethod);
    } else if (event is CheckoutShippingMethodChanged) {
      yield* _mapCheckoutShippingMethodChangedToState(event.shippingMethod);
    } else if (event is CheckoutSubmitted) {
      yield* _mapCheckoutSubmittedToState(
          event.listCartItemIds,
          event.deliveryAddressId,
          event.paymentMethod,
          event.shippingMethod,
          event.fee,
          event.insuranceFee);
    }
  }

  Stream<CheckoutState> _mapCheckoutListCartItemIdsChangedToState(
      List<int> listCartItemIds) async* {
    yield state.update(
      isListCartItemIdsValid:
          listCartItemIds != null && listCartItemIds.length > 0,
    );
  }

  Stream<CheckoutState> _mapCheckoutDeliveryAddressIdChangedToState(
      int deliveryAddressId) async* {
    yield state.update(
      isDeliveryAddressIdValid:
          deliveryAddressId != null && deliveryAddressId > 0,
    );
  }

  Stream<CheckoutState> _mapCheckoutPaymentMethodChangedToState(
      String paymentMethod) async* {
    yield state.update(
      isPaymentMethodValid: paymentMethod.isNotEmpty,
    );
  }

  Stream<CheckoutState> _mapCheckoutShippingMethodChangedToState(
      String shippingMethod) async* {
    yield state.update(
      isShippingMethodValid: shippingMethod.isNotEmpty,
    );
  }

  Stream<CheckoutState> _mapCheckoutSubmittedToState(
    List<int> listCartItemIds,
    int deliveryAddressId,
    String paymentMethod,
    String shippingMethod,
    int fee,
    int insuranceFee,
  ) async* {
    yield CheckoutState.loading();
    try {
      CheckoutResponse response = await _orderRepository.checkOut(
          listCartItemIds: listCartItemIds,
          deliveryAddressId: deliveryAddressId,
          paymentMethod: paymentMethod,
          fee: fee,
          insuranceFee: insuranceFee,
          shippingMethod: shippingMethod);
      if (response.result) {
        yield CheckoutState.success(response.message);
      } else {
        yield CheckoutState.failure(response.message);
      }
    } catch (_) {
      yield CheckoutState.failure("Lỗi kết nối");
    }
  }
}
