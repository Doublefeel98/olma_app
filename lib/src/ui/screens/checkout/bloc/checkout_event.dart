import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CheckoutEvent extends Equatable {
  const CheckoutEvent();

  @override
  List<Object> get props => [];
}

class CheckoutDeliveryAddressIdChanged extends CheckoutEvent {
  final int deliveryAddressId;

  const CheckoutDeliveryAddressIdChanged({@required this.deliveryAddressId});

  @override
  List<Object> get props => [deliveryAddressId];

  @override
  String toString() => 'DeliveryAddressIdChanged { email :$deliveryAddressId }';
}

class CheckoutPaymentMethodChanged extends CheckoutEvent {
  final String paymentMethod;

  const CheckoutPaymentMethodChanged({@required this.paymentMethod});

  @override
  List<Object> get props => [paymentMethod];

  @override
  String toString() => 'PaymentMethodChanged { paymentMethod: $paymentMethod }';
}

class CheckoutShippingMethodChanged extends CheckoutEvent {
  final String shippingMethod;

  const CheckoutShippingMethodChanged({@required this.shippingMethod});

  @override
  List<Object> get props => [shippingMethod];

  @override
  String toString() =>
      'ShippingMethodChanged { shippingMethod: $shippingMethod }';
}

class CheckoutSubmitted extends CheckoutEvent {
  final List<int> listCartItemIds;
  final int deliveryAddressId;
  final String paymentMethod;
  final String shippingMethod;
  final int fee;
  final int insuranceFee;

  CheckoutSubmitted(
      {@required this.listCartItemIds,
      @required this.deliveryAddressId,
      @required this.paymentMethod,
      @required this.shippingMethod,
      @required this.fee,
      this.insuranceFee});

  @override
  List<Object> get props => [
        listCartItemIds,
        deliveryAddressId,
        paymentMethod,
        shippingMethod,
        fee,
        insuranceFee
      ];

  @override
  String toString() {
    return 'Checkout Submitted { listCartItemIds: $listCartItemIds, deliveryAddressId: $deliveryAddressId, paymentMethod: $paymentMethod, shippingMethod: $shippingMethod, fee: $fee, insuranceFee: $insuranceFee }';
  }
}
