import 'package:meta/meta.dart';

@immutable
class CheckoutState {
  final bool isShippingMethodValid;
  final bool isListCartItemIdsValid;
  final bool isDeliveryAddressIdValid;
  final bool isPaymentMethodValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  String message = '';

  bool get isFormValid =>
      isShippingMethodValid &&
      isListCartItemIdsValid &&
      isDeliveryAddressIdValid &&
      isPaymentMethodValid;

  CheckoutState(
      {@required this.isShippingMethodValid,
      @required this.isListCartItemIdsValid,
      @required this.isDeliveryAddressIdValid,
      @required this.isPaymentMethodValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      this.message});

  factory CheckoutState.initial() {
    return CheckoutState(
      isShippingMethodValid: true,
      isListCartItemIdsValid: true,
      isDeliveryAddressIdValid: true,
      isPaymentMethodValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory CheckoutState.loading() {
    return CheckoutState(
      isShippingMethodValid: true,
      isListCartItemIdsValid: true,
      isDeliveryAddressIdValid: true,
      isPaymentMethodValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory CheckoutState.failure(String message) {
    return CheckoutState(
        isShippingMethodValid: true,
        isListCartItemIdsValid: true,
        isDeliveryAddressIdValid: true,
        isPaymentMethodValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: true,
        message: message);
  }

  factory CheckoutState.success(String message) {
    return CheckoutState(
        isShippingMethodValid: true,
        isListCartItemIdsValid: true,
        isDeliveryAddressIdValid: true,
        isPaymentMethodValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        message: message);
  }

  CheckoutState update({
    bool isShippingMethodValid,
    bool isListCartItemIdsValid,
    bool isPaymentMethodValid,
    bool isDeliveryAddressIdValid,
  }) {
    return copyWith(
      isShippingMethodValid: isShippingMethodValid,
      isListCartItemIdsValid: isListCartItemIdsValid,
      isDeliveryAddressIdValid: isDeliveryAddressIdValid,
      isPaymentMethodValid: isPaymentMethodValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  CheckoutState copyWith(
      {bool isShippingMethodValid,
      bool isListCartItemIdsValid,
      bool isDeliveryAddressIdValid,
      bool isPaymentMethodValid,
      bool isSubmitEnabled,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      String message}) {
    return CheckoutState(
        isShippingMethodValid:
            isShippingMethodValid ?? this.isShippingMethodValid,
        isListCartItemIdsValid:
            isListCartItemIdsValid ?? this.isListCartItemIdsValid,
        isDeliveryAddressIdValid:
            isDeliveryAddressIdValid ?? this.isDeliveryAddressIdValid,
        isPaymentMethodValid: isPaymentMethodValid ?? this.isPaymentMethodValid,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailure ?? this.isFailure,
        message: message ?? this.message);
  }

  @override
  String toString() {
    return '''CheckoutState {
      isShippingMethodValid: $isShippingMethodValid,
      isListCartItemIdsValid: $isListCartItemIdsValid,
      isDeliveryAddressIdValid: $isDeliveryAddressIdValid, 
      isPaymentMethodValid: $isPaymentMethodValid,       
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      message: $message
    }''';
  }
}
