import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/repository/order_repository.dart';
import 'package:olmaapp/src/ui/screens/checkout/checkout_ui.dart';

import 'bloc/bloc.dart';

class CheckoutScreen extends StatelessWidget {
  final List<CartItem> listCartItemSelected;

  const CheckoutScreen({Key key, @required this.listCartItemSelected})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _orderRepository = RepositoryProvider.of<OrderRepository>(context);
    return BlocProvider<CheckoutBloc>(
      create: (context) => CheckoutBloc(orderRepository: _orderRepository),
      child: CheckoutUI(listCartItemSelected: listCartItemSelected),
    );
  }
}
