import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Đăng ký",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  ColorConstants.BLUE_PRIMARY,
                  ColorConstants.BLUE_PRIMARY_MEDIUM,
                  ColorConstants.BLUE_PRIMARY_LIGHT
                ],
              ),
            ),
          ),
          Container(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: sizeHelper.rW(7)),
                    alignment: Alignment.center,
                    child: Image.network(
                      "https://olma.s3-ap-southeast-1.amazonaws.com/images/olma_logo2.png",
                      width: sizeHelper.rW(15),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Tên người dùng",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: sizeHelper.rW(4),
                        ),
                      ),
                      SizedBox(
                        height: sizeHelper.rW(2),
                      ),
                      Container(
                        height: sizeHelper.rW(11),
                        padding:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(0.5)),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black38,
                                blurRadius: 2,
                                spreadRadius: 0,
                                offset: Offset(1, 1),
                              ),
                            ],
                            borderRadius:
                                BorderRadius.circular(sizeHelper.rW(1))),
                        child: TextField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                            fontSize: sizeHelper.rW(4),
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.person,
                                size: sizeHelper.rW(5),
                                color: ColorConstants.BLUE_PRIMARY,
                              ),
                              hintText: "Nhập email của bạn",
                              hintStyle: TextStyle(
                                  color: Colors.black45,
                                  fontSize: sizeHelper.rW(4))),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Email",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: sizeHelper.rW(4),
                        ),
                      ),
                      SizedBox(
                        height: sizeHelper.rW(2),
                      ),
                      Container(
                        height: sizeHelper.rW(11),
                        padding:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(0.5)),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black38,
                                blurRadius: 2,
                                spreadRadius: 0,
                                offset: Offset(1, 1),
                              ),
                            ],
                            borderRadius:
                                BorderRadius.circular(sizeHelper.rW(1))),
                        child: TextField(
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                            fontSize: sizeHelper.rW(4),
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.email,
                                size: sizeHelper.rW(5),
                                color: ColorConstants.BLUE_PRIMARY,
                              ),
                              hintText: "Nhập email của bạn",
                              hintStyle: TextStyle(
                                  color: Colors.black45,
                                  fontSize: sizeHelper.rW(4))),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Mật khẩu",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: sizeHelper.rW(4),
                        ),
                      ),
                      SizedBox(
                        height: sizeHelper.rW(2),
                      ),
                      Container(
                        height: sizeHelper.rW(11),
                        padding:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(0.5)),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black38,
                                blurRadius: 2,
                                spreadRadius: 0,
                                offset: Offset(1, 1),
                              ),
                            ],
                            borderRadius:
                                BorderRadius.circular(sizeHelper.rW(1))),
                        child: TextField(
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          style: TextStyle(
                            fontSize: sizeHelper.rW(4),
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                size: sizeHelper.rW(5),
                                color: ColorConstants.BLUE_PRIMARY,
                              ),
                              hintText: "Nhập mật khẩu của bạn",
                              hintStyle: TextStyle(
                                  color: Colors.black45,
                                  fontSize: sizeHelper.rW(4))),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Nhập lại mật khẩu",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: sizeHelper.rW(4),
                        ),
                      ),
                      SizedBox(
                        height: sizeHelper.rW(2),
                      ),
                      Container(
                        height: sizeHelper.rW(11),
                        padding:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(0.5)),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black38,
                                blurRadius: 2,
                                spreadRadius: 0,
                                offset: Offset(1, 1),
                              ),
                            ],
                            borderRadius:
                                BorderRadius.circular(sizeHelper.rW(1))),
                        child: TextField(
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          style: TextStyle(
                            fontSize: sizeHelper.rW(4),
                            color: Colors.black,
                          ),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                size: sizeHelper.rW(5),
                                color: ColorConstants.BLUE_PRIMARY,
                              ),
                              hintText: "Nhập lại mật khẩu",
                              hintStyle: TextStyle(
                                  color: Colors.black45,
                                  fontSize: sizeHelper.rW(4))),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                    height: sizeHelper.rW(11),
                    child: RaisedButton(
                      onPressed: () {},
                      child: Text(
                        "Đăng ký".toUpperCase(),
                        style: TextStyle(
                            color: Colors.white, fontSize: sizeHelper.rW(4)),
                      ),
                      color: ColorConstants.BLUE_PRIMARY,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(sizeHelper.rW(1))),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
