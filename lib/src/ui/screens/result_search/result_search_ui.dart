import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/screens/list_product_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class ResultSearchUI extends StatefulWidget {
  final String search;

  const ResultSearchUI({Key key, this.search}) : super(key: key);

  @override
  _ResultSearchUIState createState() => _ResultSearchUIState();
}

class _ResultSearchUIState extends State<ResultSearchUI> {
  String get _search => widget.search;
  ResultSearchBloc _resultSearchBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _resultSearchBloc = BlocProvider.of<ResultSearchBloc>(context);
    _resultSearchBloc.add(
        FetchResultSearchEvent(search: _search, order: OrderBy.QUANTITY_SOLD));
  }

  bool _isFinish = false;

  bool _isFirstTime = true;

  int _page = 1;

  List<ProductItem> _listProducts;

  bool _isLoading = false;

  ListProductForView _listProductForView;

  String _order = OrderBy.QUANTITY_SOLD;

  bool _isInit = true;

  bool _isReload = true;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocConsumer<ResultSearchBloc, ResultSearchState>(
        listenWhen: (previous, current) {
      // return true/false to determine whether or not
      // to invoke listener with state
    }, listener: (context, state) {
      // do stuff here based on BlocA's state
      if (state is ResultSearchErrorState) {
        _scaffoldKey.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      } else if (state is ResultSearchLoadingMoreState) {
        setState(() {
          _isLoading = true;
        });
      } else if (state is ResultSearchLoadingState) {
        setState(() {
          _isReload = true;
        });
      } else if (state is ResultSearchLoadedState) {
        this.setState(() {
          _listProductForView = state.listProductForView;
          _listProducts = state.listProductForView.listProducts.data;
          _isFirstTime = false;
          _page = 1;
          _order = state.order;
          _isFinish = state.listProductForView.listProducts.lastPage == _page;
          _isLoading = false;
          _isInit = false;
          _isReload = false;
        });
      } else if (state is ResultSearchLoadedMoreState) {
        this.setState(() {
          _page = state.page;
          _isFinish = state.isFinish;
          _listProducts = state.listProductItems;
          _isLoading = false;
        });
      }
    }, buildWhen: (previous, current) {
      // return true/false to determine whether or not
      // to rebuild the widget with state
      print("previous");
      print(previous);
      print("current");
      print(current);
    }, builder: (context, state) {
      // return widget here based on BlocA's state
      // Widget widget;
      // if (state is ResultSearchInitialState ||
      //     state is ResultSearchLoadingState) {
      //   widget = buildLoading();
      // } else if (state is ResultSearchErrorState) {
      //   widget = buildErrorUi(state.message);
      // } else {
      //   widget = buildResultSearch(sizeHelper);
      // }
      // return buildContainer(widget);

      if (state is ResultSearchErrorState) {
        return buildErrorUi(state.message);
      }

      return ListProductScreen(
        isReload: _isReload,
        scaffoldKey: _scaffoldKey,
        title: _listProductForView != null ? _listProductForView.title : "",
        listProductItem: _listProducts,
        total: _listProductForView != null
            ? _listProductForView.listProducts.total
            : 0,
        type: ListProductType.SEARCH,
        banner: _listProductForView != null ? _listProductForView.banner : null,
        isFinish: _isFinish,
        isFirstTime: _isFirstTime,
        page: _page,
        isLoadingMore: _isLoading,
        id: _search,
        order: _order,
        isInit: _isInit,
      );
    });
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}
