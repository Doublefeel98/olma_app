import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/ui/screens/result_search/result_search_ui.dart';

import 'bloc/bloc.dart';

class ResultSearchScreen extends StatelessWidget {
  final String _search;

  ResultSearchScreen({Key key, @required String search})
      : assert(search != null),
        _search = search,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _productRepository =
        RepositoryProvider.of<ProductRepository>(context);
    return BlocProvider<ResultSearchBloc>(
      create: (context) =>
          ResultSearchBloc(productRepository: _productRepository),
      child: ResultSearchUI(
        search: _search,
      ),
    );
  }
}
