import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class ResultSearchEvent extends Equatable {}

class FetchResultSearchEvent extends ResultSearchEvent {
  final String search;
  final String order;

  FetchResultSearchEvent({@required this.search, @required this.order});

  @override
  List<Object> get props => [search];

  @override
  String toString() => '{ categoryId: $search }';
}

class LoadMoreResultSearchEvent extends ResultSearchEvent {
  final int page;
  final List<ProductItem> listProductItems;
  final String search;
  final String order;

  LoadMoreResultSearchEvent(
      {@required this.page,
      @required this.listProductItems,
      @required this.search,
      @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [page];
}
