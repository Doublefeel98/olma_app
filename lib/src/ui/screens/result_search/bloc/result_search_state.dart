import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class ResultSearchState extends Equatable {}

class ResultSearchInitialState extends ResultSearchState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ResultSearchLoadingState extends ResultSearchState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ResultSearchLoadedState extends ResultSearchState {
  final ListProductForView listProductForView;
  final String order;

  ResultSearchLoadedState(
      {@required this.listProductForView, @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [listProductForView];
}

class ResultSearchLoadingMoreState extends ResultSearchState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ResultSearchLoadedMoreState extends ResultSearchState {
  final List<ProductItem> listProductItems;

  final bool isFinish;

  final int page;

  ResultSearchLoadedMoreState(
      {@required this.listProductItems,
      @required this.page,
      this.isFinish = false});

  @override
  // TODO: implement props
  List<Object> get props => [listProductItems, page, isFinish];
}

class ResultSearchErrorState extends ResultSearchState {
  final String message;

  ResultSearchErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
