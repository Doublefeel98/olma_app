import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/repository/product_repository.dart';

import 'bloc.dart';

class ResultSearchBloc extends Bloc<ResultSearchEvent, ResultSearchState> {
  ProductRepository productRepository;

  ResultSearchBloc({@required this.productRepository});

  @override
  // TODO: implement initialState
  ResultSearchState get initialState => ResultSearchInitialState();

  @override
  Stream<ResultSearchState> mapEventToState(ResultSearchEvent event) async* {
    if (event is FetchResultSearchEvent) {
      yield ResultSearchLoadingState();
      try {
        ListProductForView listProductForView = await productRepository
            .getListProductByFilter(event.search, order: event.order);
        yield ResultSearchLoadedState(
            listProductForView: listProductForView, order: event.order);
      } catch (e) {
        yield ResultSearchErrorState(message: e.toString());
      }
    } else if (event is LoadMoreResultSearchEvent) {
      yield ResultSearchLoadingMoreState();
      try {
        ListProductForView listProductForView =
            await productRepository.getListProductByFilter(event.search,
                order: event.order, page: event.page);
        List<ProductItem> listProductItem = event.listProductItems;

        listProductItem.addAll(listProductForView.listProducts.data);

        bool isFinish = false;
        if (event.page == listProductForView.listProducts.lastPage) {
          isFinish = true;
        }
        yield ResultSearchLoadedMoreState(
            listProductItems: listProductItem,
            page: event.page,
            isFinish: isFinish);
      } catch (e) {
        yield ResultSearchErrorState(message: e.toString());
      }
    }
  }
}
