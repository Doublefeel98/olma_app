import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loadmore/loadmore.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/widgets/banner_home/banner_home_primary.dart';
import 'package:olmaapp/src/ui/widgets/list_category_home/list_category_home.dart';
import 'package:olmaapp/src/ui/widgets/list_deal_home/list_deal_home.dart';
import 'package:olmaapp/src/ui/widgets/list_product_for_you/bloc/bloc.dart';
import 'package:olmaapp/src/ui/widgets/list_your_product_home.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ListProductForYouBloc _listProductForYouBloc;

  @override
  void initState() {
    super.initState();
    _listProductForYouBloc = BlocProvider.of<ListProductForYouBloc>(context);
    ListProductForYouState state = _listProductForYouBloc.state;
    if (state is ListProductForYouLoadingState ||
        state is ListProductForYouLoadingMoreState) {
      setState(() {
        _isLoading = true;
      });
    } else if (state is ListProductForYouLoadedState) {
      this.setState(() {
        _listProducts = state.listProductForYou.listProducts;
        _isFirstTime = false;
        _isLoading = false;
      });
    } else if (state is ListProductForYouLoadedMoreState) {
      this.setState(() {
        page = state.page;
        _isFinish = state.isFinish;
        _listProducts = state.listProducts;
        _isLoading = false;
      });
    }
  }

  bool _isFinish = false;

  bool _isFirstTime = true;

  bool _isLoading = false;

  int page = 1;

  List<ProductItem> _listProducts;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ListProductForYouBloc, ListProductForYouState>(
        listenWhen: (previous, current) {
      // return true/false to determine whether or not
      // to invoke listener with state
    }, listener: (context, state) {
      // do stuff here based on BlocA's state
      if (state is ListProductForYouErrorState) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      } else if (state is ListProductForYouLoadingState ||
          state is ListProductForYouLoadingMoreState) {
        setState(() {
          _isLoading = true;
        });
      } else if (state is ListProductForYouLoadedState) {
        this.setState(() {
          _listProducts = state.listProductForYou.listProducts;
          _isFirstTime = false;
          _isLoading = false;
        });
      } else if (state is ListProductForYouLoadedMoreState) {
        this.setState(() {
          page = state.page;
          _isFinish = state.isFinish;
          _listProducts = state.listProducts;
          _isLoading = false;
        });
      }
    }, buildWhen: (previous, current) {
      // return true/false to determine whether or not
      // to rebuild the widget with state
      print("previous");
      print(previous);
      print("current");
      print(current);
    }, builder: (context, state) {
      // return widget here based on BlocA's state
      return Container(
        child: LoadMore(
          isFinish: _isFinish,
          onLoadMore: _loadMore,
          textBuilder: DefaultLoadMoreTextBuilder.english,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              BannerHomePrimary(),
              ListCategoryHome(),
              ListDealHome(),
              _listProducts != null
                  ? ListYourProductHome(listProducts: _listProducts)
                  : Container(),
              _isLoading ? buildLoading() : Container(),
            ],
          ),
        ),
      );
    });
  }

  Future<bool> _loadMore() async {
    print("onLoadMore");
    if (!_isFirstTime &&
        !(_listProductForYouBloc.state is ListProductForYouLoadingMoreState) &&
        !_isFinish) {
      _listProductForYouBloc.add(LoadMoreListProductForYouEvent(
          page: page + 1, listProducts: _listProducts));
    }

    return true;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
