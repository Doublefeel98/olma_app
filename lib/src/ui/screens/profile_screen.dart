import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/change_password/change_password_screen.dart';
import 'package:olmaapp/src/ui/screens/edit_profile/edit_profile_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
      if (state is AuthenticationFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text("Đăng nhập thất bại"), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      }
    }, child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
      Widget widget;
      if (state is AuthenticationSuccess) {
        widget = buildProfileContent(sizeHelper, state.user);
      } else if (state is AuthenticationInProgress) {
        widget = buildLoading();
      } else if (state is AuthenticationFailure) {
        widget = buildErrorUi(state.message);
      }
      return buildProfileScreenContainer(sizeHelper, widget);
    }));
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildProfileContent(SizeHelper sizeHelper, User user) {
    return ListView(
      children: [
        SectionWrapper(
          title: "Thông tin cá nhân",
          textReadMore: "CHỈNH SỬA",
          onTapReadMore: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => EditProfileScreen(user: user),
              ),
            );
          },
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: sizeHelper.rW(2),
              horizontal: sizeHelper.rW(3),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Tên: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: user.name,
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Email: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: user.email != null ? user.email : "Không có",
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Số điện thoại: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: user.phoneNumber != null
                          ? user.phoneNumber
                          : "Không có",
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
                SizedBox(
                  height: sizeHelper.rW(2),
                ),
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: "Thành viên từ: ",
                      style: TextStyle(
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: DateFormat("dd/MM/yyyy")
                          .format(user.createdAt.toLocal()),
                      style: TextStyle(
                        color: ColorConstants.GREY_TEXT,
                        fontSize: sizeHelper.rW(4),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
        SectionWrapper(
          title: "Mật khẩu",
          textReadMore: "ĐỔI MẬT KHẨU",
          hasTitleMarginBottom: false,
          onTapReadMore: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ChangePasswordScreen(
                  user: user,
                ),
              ),
            );
          },
          child: Container(),
        )
      ],
    );
  }

  Widget buildProfileScreenContainer(SizeHelper sizeHelper, Widget widget) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Thông tin tài khoản",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          // IconButton(
          //   icon: Icon(
          //     Icons.edit,
          //     color: Colors.white,
          //     size: sizeHelper.rW(6),
          //   ),
          //   onPressed: () {
          //     Navigator.of(context).push(MaterialPageRoute(
          //       builder: (context) => EditProfileScreen(),
          //     ));
          //   },
          // ),
        ],
      ),
      body: widget,
    );
  }
}
