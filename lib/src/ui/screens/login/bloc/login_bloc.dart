import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  UserRepository _userRepository;
  final String messageError = "Đăng nhập thất bại";

  LoginBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  LoginState get initialState => LoginState.initial();

  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
    Stream<LoginEvent> events,
    TransitionFunction<LoginEvent, LoginState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! LoginEmailChanged && event is! LoginPasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is LoginEmailChanged || event is LoginPasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginEmailChanged) {
      yield* _mapLoginEmailChangedToState(event.email);
    } else if (event is LoginPasswordChanged) {
      yield* _mapLoginPasswordChangedToState(event.password);
    } else if (event is LoginWithGooglePressed) {
      yield* _mapLoginWithGooglePressedToState();
    } else if (event is LoginWithFacebookPressed) {
      yield* _mapLoginWithFacebookPressedToState();
    } else if (event is LoginWithCredentialsPressed) {
      yield* _mapLoginWithCredentialsPressedToState(
          email: event.email, password: event.password, remeberMe: true);
    }
  }

  Stream<LoginState> _mapLoginEmailChangedToState(String email) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<LoginState> _mapLoginPasswordChangedToState(String password) async* {
    yield state.update(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }

  Stream<LoginState> _mapLoginWithGooglePressedToState() async* {
    try {
      var user = await _userRepository.signInWithGoogle();
      if (user != null) {
        yield LoginState.success();
      } else {
        yield LoginState.failure(messageError);
      }
    } catch (e) {
      yield LoginState.failure(e.toString());
    }
  }

  Stream<LoginState> _mapLoginWithFacebookPressedToState() async* {
    try {
      var user = await _userRepository.signInWithFacebook();
      if (user != null) {
        yield LoginState.success();
      } else {
        yield LoginState.failure(messageError);
      }
    } catch (e) {
      yield LoginState.failure(e.toString());
    }
  }

  Stream<LoginState> _mapLoginWithCredentialsPressedToState(
      {String email, String password, bool remeberMe}) async* {
    yield LoginState.loading();
    try {
      var user = await _userRepository.signIn(
          email: email, password: password, rememberMe: remeberMe);

      if (user != null) {
        yield LoginState.success();
      } else {
        yield LoginState.failure(messageError);
      }
    } catch (e) {
      yield LoginState.failure(e.toString());
    }
  }
}
