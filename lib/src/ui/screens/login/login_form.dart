import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/blocs/cart/cart_bloc.dart';
import 'package:olmaapp/src/blocs/cart/cart_event.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/register/register.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      LoginEmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      LoginPasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _loginBloc.add(
      LoginWithCredentialsPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.isFailure) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Đăng nhập thất bại'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang đăng nhập...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context)
              .add(AuthenticationLoggedIn());
          BlocProvider.of<CartBloc>(context).add(FetchCartEvent());
          Navigator.pop(context);
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
        SizeHelper sizeHelper = SizeHelper(context);
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Đăng nhập",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          body: Stack(
            children: [
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      ColorConstants.BLUE_PRIMARY,
                      ColorConstants.BLUE_PRIMARY_MEDIUM,
                      ColorConstants.BLUE_PRIMARY_LIGHT
                    ],
                  ),
                ),
              ),
              Container(
                child: SingleChildScrollView(
                  padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: sizeHelper.rW(7)),
                        alignment: Alignment.center,
                        child: CachedNetworkImage(
                          imageUrl:
                              "https://olma.s3-ap-southeast-1.amazonaws.com/images/olma_logo2.png",
                          width: sizeHelper.rW(15),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Email",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              controller: _emailController,
                              validator: (_) {
                                return !state.isEmailValid
                                    ? 'Email không hợp lệ!'
                                    : null;
                              },
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.email,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập email của bạn",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Mật khẩu",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                          SizedBox(
                            height: sizeHelper.rW(2),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: sizeHelper.rW(0.5)),
                            margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              controller: _passwordController,
                              validator: (_) {
                                return !state.isPasswordValid
                                    ? 'Mật khẩu không hợp lệ'
                                    : null;
                              },
                              autovalidate: true,
                              autocorrect: false,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                                color: Colors.black,
                              ),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                  filled: true,
                                  fillColor: Colors.white,
                                  prefixIcon: Icon(
                                    Icons.vpn_key,
                                    size: sizeHelper.rW(5),
                                    color: ColorConstants.BLUE_PRIMARY,
                                  ),
                                  hintText: "Nhập mật khẩu của bạn",
                                  hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4))),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        child: InkWell(
                          onTap: () => print("Forgot password"),
                          child: Text(
                            "Quên mật khẩu?",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: sizeHelper.rW(3.5),
                            ),
                          ),
                        ),
                      ),
                      Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  padding: EdgeInsets.symmetric(
                                      vertical: sizeHelper.rW(3)),
                                  onPressed: isLoginButtonEnabled(state)
                                      ? _onFormSubmitted
                                      : null,
                                  child: Text(
                                    "Đăng nhập".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: sizeHelper.rW(4)),
                                  ),
                                  color: ColorConstants.BLUE_PRIMARY,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                ),
                              ),
                            ],
                          )),
                      Container(
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                        child: Row(
                          children: [
                            Flexible(
                              flex: 2,
                              fit: FlexFit.tight,
                              child: Container(
                                height: 1,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: [
                                      Colors.transparent,
                                      Colors.black12,
                                      Colors.black26,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Flexible(
                              // flex: 1,
                              // fit: FlexFit.tight,
                              child: Center(
                                child: Text(
                                  "Hoặc",
                                  style: TextStyle(
                                    color: Colors.black54,
                                    fontSize: sizeHelper.rW(3.5),
                                  ),
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 2,
                              fit: FlexFit.tight,
                              child: Container(
                                height: 1,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.centerRight,
                                    end: Alignment.centerLeft,
                                    colors: [
                                      Colors.transparent,
                                      Colors.black12,
                                      Colors.black26,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                          width: double.infinity,
                          height: sizeHelper.rW(11),
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  onPressed: () {
                                    BlocProvider.of<LoginBloc>(context).add(
                                      LoginWithGooglePressed(),
                                    );
                                  },
                                  child: Stack(
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              "https://olma.vn/client/dist/img/google.png",
                                          width: sizeHelper.rW(6.5),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          "Đăng nhập với Google",
                                          style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: sizeHelper.rW(3.8),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                ),
                              ),
                            ],
                          )),
                      Container(
                          width: double.infinity,
                          height: sizeHelper.rW(11),
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  onPressed: () {
                                    BlocProvider.of<LoginBloc>(context).add(
                                      LoginWithFacebookPressed(),
                                    );
                                  },
                                  child: Stack(
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              "https://olma.vn/client/dist/img/facebook.png",
                                          width: sizeHelper.rW(6.5),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          "Đăng nhập với Facebook",
                                          style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: sizeHelper.rW(3.8),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          sizeHelper.rW(1))),
                                ),
                              ),
                            ],
                          )),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => RegisterScreen()));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Bạn chưa có tài khoản? ",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: sizeHelper.rW(3.5),
                              ),
                            ),
                            Text(
                              "Đăng ký ngay",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
