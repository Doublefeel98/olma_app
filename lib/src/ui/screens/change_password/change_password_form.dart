import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/choose_element_address_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

import 'bloc/bloc.dart';

class ChangePasswordForm extends StatefulWidget {
  final User user;

  const ChangePasswordForm({Key key, @required this.user}) : super(key: key);
  @override
  _ChangePasswordFormState createState() => _ChangePasswordFormState();
}

class _ChangePasswordFormState extends State<ChangePasswordForm> {
  TextEditingController _newPasswordController = new TextEditingController();
  TextEditingController _confirmNewPasswordController =
      new TextEditingController();
  TextEditingController _oldPasswordController = new TextEditingController();

  User get _user => widget.user;

  ChangePasswordBloc _editProfileBloc;
  AuthenticationBloc _authenticationBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool get isPopulated {
    bool populated = _newPasswordController.text.isNotEmpty &&
        _confirmNewPasswordController.text.isNotEmpty;
    if (_user.isPassword) {
      populated = populated && _oldPasswordController.text.isNotEmpty;
    }
    return populated;
  }

  bool isChangePasswordButtonEnabled(ChangePasswordState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _editProfileBloc = BlocProvider.of<ChangePasswordBloc>(context);

    _newPasswordController.addListener(_onNewPasswordChanged);
    _confirmNewPasswordController.addListener(_onNewConfirmPasswordChanged);
    _oldPasswordController.addListener(_onOldPasswordChanged);
  }

  @override
  void dispose() {
    _newPasswordController.dispose();
    _confirmNewPasswordController.dispose();
    _oldPasswordController.dispose();
    super.dispose();
  }

  void _onNewPasswordChanged() {
    _editProfileBloc.add(
      ChangePasswordNewPasswordChanged(
          newPassword: _newPasswordController.text),
    );
  }

  void _onNewConfirmPasswordChanged() {
    _editProfileBloc.add(
      ChangePasswordConfirmNewPasswordChanged(
          newPassword: _newPasswordController.text,
          confirmNewPassword: _confirmNewPasswordController.text),
    );
  }

  void _onOldPasswordChanged() {
    _editProfileBloc.add(
      ChangePasswordOldPasswordChanged(
          oldPassword: _oldPasswordController.text),
    );
  }

  void _onFormSubmitted() {
    _editProfileBloc.add(
      ChangePasswordSubmitted(
        oldPassword: _user.isPassword ? _oldPasswordController.text : "",
        newPassword: _newPasswordController.text,
        confirmNewPassword: _confirmNewPasswordController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<ChangePasswordBloc, ChangePasswordState>(
      listener: (context, state) {
        if (state.isSubmitting) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang cập nhật thông tin...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          _authenticationBloc.add(AuthenticationLoggedIn());
          _scaffoldKey.currentState..hideCurrentSnackBar();
          DialogAlert.showMyDialog(
              context: context,
              title: "Thông báo",
              content: state.message,
              onPress: () {
                Navigator.of(context).pop();
              });
        }
        if (state.isFailure) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(state.message),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
          builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            key: _scaffoldKey,
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Đổi mật khẩu",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
            actions: [],
          ),
          body: Column(
            children: <Widget>[
              Flexible(
                child: ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.all(sizeHelper.rW(3)),
                      child: Column(
                        children: <Widget>[
                          _user.isPassword
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Mật khẩu hiện tại",
                                      style: TextStyle(
                                        fontSize: sizeHelper.rW(4),
                                      ),
                                    ),
                                    SizedBox(
                                      height: sizeHelper.rW(2),
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: sizeHelper.rW(0.5)),
                                      margin: EdgeInsets.only(
                                          bottom: sizeHelper.rW(4)),
                                      decoration: BoxDecoration(
                                          color: Color.fromARGB(
                                              255, 255, 255, 255),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black38,
                                              blurRadius: 2,
                                              spreadRadius: 0,
                                              offset: Offset(1, 1),
                                            ),
                                          ],
                                          borderRadius: BorderRadius.circular(
                                              sizeHelper.rW(1))),
                                      child: TextFormField(
                                        controller: _oldPasswordController,
                                        validator: (_) {
                                          return !state.isOldPasswordValid
                                              ? 'Nhập mật khẩu hiện tại không được để trống!'
                                              : null;
                                        },
                                        autovalidate: true,
                                        autocorrect: false,
                                        keyboardType: TextInputType.text,
                                        style: TextStyle(
                                          fontSize: sizeHelper.rW(4),
                                          color: Colors.black,
                                        ),
                                        obscureText: true,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          prefixIcon: Icon(
                                            Icons.vpn_key,
                                            size: sizeHelper.rW(5),
                                            color: ColorConstants.BLUE_PRIMARY,
                                          ),
                                          hintText: "Nhập mật khẩu hiện tại",
                                          hintStyle: TextStyle(
                                            color: Colors.black45,
                                            fontSize: sizeHelper.rW(4),
                                          ),
                                          contentPadding:
                                              EdgeInsets.all(sizeHelper.rW(4)),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : Container(),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Mật khẩu mới",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _newPasswordController,
                                  validator: (_) {
                                    return !state.isNewPasswordValid
                                        ? 'Mật khẩu mới quá ngắn!'
                                        : null;
                                  },
                                  autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      size: sizeHelper.rW(5),
                                      color: ColorConstants.BLUE_PRIMARY,
                                    ),
                                    hintText: "Nhập mật khẩu mới",
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4),
                                    ),
                                    contentPadding:
                                        EdgeInsets.all(sizeHelper.rW(4)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Xác nhận mật khẩu mới",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _confirmNewPasswordController,
                                  validator: (_) {
                                    return !state.isConfirmNewPasswordValid
                                        ? 'Nhập lại mật khẩu mới không trùng khớp!'
                                        : null;
                                  },
                                  autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      size: sizeHelper.rW(5),
                                      color: ColorConstants.BLUE_PRIMARY,
                                    ),
                                    hintText: "Nhập lại mật khẩu mới",
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4),
                                    ),
                                    contentPadding:
                                        EdgeInsets.all(sizeHelper.rW(4)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: sizeHelper.rW(12),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                    onPressed: isChangePasswordButtonEnabled(state)
                        ? _onFormSubmitted
                        : null,
                    child: Text('Cập nhật'.toUpperCase(),
                        style: TextStyle(
                            fontSize: sizeHelper.rW(4.2),
                            fontWeight: FontWeight.w400)),
                    color: ColorConstants.BLUE_PRIMARY,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
