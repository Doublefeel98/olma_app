import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/ui/screens/change_password/change_password_form.dart';

import 'bloc/bloc.dart';

class ChangePasswordScreen extends StatelessWidget {
  final User user;

  const ChangePasswordScreen({Key key, @required this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _userRepository = RepositoryProvider.of<UserRepository>(context);
    return BlocProvider<ChangePasswordBloc>(
      create: (context) => ChangePasswordBloc(userRepository: _userRepository),
      child: ChangePasswordForm(user: user),
    );
  }
}
