import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ChangePasswordEvent extends Equatable {
  const ChangePasswordEvent();

  @override
  List<Object> get props => [];
}

class ChangePasswordOldPasswordChanged extends ChangePasswordEvent {
  final String oldPassword;

  const ChangePasswordOldPasswordChanged({@required this.oldPassword});

  @override
  List<Object> get props => [oldPassword];

  @override
  String toString() => 'OldPasswordChanged { name :$oldPassword }';
}

class ChangePasswordNewPasswordChanged extends ChangePasswordEvent {
  final String newPassword;

  const ChangePasswordNewPasswordChanged({@required this.newPassword});

  @override
  List<Object> get props => [newPassword];

  @override
  String toString() => 'NewPassword { newPassword: $newPassword }';
}

class ChangePasswordConfirmNewPasswordChanged extends ChangePasswordEvent {
  final String confirmNewPassword;
  final String newPassword;

  const ChangePasswordConfirmNewPasswordChanged(
      {@required this.newPassword, @required this.confirmNewPassword});

  @override
  List<Object> get props => [newPassword, confirmNewPassword];

  @override
  String toString() =>
      'ConfirmPasswordChanged { ConfirmPassword: $confirmNewPassword }';
}

class ChangePasswordSubmitted extends ChangePasswordEvent {
  final String oldPassword;
  final String newPassword;
  final String confirmNewPassword;

  const ChangePasswordSubmitted({
    this.oldPassword = "",
    @required this.newPassword,
    @required this.confirmNewPassword,
  });

  @override
  List<Object> get props => [oldPassword, newPassword, confirmNewPassword];

  @override
  String toString() {
    return 'Submitted { email: $oldPassword, password: $newPassword, confirmPassword: $confirmNewPassword }';
  }
}
