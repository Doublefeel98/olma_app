import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final UserRepository _userRepository;

  ChangePasswordBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ChangePasswordState get initialState => ChangePasswordState.initial();

  @override
  Stream<Transition<ChangePasswordEvent, ChangePasswordState>> transformEvents(
    Stream<ChangePasswordEvent> events,
    TransitionFunction<ChangePasswordEvent, ChangePasswordState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! ChangePasswordOldPasswordChanged &&
          event is! ChangePasswordNewPasswordChanged &&
          event is! ChangePasswordConfirmNewPasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is ChangePasswordOldPasswordChanged ||
          event is ChangePasswordNewPasswordChanged ||
          event is ChangePasswordConfirmNewPasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<ChangePasswordState> mapEventToState(
    ChangePasswordEvent event,
  ) async* {
    if (event is ChangePasswordOldPasswordChanged) {
      yield* _mapChangePasswordOldPasswordChangedToState(event.oldPassword);
    } else if (event is ChangePasswordNewPasswordChanged) {
      yield* _mapChangePasswordNewPasswordChangedToState(event.newPassword);
    } else if (event is ChangePasswordConfirmNewPasswordChanged) {
      yield* _mapChangePasswordConfirmNewPasswordChangedToState(
          event.newPassword, event.confirmNewPassword);
    } else if (event is ChangePasswordSubmitted) {
      yield* _mapChangePasswordSubmittedToState(
          event.oldPassword, event.newPassword, event.confirmNewPassword);
    }
  }

  Stream<ChangePasswordState> _mapChangePasswordOldPasswordChangedToState(
      String oldPassword) async* {
    yield state.update(
      isOldPasswordValid: oldPassword.isNotEmpty,
    );
  }

  Stream<ChangePasswordState> _mapChangePasswordNewPasswordChangedToState(
      String newPassword) async* {
    yield state.update(
      isNewPasswordValid: Validators.isValidPassword(newPassword),
    );
  }

  Stream<ChangePasswordState>
      _mapChangePasswordConfirmNewPasswordChangedToState(
          String newPassword, String confirmNewPassword) async* {
    yield state.update(
      isConfirmNewPasswordValid:
          Validators.isValidConfirmPassword(newPassword, confirmNewPassword),
    );
  }

  Stream<ChangePasswordState> _mapChangePasswordSubmittedToState(
      String oldPassword,
      String newPassword,
      String confirmNewPassword) async* {
    yield ChangePasswordState.loading();
    try {
      ResultUserResponse response = await _userRepository.updatePassword(
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmNewPassword: confirmNewPassword,
      );
      if (response.success) {
        yield ChangePasswordState.success(response.message);
      } else {
        yield ChangePasswordState.failure(response.message);
      }
    } catch (e) {
      yield ChangePasswordState.failure(e.toString());
    }
  }
}
