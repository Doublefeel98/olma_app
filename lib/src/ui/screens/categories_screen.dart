import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/category_product.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/category_detail/category_detail_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';
import 'package:olmaapp/src/ui/widgets/list_category_home/bloc/bloc.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<ListCategoryHomeBloc, ListCategoryHomeState>(
      listener: (context, state) {
        if (state is ListCategoryHomeErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<ListCategoryHomeBloc, ListCategoryHomeState>(
          builder: (context, state) {
        Widget widget;
        if (state is ListCategoryHomeLoadedState) {
          widget = buildListCategory(state.listCategoryHome, sizeHelper);
        } else if (state is ListCategoryHomeErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildContainer(widget);
      }),
    );
  }

  Widget buildItemTopLevel(
      CategoryProduct categoryProduct, int index, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        this.setState(() {
          _currentIndex = index;
        });
      },
      child: Container(
        color: _currentIndex == index ? Colors.white : Colors.transparent,
        child: Padding(
          padding: EdgeInsets.all(sizeHelper.rW(1)),
          child: Container(
            padding: EdgeInsets.all(sizeHelper.rW(1)),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: sizeHelper.rW(10),
                  width: sizeHelper.rW(10),
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                                ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                            .withAlpha(50),
                        ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                                ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                            .withAlpha(200),
                      ],
                    ),
                  ),
                  child: CachedNetworkImage(
                    imageUrl: categoryProduct.imageUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
                SizedBox(height: sizeHelper.rW(1)),
                Container(
                  child: Text(
                    categoryProduct.name,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: sizeHelper.rW(2.5),
                      color: ColorConstants.ITEM_CATEGORY_HOME_TEXT,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buidItemTwoLevel(
      CategoryProduct categoryProduct, SizeHelper sizeHelper) {
    return SectionWrapper(
      hasTitleMarginBottom: false,
      onTapReadMore: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                CategoryDetailScreen(categoryId: categoryProduct.id),
          ),
        );
      },
      title: categoryProduct.name,
      hasMarginBottom: true,
      textReadMore: "XEM TẤT CẢ",
      child: Wrap(
        children: categoryProduct.children != null
            ? categoryProduct.children
                .map((item) => buidItemThreeLevel(
                    item, categoryProduct.children.indexOf(item), sizeHelper))
                .toList()
            : [Container()],
      ),
    );
  }

  Widget buidItemThreeLevel(
      CategoryProduct categoryProduct, int index, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                CategoryDetailScreen(categoryId: categoryProduct.id),
          ),
        );
      },
      child: FractionallySizedBox(
        widthFactor: 0.3333333,
        child: Container(
          padding: EdgeInsets.all(sizeHelper.rW(1)),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
          ),
          child: Column(
            children: [
              Container(
                height: sizeHelper.rW(10),
                width: sizeHelper.rW(10),
                padding: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.circular(sizeHelper.rW(2))),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                              ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                          .withAlpha(50),
                      ColorConstants.ITEM_CATEGORY_HOME_BG[index %
                              ColorConstants.ITEM_CATEGORY_HOME_BG.length]
                          .withAlpha(200),
                    ],
                  ),
                ),
                child: CachedNetworkImage(
                  imageUrl: categoryProduct.imageUrl,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
              SizedBox(height: sizeHelper.rW(1)),
              Container(
                child: Text(
                  categoryProduct.name,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: sizeHelper.rW(2.5),
                    color: ColorConstants.ITEM_CATEGORY_HOME_TEXT,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildListItemTwoLevel(List<CategoryProduct> listCategoryProducts,
      CategoryProduct parentCategory, SizeHelper sizeHelper) {
    return Expanded(
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      CategoryDetailScreen(categoryId: parentCategory.id),
                ),
              );
            },
            child: Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(
                sizeHelper.rW(2),
              ),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.black12,
                    width: 0.5,
                  ),
                ),
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    parentCategory.name.toUpperCase(),
                    style: TextStyle(
                      fontSize: sizeHelper.rW(3.7),
                      fontWeight: FontWeight.w500,
                      color: ColorConstants.GREY_TEXT,
                    ),
                  ),
                  Icon(Icons.keyboard_arrow_right)
                ],
              ),
            ),
          ),
          Expanded(
            child: listCategoryProducts != null
                ? ListView(
                    children: listCategoryProducts
                        .map((item) => buidItemTwoLevel(item, sizeHelper))
                        .toList(),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }

  Widget buildListCategory(
      List<CategoryProduct> listCategory, SizeHelper sizeHelper) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Tất cả danh mục",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          CartButton(),
        ],
      ),
      body: Row(
        children: [
          Container(
            width: sizeHelper.rW(18),
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(color: Colors.black12, width: 0.5),
              ),
              color: ColorConstants.GREY_APP_BG,
            ),
            child: ListView(
              children: listCategory
                  .map((item) => buildItemTopLevel(
                      item, listCategory.indexOf(item), sizeHelper))
                  .toList(),
            ),
          ),
          buildListItemTwoLevel(listCategory[_currentIndex].children,
              listCategory[_currentIndex], sizeHelper),
        ],
      ),
    );
  }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}
