import 'package:flutter/material.dart' hide Banner;
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loadmore/loadmore.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/category_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/deal_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/promotion_detail/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/result_search/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/result_search/bloc/result_search_bloc.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/banner_image.dart';
import 'package:olmaapp/src/ui/widgets/cart_button.dart';
import 'package:olmaapp/src/ui/widgets/item_product.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sticky_headers/sticky_headers.dart';

enum ListProductType { DEAL, PROMOTION, CATEGORY, SEARCH }

class ListProductScreen extends StatefulWidget {
  final ListProductType type;
  final List<ProductItem> listProductItem;
  final String title;
  final int total;
  final Banner banner;
  final bool isFirstTime;
  final bool isFinish;
  final int page;
  final bool isLoadingMore;
  final String order;
  final dynamic id;
  final bool isInit;
  final bool isReload;
  final GlobalKey<ScaffoldState> scaffoldKey;

  final String content;

  const ListProductScreen(
      {Key key,
      this.id,
      @required this.type,
      @required this.listProductItem,
      @required this.title,
      @required this.total,
      this.banner,
      @required this.isFirstTime,
      @required this.isFinish,
      @required this.isLoadingMore,
      @required this.page,
      @required this.order,
      @required this.isInit,
      @required this.isReload,
      @required this.scaffoldKey,
      this.content})
      : super(key: key);

  @override
  _ListProductScreenState createState() => _ListProductScreenState();
}

class _ListProductScreenState extends State<ListProductScreen> {
  DealDetailBloc _dealDetailBloc;
  CategoryDetailBloc _categoryDetailBloc;
  PromotionDetailBloc _promotionDetailBloc;
  ResultSearchBloc _resultSearchBloc;

  GlobalKey<ScaffoldState> get _scaffoldKey => widget.scaffoldKey;

  ListProductType get _type => widget.type;
  List<ProductItem> get _listProductItems => widget.listProductItem;
  String get _title => widget.title;
  int get _total => widget.total;
  Banner get _banner => widget.banner;
  bool get _isFirstTime => widget.isFirstTime;
  bool get _isFinish => widget.isFinish;
  bool get _isLoadingMore => widget.isLoadingMore;
  int get _page => widget.page;
  dynamic get _id => widget.id;
  String get _orderBy => widget.order;
  String get _content => widget.content;
  bool get _isInit => widget.isInit;
  bool get _isReload => widget.isReload;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switch (_type) {
      case ListProductType.DEAL:
        _dealDetailBloc = BlocProvider.of<DealDetailBloc>(context);
        break;
      case ListProductType.CATEGORY:
        _categoryDetailBloc = BlocProvider.of<CategoryDetailBloc>(context);
        break;
      case ListProductType.PROMOTION:
        _promotionDetailBloc = BlocProvider.of<PromotionDetailBloc>(context);
        break;
      case ListProductType.SEARCH:
        _resultSearchBloc = BlocProvider.of<ResultSearchBloc>(context);
        break;
      default:
    }
  }

  List<Widget> listYourProduct() {
    if (_listProductItems != null) {
      List<Widget> _list = _listProductItems
          .map((item) => ItemProduct(
                productItem: item,
                type: ItemProductType.NORMAL,
              ))
          .toList();
      return _list;
    }
    return <Widget>[Container()];
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title:
            // this._type == ListProductType.SEARCH
            //     ? Container(
            //         height: sizeHelper.rW(10),
            //         padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(2)),
            //         decoration: BoxDecoration(
            //             color: Colors.white,
            //             borderRadius:
            //                 BorderRadius.all(Radius.circular(sizeHelper.rW(0.5)))),
            //         child: Theme(
            //           data: Theme.of(context).copyWith(primaryColor: Colors.black),
            //           child: TextField(
            //             style: TextStyle(fontSize: sizeHelper.rW(3.5)),
            //             decoration: InputDecoration(
            //               border: OutlineInputBorder(
            //                 borderSide:
            //                     BorderSide(width: 0.0, style: BorderStyle.none),
            //               ),
            //               hintText: 'Tìm kiếm trên Olma',
            //               hintStyle: TextStyle(
            //                 color: Colors.black54,
            //                 fontSize: sizeHelper.rW(3.5),
            //                 fontWeight: FontWeight.w400,
            //               ),
            //               icon: Icon(
            //                 Icons.search,
            //                 size: sizeHelper.rW(4.5),
            //                 color: Colors.black54,
            //               ),
            //               contentPadding: EdgeInsets.only(left: -10),
            //             ),
            //           ),
            //         ),
            //       )
            //     :
            Text(
          "$_title ($_total sản phẩm)",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: <Widget>[
          // IconButton(
          //   icon: Icon(
          //     Icons.filter_list,
          //     color: Colors.white,
          //     size: sizeHelper.rW(6),
          //   ),
          //   onPressed: () => _scaffoldKey.currentState.openDrawer(),
          // ),
          CartButton(),
        ],
      ),
      drawer: new Drawer(),
      body: Container(
          child: LoadMore(
        isFinish: _isFinish,
        onLoadMore: _loadMore,
        textBuilder: DefaultLoadMoreTextBuilder.english,
        child: ListView(
          children: [
            !_isInit
                ? (_type != ListProductType.PROMOTION
                    ? BannerImage(banner: _banner)
                    : Container())
                : buildLoadingBanner(sizeHelper),
            StickyHeader(
              header: Container(
                child: Container(
                  height: sizeHelper.rW(10),
                  padding: EdgeInsets.all(sizeHelper.rW(1)),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.black26, width: 0.5),
                      top: BorderSide(color: Colors.black26, width: 0.5),
                    ),
                    color: Colors.white,
                  ),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      InkWell(
                        onTap: _orderBy == OrderBy.QUANTITY_SOLD
                            ? null
                            : () => {setOrderBy(OrderBy.QUANTITY_SOLD)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          child: Text(
                            "Bán chạy",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.QUANTITY_SOLD
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.QUANTITY_SOLD
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _orderBy == OrderBy.NEWEST
                            ? null
                            : () => {setOrderBy(OrderBy.NEWEST)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Mới nhất",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.NEWEST
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.NEWEST
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _orderBy == OrderBy.VIEWEST
                            ? null
                            : () => {setOrderBy(OrderBy.VIEWEST)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Xem nhiều",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.VIEWEST
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.VIEWEST
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _orderBy == OrderBy.LOWEST_PRICE
                            ? null
                            : () => {setOrderBy(OrderBy.LOWEST_PRICE)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Giá cao",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.LOWEST_PRICE
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.LOWEST_PRICE
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _orderBy == OrderBy.HIGHEST_PRICE
                            ? null
                            : () => {setOrderBy(OrderBy.HIGHEST_PRICE)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Giá thấp",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.HIGHEST_PRICE
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.HIGHEST_PRICE
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: _orderBy == OrderBy.HIGHEST_RATING
                            ? null
                            : () => {setOrderBy(OrderBy.HIGHEST_RATING)},
                        child: Container(
                          height: sizeHelper.rW(8),
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: sizeHelper.rW(1),
                              horizontal: sizeHelper.rW(2)),
                          margin: EdgeInsets.symmetric(
                              horizontal: sizeHelper.rW(1)),
                          decoration: BoxDecoration(),
                          child: Text(
                            "Đánh giá cao",
                            style: TextStyle(
                                fontSize: sizeHelper.rW(3.5),
                                fontWeight: _orderBy == OrderBy.HIGHEST_RATING
                                    ? FontWeight.w500
                                    : FontWeight.w400,
                                color: _orderBy == OrderBy.HIGHEST_RATING
                                    ? Colors.black
                                    : ColorConstants.GREY_TEXT),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              content: _isReload
                  ? buildLoadingProducts(sizeHelper)
                  : Wrap(
                      children: listYourProduct(),
                    ),
            ),
            _isLoadingMore && !_isReload
                ? buildLoadingProducts(sizeHelper)
                : Container(),
          ],
        ),
      )),
    );
  }

  Widget buildLoadingProducts(SizeHelper sizeHelper) {
    double _pItemProduct = sizeHelper.rW(2.5);
    double _wItemProduct = sizeHelper.rW(40.0);
    return Shimmer.fromColors(
      period: Duration(seconds: 1),
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: Wrap(
        children: [0, 1, 2, 3]
            .map(
              (_) => FractionallySizedBox(
                widthFactor: 0.5,
                child: Container(
                  padding: EdgeInsets.all(_pItemProduct),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
                        child: AspectRatio(
                          aspectRatio: 3 / 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(3)),
                            child: Container(
                              width: double.infinity,
                              height: double.infinity,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: sizeHelper.rW(2.5),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(1.5)),
                        color: Colors.white,
                      ),
                      Container(
                        height: sizeHelper.rW(2.5),
                        width: sizeHelper.rW(30),
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(1.5)),
                        color: Colors.white,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(1.5)),
                        height: sizeHelper.rW(3),
                        width: sizeHelper.rW(20),
                        color: Colors.white,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                        height: sizeHelper.rW(2),
                        width: sizeHelper.rW(35),
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  Widget buildLoadingBanner(SizeHelper sizeHelper) {
    return Shimmer.fromColors(
      period: Duration(seconds: 1),
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: Column(
        children: <Widget>[
          SizedBox(
            height: sizeHelper.rW(2),
          ),
          Center(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                boxShadow: [
                  BoxShadow(
                    color: ColorConstants.BANNER_HOME_PRIMARY_SHAWDOW,
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: Offset(0, 0),
                  )
                ],
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: Container(
                  color: Colors.white,
                  width: sizeHelper.rW(78),
                  height: sizeHelper.rH(18),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
          ),
          Center(
            child: Container(
              width: 40.0,
              height: 3.0,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  Future<bool> _loadMore() async {
    print("onLoadMore list product");
    switch (_type) {
      case ListProductType.DEAL:
        if (!_isFirstTime &&
            !(_dealDetailBloc.state is DealDetailLoadingMoreState) &&
            !(_dealDetailBloc.state is FetchDealDetailEvent) &&
            !_isFinish) {
          _dealDetailBloc.add(LoadMoreDealDetailEvent(
              order: _orderBy,
              dealId: _id,
              page: _page + 1,
              listProductItems: _listProductItems));
        }
        break;
      case ListProductType.CATEGORY:
        if (!_isFirstTime &&
            !(_categoryDetailBloc.state is CategoryDetailLoadingMoreState) &&
            !(_categoryDetailBloc.state is FetchCategoryDetailEvent) &&
            !_isFinish) {
          _categoryDetailBloc.add(LoadMoreCategoryDetailEvent(
              order: _orderBy,
              categoryId: _id,
              page: _page + 1,
              listProductItems: _listProductItems));
        }
        break;
      case ListProductType.PROMOTION:
        if (!_isFirstTime &&
            !(_promotionDetailBloc.state is PromotionDetailLoadingMoreState) &&
            !(_promotionDetailBloc.state is FetchPromotionDetailEvent) &&
            !_isFinish) {
          _promotionDetailBloc.add(LoadMorePromotionDetailEvent(
              order: _orderBy,
              promotionId: _id,
              page: _page + 1,
              listProductItems: _listProductItems));
        }
        break;
      case ListProductType.SEARCH:
        if (!_isFirstTime &&
            !(_resultSearchBloc.state is ResultSearchLoadingMoreState) &&
            !(_resultSearchBloc.state is FetchResultSearchEvent) &&
            !_isFinish) {
          _resultSearchBloc.add(LoadMoreResultSearchEvent(
              order: _orderBy,
              search: _id,
              page: _page + 1,
              listProductItems: _listProductItems));
        }
        break;
      default:
    }

    return true;
  }

  void setOrderBy(String orderBy) {
    switch (_type) {
      case ListProductType.DEAL:
        _dealDetailBloc.add(FetchDealDetailEvent(order: orderBy, dealId: _id));
        break;
      case ListProductType.CATEGORY:
        _categoryDetailBloc
            .add(FetchCategoryDetailEvent(order: orderBy, categoryId: _id));
        break;
      case ListProductType.PROMOTION:
        _promotionDetailBloc
            .add(FetchPromotionDetailEvent(order: orderBy, promotionId: _id));
        break;
      default:
    }
  }
}
