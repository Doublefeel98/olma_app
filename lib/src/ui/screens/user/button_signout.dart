import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/blocs/cart/cart_bloc.dart';
import 'package:olmaapp/src/blocs/cart/cart_event.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class ButtonSignOut extends StatelessWidget {
  ButtonSignOut({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
      SizeHelper sizeHelper = SizeHelper(context);
      if (state is AuthenticationSuccess) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
          child: RaisedButton(
            onPressed: () {
              BlocProvider.of<AuthenticationBloc>(context).add(
                AuthenticationLoggedOut(),
              );
              BlocProvider.of<CartBloc>(context).add(
                ClearCartEvent(),
              );
            },
            child: Text(
              "Đăng xuất",
              style: TextStyle(
                  fontSize: sizeHelper.rW(4),
                  color: ColorConstants.BLUE_PRIMARY_MEDIUM),
            ),
            padding: EdgeInsets.symmetric(
              horizontal: sizeHelper.rW(8),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2),
              side: BorderSide(color: ColorConstants.BLUE_PRIMARY_MEDIUM),
            ),
          ),
        );
      }
      return Container();
    });
  }
}
