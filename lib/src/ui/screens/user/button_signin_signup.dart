import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/login/login.dart';
import 'package:olmaapp/src/ui/screens/register/register.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class ButtonSignInSignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: Column(
        children: <Widget>[
          Center(
            child: ClipRRect(
              child: Container(
                padding: EdgeInsets.all(sizeHelper.rW(2)),
                margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(100),
                  ),
                ),
                child: Icon(
                  Icons.person,
                  size: sizeHelper.rW(15),
                  color: ColorConstants.GREY_TEXT,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => LoginScreen()));
                    },
                    child: Text(
                      "Đăng nhập",
                      style: TextStyle(
                          fontSize: sizeHelper.rW(3.5), color: Colors.white),
                    ),
                    color: ColorConstants.BLUE_PRIMARY_MEDIUM,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side:
                          BorderSide(color: ColorConstants.BLUE_PRIMARY_MEDIUM),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: sizeHelper.rW(2)),
                ),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => RegisterScreen()));
                    },
                    child: Text(
                      "Đăng ký",
                      style: TextStyle(
                          fontSize: sizeHelper.rW(3.5),
                          color: ColorConstants.BLUE_PRIMARY_TEXT),
                    ),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side:
                          BorderSide(color: ColorConstants.BLUE_PRIMARY_MEDIUM),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
