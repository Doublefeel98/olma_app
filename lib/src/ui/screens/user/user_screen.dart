import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/authentication_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/authentication_state.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/address_screen.dart';
import 'package:olmaapp/src/ui/screens/profile_screen.dart';
import 'package:olmaapp/src/ui/screens/purchase/purchase_screen.dart';
import 'package:olmaapp/src/ui/screens/user/button_signin_signup.dart';
import 'package:olmaapp/src/ui/screens/user/button_signout.dart';
import 'package:olmaapp/src/ui/screens/user/user_info.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:url_launcher/url_launcher.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
      // if (state is AuthenticationFailure) {
      //   Scaffold.of(context)
      //     ..hideCurrentSnackBar()
      //     ..showSnackBar(
      //       SnackBar(
      //         content: Row(
      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //           children: [Text(state.message), Icon(Icons.error)],
      //         ),
      //         backgroundColor: Colors.red,
      //       ),
      //     );
      // }
    }, child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
      if (state is AuthenticationSuccess) {
        return buildUserScreenContainer(sizeHelper, state.user);
      }
      return buildUserScreenContainer(sizeHelper, null);
    }));
  }

  Widget buildUserScreenContainer(SizeHelper sizeHelper, User user) {
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
          child: Stack(
            children: <Widget>[
              Container(
                height: sizeHelper.rH(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(200.0, 20.0),
                    bottomRight: Radius.elliptical(200.0, 20.0),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        ColorConstants.BLUE_PRIMARY,
                        ColorConstants.GREY_APP_BG
                      ]),
                ),
              ),
              user != null ? UserInfo(user: user) : ButtonSignInSignUp(),
            ],
          ),
        ),
        SectionWrapper(
          title: "Đơn hàng của bạn",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PurchaseScreen(
                                  type: PurChaseScreenType.IN_PROGRESS)));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đang xử lý",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PurchaseScreen(
                                  type: PurChaseScreenType.IN_TRANSPORT)));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_returned,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đang vận chuyển",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PurchaseScreen(
                                  type: PurChaseScreenType.DONE)));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_turned_in,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng thành công",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PurchaseScreen(
                                  type: PurChaseScreenType.CANCEL)));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_late,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đã hủy",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SectionWrapper(
          title: "Tài khoản",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProfileScreen()));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.account_box,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Thông tin tài khoản",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: user != null
                      ? () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AddressScreen()));
                        }
                      : null,
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.location_on,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Địa chỉ",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SectionWrapper(
          title: "Hỗ trợ",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    _launchURL();
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.help,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Thông tin",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        ButtonSignOut(),
      ],
    );
  }

  _launchURL() async {
    const url = 'https://olma.vn/ho-tro-khach-hang/dieu-khoan-va-quy-dinh';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
