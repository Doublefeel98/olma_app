import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class UserInfo extends StatelessWidget {
  final User user;

  UserInfo({Key key, @required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: Column(
        children: <Widget>[
          Center(
            child: (user.avatar != null && user.avatar.isNotEmpty) ?
            CircleAvatar(
              radius: sizeHelper.rW(10),
              backgroundImage:
              NetworkImage(user.avatar),
              backgroundColor: Colors.transparent,
            ) : ClipRRect(
              child: Container(
                padding: EdgeInsets.all(sizeHelper.rW(2)),
                margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(100),
                  ),
                ),
                child: Icon(
                  Icons.person,
                  size: sizeHelper.rW(15),
                  color: ColorConstants.GREY_TEXT,
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(20)),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    (user.name != null) ? user.name : "",
                    style: TextStyle(
                        fontSize: sizeHelper.rW(4), color: Colors.black),
                  ),
                  Text(
                    (user.email != null) ? user.email : "",
                    style: TextStyle(
                        fontSize: sizeHelper.rW(2.5), color: Colors.black45),
                  )
                ]),
          )
        ],
      ),
    );
  }
}
