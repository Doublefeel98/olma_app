import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/delivery_address/bloc.dart';
import 'package:olmaapp/src/models/deliveryAddress.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';
import 'package:olmaapp/src/ui/widgets/item_delivery_address.dart';

import 'add_address/add_address_screen.dart';

class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<DeliveryAddressBloc, DeliveryAddressState>(
      listener: (context, state) {
        if (state is DeliveryAddressErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<DeliveryAddressBloc, DeliveryAddressState>(
          builder: (context, state) {
        Widget widget;
        if (state is DeliveryAddressLoadedState) {
          widget =
              buildListDeliveryAddress(state.listDeliveryAddress, sizeHelper);
        } else if (state is DeliveryAddressErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }

        return buildDeliveryAddressContainer(widget, sizeHelper);
      }),
    );
  }

  Widget buildListDeliveryAddress(
      List<DeliveryAddress> listDeliveryAddress, SizeHelper sizeHelper) {
    return Column(
      children: listDeliveryAddress
          .map((deliveryAddress) =>
              ItemDeliveryAddress(deliveryAddress: deliveryAddress))
          .toList(),
    );
  }

  Widget buildDeliveryAddressContainer(Widget widget, SizeHelper sizeHelper) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Địa chỉ",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.white,
              size: sizeHelper.rW(6),
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddAddressScreen(),
              ));
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          SectionWrapper(
            title: "Danh sách địa chỉ",
            showReadMore: false,
            child: widget,
          ),
        ],
      ),
    );
  }
}
