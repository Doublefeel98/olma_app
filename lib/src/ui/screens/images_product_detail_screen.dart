import 'package:flutter/material.dart';
import 'package:olmaapp/src/models/product.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/images_products_full.dart';

class ImagesProductDetailScreen extends StatelessWidget {
  final List<ProductImage> images;
  final int initialPage;

  ImagesProductDetailScreen({this.images, this.initialPage});

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        child: ImagesProductFull(
          images: this.images,
          initialPage: this.initialPage,
        ),
      ),
    );
  }
}
