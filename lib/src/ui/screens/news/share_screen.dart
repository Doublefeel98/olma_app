import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/category_news.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/detail_news_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class ShareScreen extends StatefulWidget {
  @override
  _ShareScreenState createState() => _ShareScreenState();
}

class _ShareScreenState extends State<ShareScreen> {
  int _currentIndex = -1;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<NewsBloc, NewsState>(
      listener: (context, state) {
        if (state is NewsErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<NewsBloc, NewsState>(builder: (context, state) {
        Widget widget;
        if (state is NewsLoadedState) {
          widget = buildListCategoryNews(
              state.listCategoryNews, context, sizeHelper);
        } else if (state is NewsErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildContainer(widget);
      }),
    );
  }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildListCategoryNews(List<CategoryNews> listCategoryNews,
      BuildContext context, SizeHelper sizeHelper) {
    return Container(
      child: Column(
        children: [
          Container(
            height: sizeHelper.rW(10),
            padding: EdgeInsets.all(sizeHelper.rW(1)),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Colors.black26, width: 0.5),
              ),
              color: Colors.white,
            ),
            child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  buildCategoryNewsItem(-1, "Tất cả", sizeHelper),
                ]..addAll(listCategoryNews
                    .asMap()
                    .map((index, categoryNews) => MapEntry(
                        index,
                        buildCategoryNewsItem(
                            index, categoryNews.name, sizeHelper)))
                    .values
                    .toList())),
          ),
          buildTabContentItems(listCategoryNews, sizeHelper)
        ],
      ),
    );
  }

  Widget buildTabContentItems(
      List<CategoryNews> listCategoryNews, SizeHelper sizeHelper) {
    return Expanded(
      child: Container(
          color: Colors.white,
          child: _currentIndex == -1
              ? buildAllItems(listCategoryNews, sizeHelper)
              : buildItemsByIndex(listCategoryNews, sizeHelper)),
    );
  }

  Widget buildItemsByIndex(
      List<CategoryNews> listCategoryNews, SizeHelper sizeHelper) {
    return ListView(
      children: listCategoryNews[_currentIndex]
          .listNews
          .map((news) => buildNewsItem(news, sizeHelper))
          .toList(),
    );
  }

  Widget buildAllItems(
      List<CategoryNews> listCategoryNews, SizeHelper sizeHelper) {
    List<News> listNews = new List<News>();
    for (var categoryNews in listCategoryNews) {
      listNews.addAll(categoryNews.listNews);
    }
    return ListView(
      children:
          listNews.map((news) => buildNewsItem(news, sizeHelper)).toList(),
    );
  }

  Widget buildNewsItem(News news, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () => {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => DetailNewsScreen(news: news),
          ),
        )
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(3)),
        margin: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black, width: 0.5),
          ),
        ),
        child: Column(
          children: [
            Text(
              news.title,
              style: TextStyle(
                  fontSize: sizeHelper.rW(4.5), fontWeight: FontWeight.w500),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            SizedBox(
              height: sizeHelper.rW(2),
            ),
            CachedNetworkImage(
              imageUrl: news.poster,
              width: double.infinity,
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              height: sizeHelper.rW(2),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    news.categoryNews.name,
                    style: TextStyle(
                      fontSize: sizeHelper.rW(3.3),
                      color: ColorConstants.GREY_TEXT,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    news.createdAt,
                    style: TextStyle(
                      fontSize: sizeHelper.rW(3.3),
                      color: ColorConstants.GREY_TEXT,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCategoryNewsItem(int index, String name, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        this.setState(() {
          _currentIndex = index;
        });
      },
      child: Container(
        height: sizeHelper.rW(8),
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(
            vertical: sizeHelper.rW(1), horizontal: sizeHelper.rW(2)),
        margin: EdgeInsets.symmetric(horizontal: sizeHelper.rW(1)),
        decoration: BoxDecoration(),
        child: Text(
          name,
          style: TextStyle(
              fontSize: sizeHelper.rW(3.5),
              fontWeight:
                  _currentIndex == index ? FontWeight.w500 : FontWeight.w400,
              color: _currentIndex == index
                  ? Colors.black
                  : ColorConstants.GREY_TEXT),
        ),
      ),
    );
  }
}
