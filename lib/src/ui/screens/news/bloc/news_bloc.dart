import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/category_news.dart';
import 'package:olmaapp/src/repository/news_repository.dart';

import 'bloc.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  NewsRepository newsRepository;

  NewsBloc({@required this.newsRepository});

  @override
  // TODO: implement initialState
  NewsState get initialState => NewsInitialState();

  @override
  Stream<NewsState> mapEventToState(NewsEvent event) async* {
    if (event is FetchNewsEvent) {
      yield NewsLoadingState();
      try {
        List<CategoryNews> listCategoryNews =
            await newsRepository.getListCategoryNewsForHome();
        yield NewsLoadedState(listCategoryNews: listCategoryNews);
      } catch (e) {
        yield NewsErrorState(message: e.toString());
      }
    }
  }
}
