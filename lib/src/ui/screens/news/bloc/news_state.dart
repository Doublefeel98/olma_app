import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/category_news.dart';

abstract class NewsState extends Equatable {}

class NewsInitialState extends NewsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class NewsLoadingState extends NewsState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class NewsLoadedState extends NewsState {
  List<CategoryNews> listCategoryNews;

  NewsLoadedState({@required this.listCategoryNews});

  @override
  // TODO: implement props
  List<Object> get props => [listCategoryNews];
}

class NewsErrorState extends NewsState {
  String message;

  NewsErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
