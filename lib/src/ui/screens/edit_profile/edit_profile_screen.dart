import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/ui/screens/edit_profile/edit_profile_form.dart';

import 'bloc/bloc.dart';

class EditProfileScreen extends StatelessWidget {
  final User user;

  const EditProfileScreen({Key key, @required this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _userRepository = RepositoryProvider.of<UserRepository>(context);
    return BlocProvider<EditProfileBloc>(
      create: (context) => EditProfileBloc(userRepository: _userRepository),
      child: EditProfileForm(user: user),
    );
  }
}
