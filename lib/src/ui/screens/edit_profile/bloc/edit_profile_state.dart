import 'package:meta/meta.dart';

class EditProfileState {
  final bool isNameValid;
  final bool isEmailValid;
  final bool isPhoneNumberValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  String message = '';

  bool get isFormValid => isNameValid && isEmailValid && isPhoneNumberValid;

  EditProfileState(
      {@required this.isNameValid,
      @required this.isEmailValid,
      @required this.isPhoneNumberValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      this.message});

  factory EditProfileState.initial() {
    return EditProfileState(
      isNameValid: true,
      isEmailValid: true,
      isPhoneNumberValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory EditProfileState.loading() {
    return EditProfileState(
      isNameValid: true,
      isEmailValid: true,
      isPhoneNumberValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory EditProfileState.failure() {
    return EditProfileState(
      isNameValid: true,
      isEmailValid: true,
      isPhoneNumberValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory EditProfileState.success(String message) {
    return EditProfileState(
        isNameValid: true,
        isEmailValid: true,
        isPhoneNumberValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        message: message);
  }

  EditProfileState update({
    bool isNameValid,
    bool isEmailValid,
    bool isPasswordValid,
    bool isConfirmPasswordValid,
  }) {
    return copyWith(
      isNameValid: isNameValid,
      isEmailValid: isEmailValid,
      isPhoneNumberValid: isPhoneNumberValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  EditProfileState copyWith(
      {bool isNameValid,
      bool isEmailValid,
      bool isPhoneNumberValid,
      bool isSubmitEnabled,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      String message}) {
    return EditProfileState(
        isNameValid: isNameValid ?? this.isNameValid,
        isEmailValid: isEmailValid ?? this.isEmailValid,
        isPhoneNumberValid: isPhoneNumberValid ?? this.isPhoneNumberValid,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailure ?? this.isFailure,
        message: message ?? this.message);
  }

  @override
  String toString() {
    return '''EditProfileState {
      isNameValid: $isNameValid,
      isEmailValid: $isEmailValid,
      ishoneNumberValid: $isPhoneNumberValid,       
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      message: $message
    }''';
  }
}
