import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/repository/user_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  final UserRepository _userRepository;

  EditProfileBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  EditProfileState get initialState => EditProfileState.initial();

  @override
  Stream<Transition<EditProfileEvent, EditProfileState>> transformEvents(
    Stream<EditProfileEvent> events,
    TransitionFunction<EditProfileEvent, EditProfileState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! EditProfileNameChanged &&
          event is! EditProfileEmailChanged &&
          event is! EditProfilePhoneNumberChanged);
    });
    final debounceStream = events.where((event) {
      return (event is EditProfileNameChanged ||
          event is EditProfileEmailChanged ||
          event is EditProfilePhoneNumberChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<EditProfileState> mapEventToState(
    EditProfileEvent event,
  ) async* {
    if (event is EditProfileNameChanged) {
      yield* _mapEditProfileNameChangedToState(event.name);
    } else if (event is EditProfileEmailChanged) {
      yield* _mapEditProfileEmailChangedToState(event.email);
    } else if (event is EditProfilePhoneNumberChanged) {
      yield* _mapEditProfilePhoneNumberChangedToState(event.phoneNumber);
    } else if (event is EditProfileSubmitted) {
      yield* _mapEditProfileSubmittedToState(
          event.name, event.email, event.phoneNumber);
    }
  }

  Stream<EditProfileState> _mapEditProfileNameChangedToState(
      String name) async* {
    yield state.update(
      isNameValid: name.isNotEmpty,
    );
  }

  Stream<EditProfileState> _mapEditProfileEmailChangedToState(
      String email) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<EditProfileState> _mapEditProfilePhoneNumberChangedToState(
      String phoneNumber) async* {
    yield state.update(
      isPasswordValid: Validators.isValidPassword(phoneNumber),
    );
  }

  Stream<EditProfileState> _mapEditProfileSubmittedToState(
      String name, String email, String phoneNumber) async* {
    yield EditProfileState.loading();
    try {
      ResultUserResponse response = await _userRepository.updateProfile(
        name: name,
        email: email,
        phoneNumber: phoneNumber,
      );
      if (response.success) {
        yield EditProfileState.success(response.message);
      } else {
        yield EditProfileState.failure();
      }
    } catch (_) {
      yield EditProfileState.failure();
    }
  }
}
