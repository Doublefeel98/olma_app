import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class EditProfileEvent extends Equatable {
  const EditProfileEvent();

  @override
  List<Object> get props => [];
}

class EditProfileNameChanged extends EditProfileEvent {
  final String name;

  const EditProfileNameChanged({@required this.name});

  @override
  List<Object> get props => [name];

  @override
  String toString() => 'NameChanged { name :$name }';
}

class EditProfileEmailChanged extends EditProfileEvent {
  final String email;

  const EditProfileEmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email :$email }';
}

class EditProfilePhoneNumberChanged extends EditProfileEvent {
  final String phoneNumber;

  const EditProfilePhoneNumberChanged({@required this.phoneNumber});

  @override
  List<Object> get props => [phoneNumber];

  @override
  String toString() => 'PhoneNumberChanged { phoneNumber: $phoneNumber }';
}

class EditProfileSubmitted extends EditProfileEvent {
  final String name;
  final String email;
  final String phoneNumber;

  const EditProfileSubmitted({
    @required this.name,
    @required this.email,
    @required this.phoneNumber,
  });

  @override
  List<Object> get props => [name, email, phoneNumber];

  @override
  String toString() {
    return 'Submitted { name: $name, email: $email, phoneNumber: $phoneNumber}';
  }
}
