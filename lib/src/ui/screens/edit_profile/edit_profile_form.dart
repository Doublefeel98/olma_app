import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/authentication/bloc.dart';
import 'package:olmaapp/src/models/user.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/edit_profile/bloc/bloc.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

class EditProfileForm extends StatefulWidget {
  final User user;

  const EditProfileForm({Key key, @required this.user}) : super(key: key);

  @override
  _EditProfileFormState createState() => _EditProfileFormState();
}

class _EditProfileFormState extends State<EditProfileForm> {
  User get _user => widget.user;

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _phoneNumberController = new TextEditingController();

  EditProfileBloc _editProfileBloc;
  AuthenticationBloc _authenticationBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool get isPopulated {
    bool populated = _nameController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _phoneNumberController.text.isNotEmpty;

    if (_user.email != null && _user.phoneNumber != null) {
      populated = populated && (_nameController.text != _user.name);
    }
    return populated;
  }

  bool isEditProfileButtonEnabled(EditProfileState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _editProfileBloc = BlocProvider.of<EditProfileBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    _nameController.text = _user.name ?? "";
    _emailController.text = _user.email ?? "";
    _phoneNumberController.text = _user.phoneNumber ?? "";

    _nameController.addListener(_onNameChanged);
    _emailController.addListener(_onEmailChanged);
    _phoneNumberController.addListener(_onPhoneNumberChanged);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  void _onNameChanged() {
    _editProfileBloc.add(
      EditProfileNameChanged(name: _nameController.text),
    );
  }

  void _onEmailChanged() {
    _editProfileBloc.add(
      EditProfileEmailChanged(email: _emailController.text),
    );
  }

  void _onPhoneNumberChanged() {
    _editProfileBloc.add(
      EditProfilePhoneNumberChanged(phoneNumber: _phoneNumberController.text),
    );
  }

  void _onFormSubmitted() {
    _editProfileBloc.add(
      EditProfileSubmitted(
        name: _nameController.text,
        email: _emailController.text,
        phoneNumber: _phoneNumberController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<EditProfileBloc, EditProfileState>(
      listener: (context, state) {
        if (state.isSubmitting) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang cập nhật thông tin...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          _scaffoldKey.currentState..hideCurrentSnackBar();
          _authenticationBloc.add(AuthenticationLoggedIn());
          DialogAlert.showMyDialog(
              context: context,
              title: "Thông báo",
              content: state.message,
              onPress: () {
                Navigator.of(context).pop();
              });
        }
        if (state.isFailure) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(state.message),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<EditProfileBloc, EditProfileState>(
          builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Chỉnh sửa thông tin",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
            actions: [],
          ),
          body: Column(
            children: <Widget>[
              Flexible(
                child: ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.all(sizeHelper.rW(3)),
                      child: Column(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Họ và tên",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _nameController,
                                  validator: (_) {
                                    return !state.isNameValid
                                        ? 'Tên không để trống!'
                                        : null;
                                  },
                                  autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.person,
                                      size: sizeHelper.rW(5),
                                      color: ColorConstants.BLUE_PRIMARY,
                                    ),
                                    hintText: "Nhập họ và tên",
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4),
                                    ),
                                    contentPadding:
                                        EdgeInsets.all(sizeHelper.rW(4)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Email",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: _user.email != null
                                        ? Colors.grey[400]
                                        : Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  readOnly: _user.email != null,
                                  controller: _emailController,
                                  validator: (_) {
                                    return !state.isEmailValid
                                        ? 'Email không hợp lệ!'
                                        : null;
                                  },
                                  autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.email,
                                      size: sizeHelper.rW(5),
                                      color: ColorConstants.BLUE_PRIMARY,
                                    ),
                                    hintText: "Nhập email",
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4),
                                    ),
                                    contentPadding:
                                        EdgeInsets.all(sizeHelper.rW(4)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Số điện thoại",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: _user.phoneNumber != null
                                        ? Colors.grey[400]
                                        : Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  readOnly: _user.phoneNumber != null,
                                  controller: _phoneNumberController,
                                  validator: (_) {
                                    return !state.isEmailValid
                                        ? 'Số điện không hợp lệ!'
                                        : null;
                                  },
                                  autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Icon(
                                      Icons.phone,
                                      size: sizeHelper.rW(5),
                                      color: ColorConstants.BLUE_PRIMARY,
                                    ),
                                    hintText: "Nhập số điện thoại",
                                    hintStyle: TextStyle(
                                      color: Colors.black45,
                                      fontSize: sizeHelper.rW(4),
                                    ),
                                    contentPadding:
                                        EdgeInsets.all(sizeHelper.rW(4)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: sizeHelper.rW(12),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                    onPressed: isEditProfileButtonEnabled(state)
                        ? _onFormSubmitted
                        : null,
                    child: Text('Cập nhật'.toUpperCase(),
                        style: TextStyle(
                            fontSize: sizeHelper.rW(4.2),
                            fontWeight: FontWeight.w400)),
                    color: ColorConstants.BLUE_PRIMARY,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
