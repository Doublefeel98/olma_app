import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/models/promotion.dart';
import 'package:olmaapp/src/ui/screens/promotion/bloc/bloc.dart';
import 'package:olmaapp/src/ui/screens/promotion_detail/promotion_detail_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class PromotionScreen extends StatefulWidget {
  @override
  _PromotionScreenState createState() => _PromotionScreenState();
}

class _PromotionScreenState extends State<PromotionScreen> {
  PromotionBloc _promotionBloc;

  @override
  void initState() {
    super.initState();
    _promotionBloc = BlocProvider.of<PromotionBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<PromotionBloc, PromotionState>(
      listener: (context, state) {
        if (state is PromotionErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child:
          BlocBuilder<PromotionBloc, PromotionState>(builder: (context, state) {
        Widget widget;
        if (state is PromotionLoadedState) {
          widget = buildListPromotion(state.listPromotionForHomes, sizeHelper);
        } else if (state is PromotionErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return buildContainer(widget);
      }),
    );
  }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildListPromotion(
      List<PromotionForHome> listPromotionForHomes, SizeHelper sizeHelper) {
    return Container(
      color: Colors.white,
      child: ListView(
        children: listPromotionForHomes
            .map((item) => buildPromotionItem(item, sizeHelper))
            .toList(),
      ),
    );
  }

  Widget buildPromotionItem(
      PromotionForHome promotionForHome, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                PromotionDetailScreen(promotionId: promotionForHome.id),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(3)),
        margin: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black, width: 0.5),
          ),
        ),
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: promotionForHome.banner,
              width: double.infinity,
              // imageBuilder: (context, imageProvider) => Container(
              //   decoration: BoxDecoration(
              //     image: DecorationImage(
              //       image: imageProvider,
              //       fit: BoxFit.scaleDown,
              //     ),
              //   ),
              // ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              height: sizeHelper.rW(2),
            ),
            Text(
              promotionForHome.name,
              style: TextStyle(
                  fontSize: sizeHelper.rW(4.5), fontWeight: FontWeight.w500),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            SizedBox(
              height: sizeHelper.rW(2),
            ),
          ],
        ),
      ),
    );
  }
}
