import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/promotion.dart';
import 'package:olmaapp/src/repository/promotion_repository.dart';
import 'package:olmaapp/src/ui/screens/promotion/bloc/bloc.dart';

class PromotionBloc extends Bloc<PromotionEvent, PromotionState> {
  PromotionRepository promotionRepository;

  PromotionBloc({@required this.promotionRepository});

  @override
  // TODO: implement initialState
  PromotionState get initialState => PromotionInitialState();

  @override
  Stream<PromotionState> mapEventToState(PromotionEvent event) async* {
    if (event is FetchPromotionEvent) {
      yield PromotionLoadingState();
      try {
        List<PromotionForHome> listPromotionForHomes =
            await promotionRepository.getPromotionForHome();
        yield PromotionLoadedState(
            listPromotionForHomes: listPromotionForHomes);
      } catch (e) {
        yield PromotionErrorState(message: e.toString());
      }
    }
  }
}
