import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/banner.dart';
import 'package:olmaapp/src/models/promotion.dart';

abstract class PromotionState extends Equatable {}

class PromotionInitialState extends PromotionState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PromotionLoadingState extends PromotionState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class PromotionLoadedState extends PromotionState {
  List<PromotionForHome> listPromotionForHomes;

  PromotionLoadedState({@required this.listPromotionForHomes});

  @override
  // TODO: implement props
  List<Object> get props => [listPromotionForHomes];
}

class PromotionErrorState extends PromotionState {
  String message;

  PromotionErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
