import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return Container(
      child: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
                vertical: sizeHelper.rW(2), horizontal: sizeHelper.rW(3)),
            padding: EdgeInsets.all(sizeHelper.rW(2)),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(20, 0, 0, 0),
                  blurRadius: 1,
                  spreadRadius: 1,
                  offset: Offset(1, 1),
                ),
              ],
              borderRadius: BorderRadius.all(
                Radius.circular(
                  sizeHelper.rW(1),
                ),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: 2,
                  fit: FlexFit.tight,
                  child: Container(
                    margin: EdgeInsets.only(right: sizeHelper.rW(2)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(sizeHelper.rW(1)),
                      child: Image.network(
                          "https://olma.s3-ap-southeast-1.amazonaws.com/images/5efc6b3b-3626-47cc-b408-a27515faa2cc.jpg"),
                    ),
                  ),
                ),
                Flexible(
                  flex: 6,
                  fit: FlexFit.tight,
                  child: Text(
                    "Tương ớt Sạch VietGap Phúc Lộc Thọ - Cay từ Đất, Chất từ Tâm được giảm giá khủng, hãy nhanh tay đăt mua ngay",
                    style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
