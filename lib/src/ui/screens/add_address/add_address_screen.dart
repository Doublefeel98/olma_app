import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'package:olmaapp/src/ui/screens/add_address/add_address_form.dart';

import 'bloc/bloc.dart';

class AddAddressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _deliveryAddressRepository =
        RepositoryProvider.of<DeliveryAddressRepository>(context);
    return BlocProvider<AddAddressBloc>(
      create: (context) =>
          AddAddressBloc(deliveryAddressRepository: _deliveryAddressRepository),
      child: AddAddressForm(),
    );
  }
}
