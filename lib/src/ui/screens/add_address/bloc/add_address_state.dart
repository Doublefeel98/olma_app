import 'package:meta/meta.dart';

class AddAddressState {
  final bool isNameValid;
  final bool isPhoneValid;
  final bool isAddressValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  String message = '';

  bool get isFormValid => isNameValid && isPhoneValid && isAddressValid;

  AddAddressState(
      {@required this.isNameValid,
      @required this.isPhoneValid,
      @required this.isAddressValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      this.message});

  factory AddAddressState.initial() {
    return AddAddressState(
      isNameValid: true,
      isPhoneValid: true,
      isAddressValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory AddAddressState.loading() {
    return AddAddressState(
      isNameValid: true,
      isPhoneValid: true,
      isAddressValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory AddAddressState.failure() {
    return AddAddressState(
      isNameValid: true,
      isPhoneValid: true,
      isAddressValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory AddAddressState.success(String message) {
    return AddAddressState(
        isNameValid: true,
        isPhoneValid: true,
        isAddressValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        message: message);
  }

  AddAddressState update({
    bool isNameValid,
    bool isPhoneValid,
    bool isAddressValid,
    bool isConfirmAddressValid,
  }) {
    return copyWith(
      isNameValid: isNameValid,
      isPhoneValid: isPhoneValid,
      isAddressValid: isAddressValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  AddAddressState copyWith(
      {bool isNameValid,
      bool isPhoneValid,
      bool isAddressValid,
      bool isSubmitEnabled,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      String message}) {
    return AddAddressState(
        isNameValid: isNameValid ?? this.isNameValid,
        isPhoneValid: isPhoneValid ?? this.isPhoneValid,
        isAddressValid: isAddressValid ?? this.isAddressValid,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailure ?? this.isFailure,
        message: message ?? this.message);
  }

  @override
  String toString() {
    return '''AddAddressState {
      isNameValid: $isNameValid,
      isPhoneValid: $isPhoneValid,
      isAddressValid: $isAddressValid,      
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      message: $message
    }''';
  }
}
