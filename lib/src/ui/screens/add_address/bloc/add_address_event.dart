import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddAddressEvent extends Equatable {
  const AddAddressEvent();

  @override
  List<Object> get props => [];
}

class AddAddressNameChanged extends AddAddressEvent {
  final String name;

  const AddAddressNameChanged({@required this.name});

  @override
  List<Object> get props => [name];

  @override
  String toString() => 'PhoneChanged { phone :$name }';
}

class AddAddressPhoneChanged extends AddAddressEvent {
  final String phone;

  const AddAddressPhoneChanged({@required this.phone});

  @override
  List<Object> get props => [phone];

  @override
  String toString() => 'PhoneChanged { phone :$phone }';
}

class AddAddressAddressChanged extends AddAddressEvent {
  final String address;

  const AddAddressAddressChanged({@required this.address});

  @override
  List<Object> get props => [address];

  @override
  String toString() => 'AddressChanged { address: $address }';
}

class AddAddressSubmitted extends AddAddressEvent {
  final String name;
  final String phone;
  final String cityId;
  final String cityName;
  final String districtId;
  final String districtName;
  final String wardId;
  final String wardName;
  final String address;
  final bool isDefault;

  const AddAddressSubmitted(
      {@required this.name,
      @required this.phone,
      @required this.cityId,
      @required this.cityName,
      @required this.districtId,
      @required this.districtName,
      @required this.wardId,
      @required this.wardName,
      @required this.address,
      @required this.isDefault});

  @override
  List<Object> get props => [name, phone, address];

  @override
  String toString() {
    return 'Add Delivery Address Submitted { name: $name, phone: $phone, address: $address, wardName: $wardName, wardId: $wardId ,districtName: $districtName } ,districtId: $districtId } ,cityName: $cityName } ,cityId: $cityId }';
  }
}
