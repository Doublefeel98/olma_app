import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/repository/delivery_address_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class AddAddressBloc extends Bloc<AddAddressEvent, AddAddressState> {
  final DeliveryAddressRepository _deliveryAddressRepository;

  AddAddressBloc(
      {@required DeliveryAddressRepository deliveryAddressRepository})
      : assert(deliveryAddressRepository != null),
        _deliveryAddressRepository = deliveryAddressRepository;

  @override
  AddAddressState get initialState => AddAddressState.initial();

  @override
  Stream<Transition<AddAddressEvent, AddAddressState>> transformEvents(
    Stream<AddAddressEvent> events,
    TransitionFunction<AddAddressEvent, AddAddressState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! AddAddressPhoneChanged &&
          event is! AddAddressAddressChanged);
    });
    final debounceStream = events.where((event) {
      return (event is AddAddressPhoneChanged ||
          event is AddAddressAddressChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<AddAddressState> mapEventToState(
    AddAddressEvent event,
  ) async* {
    if (event is AddAddressNameChanged) {
      yield* _mapAddAddressNameChangedToState(event.name);
    } else if (event is AddAddressPhoneChanged) {
      yield* _mapAddAddressPhoneChangedToState(event.phone);
    } else if (event is AddAddressAddressChanged) {
      yield* _mapAddAddressAddressChangedToState(event.address);
    } else if (event is AddAddressSubmitted) {
      yield* _mapAddAddressSubmittedToState(
          event.name,
          event.phone,
          event.cityId,
          event.cityName,
          event.districtId,
          event.districtName,
          event.wardId,
          event.wardName,
          event.address,
          event.isDefault);
    }
  }

  Stream<AddAddressState> _mapAddAddressNameChangedToState(String name) async* {
    yield state.update(
      isNameValid: name.isNotEmpty,
    );
  }

  Stream<AddAddressState> _mapAddAddressPhoneChangedToState(
      String phone) async* {
    yield state.update(
      isPhoneValid: Validators.isValidPhone(phone),
    );
  }

  Stream<AddAddressState> _mapAddAddressAddressChangedToState(
      String address) async* {
    yield state.update(
      isAddressValid: address.isNotEmpty,
    );
  }

  Stream<AddAddressState> _mapAddAddressSubmittedToState(
      String name,
      String phone,
      String cityId,
      String cityName,
      String districtId,
      String districtName,
      String wardId,
      String wardName,
      String address,
      bool isDefault) async* {
    yield AddAddressState.loading();
    try {
      bool result = await _deliveryAddressRepository.addDeliveryAddress(
        name: name,
        phone: phone,
        cityId: cityId,
        cityName: cityName,
        districtId: districtId,
        districtName: districtName,
        wardId: wardId,
        wardName: wardName,
        address: address,
        isDefault: isDefault,
      );
      if (result) {
        yield AddAddressState.success("Thêm địa chỉ mới thành công");
      } else {
        yield AddAddressState.failure();
      }
    } catch (_) {
      yield AddAddressState.failure();
    }
  }
}
