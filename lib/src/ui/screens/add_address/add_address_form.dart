import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/address_districs/bloc.dart';
import 'package:olmaapp/src/blocs/address_wards/bloc.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/choose_element_address_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

import 'bloc/bloc.dart';

class AddAddressForm extends StatefulWidget {
  @override
  _AddAddressFormState createState() => _AddAddressFormState();
}

class _AddAddressFormState extends State<AddAddressForm> {
  City city;
  District district;
  Ward ward;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  final TextEditingController _cityController = new TextEditingController();
  final TextEditingController _districtController = new TextEditingController();
  final TextEditingController _wardController = new TextEditingController();

  AddAddressBloc _addAddressBloc;
  AddressDistricsBloc _addressDistricsBloc;
  AddressWardsBloc _addressWardsBloc;

  bool isDefault = true;

  final globalKey = GlobalKey<ScaffoldState>();

  bool get isPopulated =>
      _nameController.text.isNotEmpty &&
      _phoneController.text.isNotEmpty &&
      _addressController.text.isNotEmpty &&
      _cityController.text.isNotEmpty &&
      _districtController.text.isNotEmpty &&
      _wardController.text.isNotEmpty;

  bool isAddButtonEnabled(AddAddressState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _nameController.addListener(_onNameChanged);
    _phoneController.addListener(_onPhoneChanged);
    _addressController.addListener(_onAddressChanged);

    _addAddressBloc = BlocProvider.of<AddAddressBloc>(context);
    _addressDistricsBloc = BlocProvider.of<AddressDistricsBloc>(context);
    _addressWardsBloc = BlocProvider.of<AddressWardsBloc>(context);
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  void _onNameChanged() {
    _addAddressBloc.add(
      AddAddressNameChanged(name: _nameController.text),
    );
  }

  void _onPhoneChanged() {
    _addAddressBloc.add(
      AddAddressPhoneChanged(phone: _phoneController.text),
    );
  }

  void _onAddressChanged() {
    _addAddressBloc.add(
      AddAddressAddressChanged(address: _addressController.text),
    );
  }

  void _onFormSubmitted() {
    _addAddressBloc.add(
      AddAddressSubmitted(
          name: _nameController.text,
          phone: _phoneController.text,
          address: _addressController.text,
          cityId: city.code,
          cityName: city.name,
          districtId: district.code,
          districtName: district.name,
          wardId: ward.code,
          wardName: ward.name,
          isDefault: isDefault),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<AddAddressBloc, AddAddressState>(
      listener: (context, state) {
        if (state.isSubmitting) {
          globalKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Đang thêm địa chỉ...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          globalKey.currentState..hideCurrentSnackBar();
          DialogAlert.showMyDialog(
              context: context,
              title: "Thông báo",
              content: state.message,
              onPress: () {
                Navigator.of(context).pop();
              });
        }
        if (state.isFailure) {
          globalKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Thêm địa chỉ mới thất bại'),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<AddAddressBloc, AddAddressState>(
          builder: (context, state) {
        return Scaffold(
          key: globalKey,
          appBar: AppBar(
            backgroundColor: ColorConstants.BLUE_PRIMARY,
            elevation: 0.0,
            titleSpacing: 0.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.white,
              iconSize: sizeHelper.rW(6),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Thêm địa chỉ nhận hàng",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
            actions: [],
          ),
          body: Column(
            children: <Widget>[
              Flexible(
                child: ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.all(sizeHelper.rW(3)),
                      child: Column(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Họ và tên",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _nameController,
                                  validator: (_) {
                                    return !state.isNameValid
                                        ? 'Tên không hợp lệ!'
                                        : null;
                                  },
                                  // autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.person,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập họ và tên",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Số điện thoại",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _phoneController,
                                  validator: (_) {
                                    return !state.isPhoneValid
                                        ? 'Số điện thoại không hợp lệ!'
                                        : null;
                                  },
                                  // autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.phone,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.phone,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập số điện thoại",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Tỉnh/Thành phố",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  onTap: () {
                                    _navigatorChooseElementAddress(
                                        ChooseElementAddressType.CITY, context);
                                  },
                                  readOnly: true,
                                  keyboardType: TextInputType.text,
                                  controller: _cityController,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.location_city,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập Tỉnh/Thành phố",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Quận/Huyện",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  onTap: city != null
                                      ? () {
                                          _navigatorChooseElementAddress(
                                              ChooseElementAddressType.DISTRICT,
                                              context);
                                        }
                                      : null,
                                  readOnly: true,
                                  controller: _districtController,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.location_on,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập Quận/Huyện",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Phường/Xã",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  onTap: district != null
                                      ? () {
                                          _navigatorChooseElementAddress(
                                              ChooseElementAddressType.WARD,
                                              context);
                                        }
                                      : null,
                                  readOnly: true,
                                  controller: _wardController,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.my_location,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập Phường/Xã",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Tòa nhà, Tên đường...",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                              SizedBox(
                                height: sizeHelper.rW(2),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: sizeHelper.rW(0.5)),
                                margin:
                                    EdgeInsets.only(bottom: sizeHelper.rW(4)),
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black38,
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                        offset: Offset(1, 1),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(
                                        sizeHelper.rW(1))),
                                child: TextFormField(
                                  controller: _addressController,
                                  validator: (_) {
                                    return !state.isAddressValid
                                        ? 'Địa chỉ không hợp lệ!'
                                        : null;
                                  },
                                  // autovalidate: true,
                                  autocorrect: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(
                                    fontSize: sizeHelper.rW(4),
                                    color: Colors.black,
                                  ),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      prefixIcon: Icon(
                                        Icons.home,
                                        size: sizeHelper.rW(5),
                                        color: ColorConstants.BLUE_PRIMARY,
                                      ),
                                      hintText: "Nhập Tòa nhà, Tên đường..",
                                      hintStyle: TextStyle(
                                          color: Colors.black45,
                                          fontSize: sizeHelper.rW(4))),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              SizedBox(
                                width: sizeHelper.rW(5),
                                height: sizeHelper.rW(5),
                                child: Transform.scale(
                                  scale: 0.8,
                                  child: Checkbox(
                                    value: isDefault,
                                    onChanged: (bool value) {
                                      setState(() {
                                        isDefault = value;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              Text(
                                "Đặt làm điạ chỉ mặc định",
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: sizeHelper.rW(12),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                    onPressed:
                        isAddButtonEnabled(state) ? _onFormSubmitted : null,
                    child: Text('THÊM'.toUpperCase(),
                        style: TextStyle(
                            fontSize: sizeHelper.rW(4.2),
                            fontWeight: FontWeight.w400)),
                    color: ColorConstants.BLUE_PRIMARY,
                    textColor: Colors.white,
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }

  _navigatorChooseElementAddress(
      ChooseElementAddressType type, BuildContext context) async {
    switch (type) {
      case ChooseElementAddressType.DISTRICT:
        _addressDistricsBloc.add(FetchAddressDistricsEvent(city.code));
        break;
      case ChooseElementAddressType.WARD:
        _addressWardsBloc.add(FetchAddressWardsEvent(district.code));
        break;
      case ChooseElementAddressType.CITY:
        break;
    }
    dynamic result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChooseElementAddressScreen(type: type)));
    setState(() {
      if (result is City) {
        city = result;
        _cityController.text = city.name;
      } else if (result is District) {
        district = result;
        _districtController.text = district.name;
      } else if (result is Ward) {
        ward = result;
        _wardController.text = district.name;
      }
    });
    print(result);
  }
}
