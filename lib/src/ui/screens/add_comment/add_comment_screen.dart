import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/comment_repository.dart';
import 'package:olmaapp/src/ui/screens/add_comment/add_comment_form.dart';

import 'bloc/bloc.dart';

class AddCommentScreen extends StatelessWidget {
  final int productId;

  const AddCommentScreen({Key key, @required this.productId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _commentRepository =
        RepositoryProvider.of<CommentRepository>(context);
    return BlocProvider<AddCommentBloc>(
      create: (context) =>
          AddCommentBloc(commentRepository: _commentRepository),
      child: AddCommentForm(productId: productId),
    );
  }
}
