import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';
import 'package:olmaapp/src/ui/widgets/comment_product_detail/bloc/bloc.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'bloc/bloc.dart';

class AddCommentForm extends StatefulWidget {
  final int productId;

  const AddCommentForm({Key key, this.productId}) : super(key: key);
  @override
  _AddCommentFormState createState() => _AddCommentFormState();
}

class _AddCommentFormState extends State<AddCommentForm> {
  TextEditingController _contentController = new TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AddCommentBloc _addCommentBloc;
  CommentsProductDetailBloc _commentsProductDetailBloc;

  int get _productId => widget.productId;

  int _rating = 5;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _contentController.addListener(_onContentChanged);

    _addCommentBloc = BlocProvider.of<AddCommentBloc>(context);
    _commentsProductDetailBloc =
        BlocProvider.of<CommentsProductDetailBloc>(context);
  }

  bool get isPopulated => _contentController.text.isNotEmpty && _rating != null;

  bool isAddButtonEnabled(AddCommentState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void dispose() {
    _contentController.dispose();
    super.dispose();
  }

  void _onContentChanged() {
    _addCommentBloc.add(
      AddCommentContentChanged(content: _contentController.text),
    );
  }

  void _onFormSubmitted() {
    _addCommentBloc.add(
      AddCommentSubmitted(
          productId: _productId,
          rating: _rating,
          content: _contentController.text),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return BlocListener<AddCommentBloc, AddCommentState>(listener:
        (context, state) {
      if (state.isSubmitting) {
        _scaffoldKey.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Đang thêm bình luận...'),
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
      }
      if (state.isSuccess) {
        _commentsProductDetailBloc
            .add(FetchCommentsProductDetailEvent(productId: _productId));
        _scaffoldKey.currentState..hideCurrentSnackBar();
        DialogAlert.showMyDialog(
            context: context,
            title: "Thông báo",
            content: state.message,
            onPress: () {
              Navigator.of(context).pop();
            });
      }
      if (state.isFailure) {
        _scaffoldKey.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Thêm địa chỉ mới thất bại'),
                  Icon(Icons.error),
                ],
              ),
              backgroundColor: Colors.red,
            ),
          );
      }
    }, child:
        BlocBuilder<AddCommentBloc, AddCommentState>(builder: (context, state) {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: ColorConstants.BLUE_PRIMARY,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            iconSize: sizeHelper.rW(6),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "Gửi nhận xét",
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(
              color: Colors.white,
              fontSize: sizeHelper.rW(4),
              fontWeight: FontWeight.w400,
            ),
          ),
          actions: [],
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              child: ListView(
                children: [
                  Container(
                    padding: EdgeInsets.all(sizeHelper.rW(3)),
                    child: Column(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Chọn đánh giá của bạn",
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                              ),
                            ),
                            SizedBox(
                              height: sizeHelper.rW(2),
                            ),
                            Center(
                              child: SmoothStarRating(
                                rating: _rating.toDouble(),
                                isReadOnly: false,
                                size: 40,
                                filledIconData: Icons.star,
                                halfFilledIconData: Icons.star_half,
                                defaultIconData: Icons.star_border,
                                starCount: 5,
                                allowHalfRating: false,
                                spacing: 2.0,
                                color: Colors.yellow,
                                borderColor: Colors.yellow,
                                onRated: (value) {
                                  setState(() {
                                    _rating = value.toInt();
                                  });
                                  print("rating value -> $value");
                                },
                              ),
                            ),
                            SizedBox(
                              height: sizeHelper.rW(2),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Nội dung nhận xét",
                              style: TextStyle(
                                fontSize: sizeHelper.rW(4),
                              ),
                            ),
                            SizedBox(
                              height: sizeHelper.rW(2),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: sizeHelper.rW(0.5)),
                              margin: EdgeInsets.only(bottom: sizeHelper.rW(4)),
                              decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black38,
                                      blurRadius: 2,
                                      spreadRadius: 0,
                                      offset: Offset(1, 1),
                                    ),
                                  ],
                                  borderRadius:
                                      BorderRadius.circular(sizeHelper.rW(1))),
                              child: TextFormField(
                                controller: _contentController,
                                validator: (_) {
                                  return !state.isContentValid
                                      ? 'Nội dung nhận xét cần lớn hơn 25 ký tự!'
                                      : null;
                                },
                                autovalidate: true,
                                autocorrect: false,
                                keyboardType: TextInputType.text,
                                style: TextStyle(
                                  fontSize: sizeHelper.rW(4),
                                  color: Colors.black,
                                ),
                                maxLines: 5,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Nhập nhận xét của bạn về sản phẩm",
                                  hintStyle: TextStyle(
                                    color: Colors.black45,
                                    fontSize: sizeHelper.rW(4),
                                  ),
                                  contentPadding:
                                      EdgeInsets.all(sizeHelper.rW(4)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: sizeHelper.rW(12),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                  ),
                  onPressed:
                      isAddButtonEnabled(state) ? _onFormSubmitted : null,
                  child: Text('Gửi nhận xét'.toUpperCase(),
                      style: TextStyle(
                          fontSize: sizeHelper.rW(4.2),
                          fontWeight: FontWeight.w400)),
                  color: ColorConstants.BLUE_PRIMARY,
                  textColor: Colors.white,
                ),
              ),
            )
          ],
        ),
      );
    }));
  }
}
