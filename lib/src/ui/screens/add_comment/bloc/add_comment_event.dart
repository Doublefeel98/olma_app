import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddCommentEvent extends Equatable {
  const AddCommentEvent();

  @override
  List<Object> get props => [];
}

class AddCommentContentChanged extends AddCommentEvent {
  final String content;

  const AddCommentContentChanged({@required this.content});

  @override
  List<Object> get props => [content];

  @override
  String toString() => 'ContentChanged { content :$content }';
}

class AddCommentRatingChanged extends AddCommentEvent {
  final int rating;

  const AddCommentRatingChanged({@required this.rating});

  @override
  List<Object> get props => [rating];

  @override
  String toString() => 'RatingChanged { rating :$rating }';
}

class AddCommentSubmitted extends AddCommentEvent {
  final String content;
  final int rating;
  final int productId;

  const AddCommentSubmitted(
      {@required this.content,
      @required this.rating,
      @required this.productId});

  @override
  List<Object> get props => [content, rating, productId];

  @override
  String toString() {
    return 'Add Screen Submitted { content: $content, rating: $rating, productIductId: $productId }';
  }
}
