import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/repository/comment_repository.dart';
import 'package:olmaapp/src/validators/Validations.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class AddCommentBloc extends Bloc<AddCommentEvent, AddCommentState> {
  final CommentRepository _commentRepository;

  AddCommentBloc({@required CommentRepository commentRepository})
      : assert(commentRepository != null),
        _commentRepository = commentRepository;

  @override
  AddCommentState get initialState => AddCommentState.initial();

  @override
  Stream<Transition<AddCommentEvent, AddCommentState>> transformEvents(
    Stream<AddCommentEvent> events,
    TransitionFunction<AddCommentEvent, AddCommentState> transitionFn,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! AddCommentContentChanged);
    });
    final debounceStream = events.where((event) {
      return (event is AddCommentContentChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      transitionFn,
    );
  }

  @override
  Stream<AddCommentState> mapEventToState(
    AddCommentEvent event,
  ) async* {
    if (event is AddCommentContentChanged) {
      yield* _mapAddCommentContentChangedToState(event.content);
    } else if (event is AddCommentSubmitted) {
      yield* _mapAddCommentSubmittedToState(
          event.productId, event.content, event.rating);
    }
  }

  Stream<AddCommentState> _mapAddCommentContentChangedToState(
      String content) async* {
    yield state.update(
      isContentValid: Validators.isValidContentComment(content),
    );
  }

  Stream<AddCommentState> _mapAddCommentSubmittedToState(
      int productId, String content, int rating) async* {
    yield AddCommentState.loading();
    try {
      bool result = await _commentRepository.postComment(
        productId: productId,
        content: content,
        rating: rating,
      );
      if (result) {
        yield AddCommentState.success("Thêm bình luận thành công!!!");
      } else {
        yield AddCommentState.failure();
      }
    } catch (_) {
      yield AddCommentState.failure();
    }
  }
}
