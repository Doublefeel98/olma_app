import 'package:meta/meta.dart';

@immutable
class AddCommentState {
  final bool isContentValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  String message = '';

  bool get isFormValid => isContentValid;

  AddCommentState(
      {@required this.isContentValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      this.message});

  factory AddCommentState.initial() {
    return AddCommentState(
      isContentValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory AddCommentState.loading() {
    return AddCommentState(
      isContentValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory AddCommentState.failure() {
    return AddCommentState(
      isContentValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
    );
  }

  factory AddCommentState.success(String message) {
    return AddCommentState(
        isContentValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        message: message);
  }

  AddCommentState update({
    bool isContentValid,
  }) {
    return copyWith(
      isContentValid: isContentValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  AddCommentState copyWith(
      {bool isContentValid,
      bool isSubmitEnabled,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      String message}) {
    return AddCommentState(
        isContentValid: isContentValid ?? this.isContentValid,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailure ?? this.isFailure,
        message: message ?? this.message);
  }

  @override
  String toString() {
    return '''AddCommentState {
      isContentValid: $isContentValid,    
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      message: $message
    }''';
  }
}
