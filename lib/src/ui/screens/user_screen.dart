import 'package:flutter/material.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/login_screen.dart';
import 'package:olmaapp/src/ui/screens/register_screen.dart';
import 'package:olmaapp/src/ui/shares/section_wrapper.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);
    return ListView(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
          child: Stack(
            children: <Widget>[
              Container(
                height: sizeHelper.rH(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.elliptical(200.0, 20.0),
                    bottomRight: Radius.elliptical(200.0, 20.0),
                  ),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        ColorConstants.BLUE_PRIMARY,
                        ColorConstants.GREY_APP_BG
                      ]),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: sizeHelper.rW(3)),
                child: Column(
                  children: <Widget>[
                    Center(
                      child: ClipRRect(
                        child: Container(
                          padding: EdgeInsets.all(sizeHelper.rW(2)),
                          margin: EdgeInsets.only(bottom: sizeHelper.rW(3)),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(100),
                            ),
                          ),
                          child: Icon(
                            Icons.person,
                            size: sizeHelper.rW(15),
                            color: ColorConstants.GREY_TEXT,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: sizeHelper.rW(15)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => LoginScreen()));
                              },
                              child: Text(
                                "Đăng nhập",
                                style: TextStyle(
                                    fontSize: sizeHelper.rW(3.5),
                                    color: Colors.white),
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: sizeHelper.rW(4),
                              ),
                              color: ColorConstants.BLUE_PRIMARY_MEDIUM,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2),
                                side: BorderSide(
                                    color: ColorConstants.BLUE_PRIMARY_MEDIUM),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: sizeHelper.rW(2)),
                          ),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => RegisterScreen()));
                              },
                              child: Text(
                                "Đăng ký",
                                style: TextStyle(
                                    fontSize: sizeHelper.rW(3.5),
                                    color: ColorConstants.BLUE_PRIMARY_TEXT),
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: sizeHelper.rW(4),
                              ),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2),
                                side: BorderSide(
                                    color: ColorConstants.BLUE_PRIMARY_MEDIUM),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        SectionWrapper(
          title: "Đơn hàng của bạn",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đang xử lý",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_returned,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đang vận chuyển",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_turned_in,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng thành công",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.assignment_late,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Đơn hàng đã hủy",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SectionWrapper(
          title: "Tài khoản",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.account_box,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Thông tin tài khoản",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.location_on,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Địa chỉ",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SectionWrapper(
          title: "Hỗ trợ",
          showReadMore: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: sizeHelper.rW(3)),
            child: Column(
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(bottom: sizeHelper.rW(2)),
                    padding: EdgeInsets.symmetric(
                        vertical: sizeHelper.rW(2),
                        horizontal: sizeHelper.rW(3)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromARGB(255, 255, 255, 255),
                          Color.fromARGB(255, 240, 240, 240),
                        ],
                      ),
                      borderRadius:
                          BorderRadius.all(Radius.circular(sizeHelper.rW(0.5))),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 0,
                          blurRadius: 1,
                          offset: Offset(0, 0),
                        )
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.help,
                              size: sizeHelper.rW(5),
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(right: sizeHelper.rW(3))),
                            Text(
                              "Thông tin",
                              style: TextStyle(fontSize: sizeHelper.rW(3.5)),
                            ),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: sizeHelper.rW(5),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
