import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/address_cities/bloc.dart';
import 'package:olmaapp/src/blocs/address_districs/bloc.dart';
import 'package:olmaapp/src/blocs/address_wards/bloc.dart';
import 'package:olmaapp/src/models/address.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/utils/utils.dart';

enum ChooseElementAddressType { CITY, DISTRICT, WARD }

class ChooseElementAddressScreen extends StatefulWidget {
  final ChooseElementAddressType type;

  const ChooseElementAddressScreen({Key key, @required this.type})
      : super(key: key);

  @override
  _ChooseElementAddressScreenState createState() =>
      _ChooseElementAddressScreenState();
}

class _ChooseElementAddressScreenState
    extends State<ChooseElementAddressScreen> {
  String title;

  ChooseElementAddressType get _type => widget.type;

  AddressCitesBloc _addressCitesBloc;
  AddressDistricsBloc _addressDistricsBloc;
  AddressWardsBloc _addressWardsBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _addressCitesBloc = BlocProvider.of<AddressCitesBloc>(context);
    _addressDistricsBloc = BlocProvider.of<AddressDistricsBloc>(context);
    _addressWardsBloc = BlocProvider.of<AddressWardsBloc>(context);

    switch (_type) {
      case ChooseElementAddressType.CITY:
        title = "Tỉnh/Thành phố";
        break;
      case ChooseElementAddressType.DISTRICT:
        title = "Quận/Huyện";
        break;
      case ChooseElementAddressType.WARD:
        title = "Phường/Xã";
        break;
      default:
        title = "";
    }
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildItemCity(City city, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, city);
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(3),
          horizontal: sizeHelper.rW(3),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black45, width: 0.5),
          ),
        ),
        child: Text(
          city.name,
          style: TextStyle(fontSize: sizeHelper.rW(4)),
        ),
      ),
    );
  }

  Widget buildItemDistrict(District district, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, district);
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(3),
          horizontal: sizeHelper.rW(3),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black45, width: 0.5),
          ),
        ),
        child: Text(
          district.name,
          style: TextStyle(fontSize: sizeHelper.rW(4)),
        ),
      ),
    );
  }

  Widget buildItemWard(Ward ward, SizeHelper sizeHelper) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, ward);
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeHelper.rW(3),
          horizontal: sizeHelper.rW(3),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black45, width: 0.5),
          ),
        ),
        child: Text(
          ward.name,
          style: TextStyle(fontSize: sizeHelper.rW(4)),
        ),
      ),
    );
  }

  Widget buildListCities(SizeHelper sizeHelper) {
    return BlocListener<AddressCitesBloc, AddressCitesState>(
      listener: (context, state) {
        if (state is AddressCitesErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<AddressCitesBloc, AddressCitesState>(
          builder: (context, state) {
        Widget widget;
        if (state is AddressCitesLoadedState) {
          List<Widget> listWidgets;
          listWidgets = state.listCities.map((item) {
            return buildItemCity(item, sizeHelper);
          }).toList();
          widget = ListView(
            children: listWidgets,
          );
        } else if (state is AddressCitesErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return widget;
      }),
    );
  }

  Widget buildListDistrics(SizeHelper sizeHelper) {
    return BlocListener<AddressDistricsBloc, AddressDistricsState>(
      listener: (context, state) {
        if (state is AddressDistricsErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<AddressDistricsBloc, AddressDistricsState>(
          builder: (context, state) {
        Widget widget;
        if (state is AddressDistricsLoadedState) {
          List<Widget> listWidgets;
          listWidgets = state.listDistrics.map((item) {
            return buildItemDistrict(item, sizeHelper);
          }).toList();
          widget = ListView(
            children: listWidgets,
          );
        } else if (state is AddressDistricsErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return widget;
      }),
    );
  }

  Widget buildListWards(SizeHelper sizeHelper) {
    return BlocListener<AddressWardsBloc, AddressWardsState>(
      listener: (context, state) {
        if (state is AddressWardsErrorState) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<AddressWardsBloc, AddressWardsState>(
          builder: (context, state) {
        Widget widget;
        if (state is AddressWardsLoadedState) {
          List<Widget> listWidgets;
          listWidgets = state.listWards.map((item) {
            return buildItemWard(item, sizeHelper);
          }).toList();
          widget = ListView(
            children: listWidgets,
          );
        } else if (state is AddressWardsErrorState) {
          widget = buildErrorUi(state.message);
        } else {
          widget = buildLoading();
        }
        return widget;
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    Widget widget;
    switch (_type) {
      case ChooseElementAddressType.CITY:
        widget = buildListCities(sizeHelper);
        break;
      case ChooseElementAddressType.DISTRICT:
        widget = buildListDistrics(sizeHelper);
        break;
      case ChooseElementAddressType.WARD:
        widget = buildListWards(sizeHelper);
        break;
      default:
        title = "";
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          title,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(
            color: Colors.white,
            fontSize: sizeHelper.rW(4),
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: [],
      ),
      body: widget,
    );
  }
}
