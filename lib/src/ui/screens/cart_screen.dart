import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/blocs/cart/bloc.dart';
import 'package:olmaapp/src/models/cart.dart';
import 'package:olmaapp/src/ui/constants/color_constants.dart';
import 'package:olmaapp/src/ui/screens/checkout/checkout_screen.dart';
import 'package:olmaapp/src/ui/utils/money_formatter.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';
import 'package:olmaapp/src/ui/widgets/cart_item/cart_item.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  CartBloc _cartBloc;
  bool _isCheckAll;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<CartItem> _listCartItemSelecteds;
  Cart _cart;

  @override
  void initState() {
    super.initState();
    _cartBloc = BlocProvider.of<CartBloc>(context);
    CartState state = _cartBloc.state;

    if (state is CartLoadedState) {
      _cart = state.cart;
    }

    _isCheckAll = false;
    _updateSelectedCartAll();
  }

  void _updateSelectedCartAll() {
    if (_cart != null) {
      setState(() {
        _listCartItemSelecteds =
            _cart.listCartItem.where((element) => element.selected).toList();
      });
    } else {
      _listCartItemSelecteds = List<CartItem>();
    }

    if (_listCartItemSelecteds.length == _cart.listCartItem.length) {
      setState(() {
        _isCheckAll = true;
      });
    } else {
      setState(() {
        _isCheckAll = false;
      });
    }
  }

  void _updateCheckALlAndUnCheckAll(bool value) {
    setState(() {
      _isCheckAll = value;
      if (value) {
        _listCartItemSelecteds =
            _cart.listCartItem.map((cartItem) => cartItem).toList();
      } else {
        _listCartItemSelecteds.clear();
      }
    });
    _fetchDataCartItem();
  }

  void _updateSelectedCartItem(bool result, CartItem cartItem) {
    if (result) {
      if (!_listCartItemSelecteds.contains(cartItem)) {
        _listCartItemSelecteds.add(cartItem);
      }
    } else {
      if (_listCartItemSelecteds.contains(cartItem)) {
        _listCartItemSelecteds.remove(cartItem);
      }
    }
    //setState(() {});
    _fetchDataCartItem();
  }

  void _fetchDataCartItem() {
    if (_listCartItemSelecteds.length > 0) {
      _cartBloc.add(FetchCartEvent(
          listCartItemIdSelecteds:
              _listCartItemSelecteds.map((e) => e.id).toList()));
    } else {
      _cartBloc.add(FetchCartEvent(listCartItemIdSelecteds: [-1]));
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocListener<CartBloc, CartState>(
      listener: (context, state) {
        if (state is CartErrorState) {
          _scaffoldKey.currentState
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(state.message), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state is CartLoadedState) {
          Cart cart = state.cart;
          setState(() {
            _cart = cart;
          });
          _updateSelectedCartAll();
        }
      },
      child: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
        Widget widget;
        int totalPrice = 0;
        int totalProduct = 0;
        int toatalCartitem = 0;
        if (state is CartLoadedState) {
          Cart cart = state.cart;
          widget = buildListCartItem(cart, sizeHelper);
          totalPrice = cart.totalPrice;
          totalProduct = cart.totalProduct;
          toatalCartitem = cart.listCartItem.length;
        } else if (state is CartErrorState) {
          widget = buildErrorUi(state.message);
        } else if (state is CartLoadingState) {
          widget = buildLoading();
        } else {
          widget = Center(
            child: Text("Không có sản phẩm nào trong giỏ hàng"),
          );
        }
        return buildCartContainer(
            totalProduct, totalPrice, toatalCartitem, widget, sizeHelper);
      }),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildListCartItem(Cart cart, SizeHelper sizeHelper) {
    return ListView(
      children: cart.listCartItem
          .map((cartItem) => CartItemWidget(
                cartItem: cartItem,
                scaffoldKey: _scaffoldKey,
                onChane: _updateSelectedCartItem,
              ))
          .toList(),
    );
  }

  Widget buildCartContainer(int totalProduct, int totalPrice, int totalCartItem,
      Widget widget, SizeHelper sizeHelper) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorConstants.BLUE_PRIMARY,
        elevation: 0.0,
        titleSpacing: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: sizeHelper.rW(6),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Giỏ hàng",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: sizeHelper.rW(4),
                fontWeight: FontWeight.w400,
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: sizeHelper.rW(3)),
              child: Text(
                "$totalProduct sản phẩm",
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: sizeHelper.rW(4),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ),
        actions: [],
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: widget,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(
                    horizontal: sizeHelper.rW(3),
                    vertical: sizeHelper.rW(3),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: sizeHelper.rW(5),
                            height: sizeHelper.rW(5),
                            child: Transform.scale(
                              scale: 0.8,
                              child: Checkbox(
                                value: _isCheckAll,
                                onChanged: (bool value) {
                                  _updateCheckALlAndUnCheckAll(value);
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: sizeHelper.rW(2),
                          ),
                          Text(
                            "Chọn tất cả",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: sizeHelper.rW(4),
                            ),
                          ),
                        ],
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Tạm tính: ",
                              style: TextStyle(
                                color: Colors.black87,
                                fontSize: sizeHelper.rW(4),
                              ),
                            ),
                            TextSpan(
                              text:
                                  MoneyFormatter(amount: totalPrice).toString(),
                              style: TextStyle(
                                color: ColorConstants.BLUE_PRIMARY_TEXT,
                                fontSize: sizeHelper.rW(4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: sizeHelper.rW(12),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                    onPressed: totalCartItem > 0
                        ? () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => CheckoutScreen(
                                    listCartItemSelected:
                                        _listCartItemSelecteds)));
                          }
                        : null,
                    child: Text('Đặt hàng ngay'.toUpperCase(),
                        style: TextStyle(
                            fontSize: sizeHelper.rW(4.2),
                            fontWeight: FontWeight.w400)),
                    color: ColorConstants.BLUE_PRIMARY,
                    textColor: Colors.white,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
