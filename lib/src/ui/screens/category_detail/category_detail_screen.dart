import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/repository/product_repository.dart';
import 'package:olmaapp/src/ui/screens/category_detail/category_detail_ui.dart';

import 'bloc/bloc.dart';

class CategoryDetailScreen extends StatelessWidget {
  final int _categoryId;

  CategoryDetailScreen({Key key, @required int categoryId})
      : assert(categoryId != null),
        _categoryId = categoryId,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _productRepository =
        RepositoryProvider.of<ProductRepository>(context);
    return BlocProvider<CategoryDetailBloc>(
      create: (context) =>
          CategoryDetailBloc(productRepository: _productRepository),
      child: CategoryDetailUI(
        categoryId: _categoryId,
      ),
    );
  }
}
