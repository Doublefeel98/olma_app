import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:olmaapp/src/constant/global.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/ui/screens/list_product_screen.dart';
import 'package:olmaapp/src/ui/utils/size_helper.dart';

import 'bloc/bloc.dart';

class CategoryDetailUI extends StatefulWidget {
  final int categoryId;

  const CategoryDetailUI({Key key, this.categoryId}) : super(key: key);

  @override
  _CategoryDetailUIState createState() => _CategoryDetailUIState();
}

class _CategoryDetailUIState extends State<CategoryDetailUI> {
  int get _categoryId => widget.categoryId;
  CategoryDetailBloc _categoryDetailBloc;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _categoryDetailBloc = BlocProvider.of<CategoryDetailBloc>(context);
    _categoryDetailBloc.add(FetchCategoryDetailEvent(
        categoryId: _categoryId, order: OrderBy.QUANTITY_SOLD));
  }

  bool _isFinish = false;

  bool _isFirstTime = true;

  int _page = 1;

  List<ProductItem> _listProducts;

  bool _isLoadingMore = false;

  ListProductForView _listProductForView;

  String _order = OrderBy.QUANTITY_SOLD;

  bool _isInit = true;

  bool _isReload = true;

  @override
  Widget build(BuildContext context) {
    SizeHelper sizeHelper = SizeHelper(context);

    return BlocConsumer<CategoryDetailBloc, CategoryDetailState>(
        listenWhen: (previous, current) {
      // return true/false to determine whether or not
      // to invoke listener with state
    }, listener: (context, state) {
      // do stuff here based on BlocA's state
      if (state is CategoryDetailErrorState) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(state.message), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      } else if (state is CategoryDetailLoadingMoreState) {
        setState(() {
          _isLoadingMore = true;
        });
      } else if (state is CategoryDetailLoadingState) {
        setState(() {
          _isReload = true;
        });
      } else if (state is CategoryDetailLoadedState) {
        this.setState(() {
          _listProductForView = state.listProductForView;
          _listProducts = state.listProductForView.listProducts.data;
          _isFirstTime = false;
          _page = 1;
          _order = state.order;
          _isFinish = state.listProductForView.listProducts.lastPage == _page;
          _isLoadingMore = false;
          _isInit = false;
          _isReload = false;
        });
      } else if (state is CategoryDetailLoadedMoreState) {
        this.setState(() {
          _page = state.page;
          _isFinish = state.isFinish;
          _listProducts = state.listProductItems;
          _isLoadingMore = false;
        });
      }
    }, buildWhen: (previous, current) {
      // return true/false to determine whether or not
      // to rebuild the widget with state
      print("previous");
      print(previous);
      print("current");
      print(current);
    }, builder: (context, state) {
      // return widget here based on BlocA's state
      // Widget widget;
      // if (state is CategoryDetailInitialState ||
      //     state is CategoryDetailLoadingState) {
      //   widget = buildLoading();
      // } else if (state is CategoryDetailErrorState) {
      //   widget = buildErrorUi(state.message);
      // } else {
      //   widget = buildCategoryDetail(sizeHelper);
      // }
      // return buildContainer(widget);

      if (state is CategoryDetailErrorState) {
        return buildErrorUi(state.message);
      }

      return ListProductScreen(
        isReload: _isReload,
        scaffoldKey: _scaffoldKey,
        isInit: _isInit,
        title: _listProductForView != null ? _listProductForView.title : "",
        listProductItem: _listProducts,
        total: _listProductForView != null
            ? _listProductForView.listProducts.total
            : 0,
        type: ListProductType.CATEGORY,
        banner: _listProductForView != null ? _listProductForView.banner : null,
        isFinish: _isFinish,
        isFirstTime: _isFirstTime,
        page: _page,
        isLoadingMore: _isLoadingMore,
        id: _categoryId,
        order: _order,
      );
    });
  }

  // Widget buildCategoryDetail(SizeHelper sizeHelper) {
  //   return ListProductScreen(
  //     scaffoldKey: _scaffoldKey,
  //     title: _listProductForView.title,
  //     listProductItem: _listProducts,
  //     total: _listProductForView.listProducts.total,
  //     type: ListProductType.CATEGORY,
  //     banner: _listProductForView.banner,
  //     isFinish: _isFinish,
  //     isFirstTime: _isFirstTime,
  //     page: _page,
  //     isLoading: _isLoading,
  //     id: _categoryId,
  //     order: _order,
  //   );
  // }

  Widget buildContainer(Widget widget) {
    return widget;
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }
}
