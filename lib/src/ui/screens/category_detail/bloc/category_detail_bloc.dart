import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';
import 'package:olmaapp/src/repository/product_repository.dart';

import 'bloc.dart';

class CategoryDetailBloc
    extends Bloc<CategoryDetailEvent, CategoryDetailState> {
  ProductRepository productRepository;

  CategoryDetailBloc({@required this.productRepository});

  @override
  // TODO: implement initialState
  CategoryDetailState get initialState => CategoryDetailInitialState();

  @override
  Stream<CategoryDetailState> mapEventToState(
      CategoryDetailEvent event) async* {
    if (event is FetchCategoryDetailEvent) {
      yield CategoryDetailLoadingState();
      try {
        ListProductForView listProductForView = await productRepository
            .getListProductByCategoryProduct(event.categoryId,
                order: event.order);
        yield CategoryDetailLoadedState(
            listProductForView: listProductForView, order: event.order);
      } catch (e) {
        yield CategoryDetailErrorState(message: e.toString());
      }
    } else if (event is LoadMoreCategoryDetailEvent) {
      yield CategoryDetailLoadingMoreState();
      try {
        ListProductForView listProductForView = await productRepository
            .getListProductByCategoryProduct(event.categoryId,
                order: event.order, page: event.page);
        List<ProductItem> listProductItem = event.listProductItems;

        listProductItem.addAll(listProductForView.listProducts.data);

        bool isFinish = false;
        if (event.page == listProductForView.listProducts.lastPage) {
          isFinish = true;
        }
        yield CategoryDetailLoadedMoreState(
            listProductItems: listProductItem,
            page: event.page,
            isFinish: isFinish);
      } catch (e) {
        yield CategoryDetailErrorState(message: e.toString());
      }
    }
  }
}
