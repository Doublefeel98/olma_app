import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class CategoryDetailState extends Equatable {}

class CategoryDetailInitialState extends CategoryDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryDetailLoadingState extends CategoryDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryDetailLoadedState extends CategoryDetailState {
  final ListProductForView listProductForView;
  final String order;

  CategoryDetailLoadedState(
      {@required this.listProductForView, @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [listProductForView];
}

class CategoryDetailLoadingMoreState extends CategoryDetailState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CategoryDetailLoadedMoreState extends CategoryDetailState {
  final List<ProductItem> listProductItems;

  final bool isFinish;

  final int page;

  CategoryDetailLoadedMoreState(
      {@required this.listProductItems,
      @required this.page,
      this.isFinish = false});

  @override
  // TODO: implement props
  List<Object> get props => [listProductItems, page, isFinish];
}

class CategoryDetailErrorState extends CategoryDetailState {
  final String message;

  CategoryDetailErrorState({@required this.message});

  @override
  // TODO: implement props
  List<Object> get props => [message];
}
