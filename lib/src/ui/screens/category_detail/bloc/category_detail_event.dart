import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:olmaapp/src/models/list_product_item.dart';

abstract class CategoryDetailEvent extends Equatable {}

class FetchCategoryDetailEvent extends CategoryDetailEvent {
  final int categoryId;
  final String order;

  FetchCategoryDetailEvent({@required this.categoryId, @required this.order});

  @override
  List<Object> get props => [categoryId];

  @override
  String toString() => '{ categoryId: $categoryId }';
}

class LoadMoreCategoryDetailEvent extends CategoryDetailEvent {
  final int page;
  final List<ProductItem> listProductItems;
  final int categoryId;
  final String order;

  LoadMoreCategoryDetailEvent(
      {@required this.page,
      @required this.listProductItems,
      @required this.categoryId,
      @required this.order});

  @override
  // TODO: implement props
  List<Object> get props => [page];
}
