const SERVER_NAME = "https://olma.vn/api";
// const SERVER_NAME = "http://192.168.0.108:5001/api";
//const SERVER_NAME = "http://10.0.3.2:5001/api";

const String USER_TOKEN_KEY = "ut";
const String CART_TOKEN_KEY = "cart_token";
const String CART_TOKEN_HEADER = "CartToken";

class OrderBy {
  static const QUANTITY_SOLD = "quantitySold|desc";
  static const NEWEST = "Products.created_at|desc";
  static const VIEWEST = "viewed|desc";
  static const LOWEST_PRICE = "price|desc";
  static const HIGHEST_PRICE = "price|asc";
  static const HIGHEST_RATING = "avgRating|desc";
}

class ShippingMethods {
  static const OlmaNow = "olma_now";
  static const GHTK = "ghtk";
  static const GHN = "ghn";
}

class PaymentMethods {
  static const CashPaymentOnDelivery = "cod";
  static const VisaMasterJCB = "visa_master_jcb";
  static const ATM_OR_InternetBanking = "atm_internet_baking";
  static const MoMoWallet = "momo_wallet";
}

class StockStatus {
  static const ComingSoon = "coming_soon";
  static const PreOrder = "pre_order";
  static const InStock = "in_stock";
  static const OutOfStock = "out_of_stock";
}

class OrderStatuses {
  //Đang chờ xác nhận đơn hàng
  static const WaitingConfirm = 0;
  //Đang chờ xử lý
  static const Pending = 1;
  //Đang xử lý
  static const Processing = 2;
  //Đã xử lý
  static const Processed = 3;
  //Đang chuyển hàng
  static const Shipping = 4;
  //Đã giao hàng
  static const Shiped = 5;
  //Đã hủy bỏ
  static const Canceled = 10;
  //Hủy bỏ không được thực hiện
  static const CanceledReversal = 11;
  //Bồi Hoàn
  static const Chargeback = 12;
  //Đã từ chối nhận hàng
  static const Denied = 13;
  //Đã hết hạn
  static const Expired = 14;
  //Giao hàng thất bại
  static const Failed = 15;
  //Hoàn tiền
  static const Refunded = 16;
  //Đã vô hiệu
  static const Voided = 17;
  //Hoàn thành
  static const Complete = 20;
}
