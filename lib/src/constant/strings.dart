class StringConstant{
  static const String EMAIL_VALIDATE_MESSAGE = "Enter a valid email";
  static const String PASSWORD_VALIDATE_MESSAGE = "Password must be at least 4 characters";
  static const String PASSWORD_HINT = "Enter Password";
  static const String EMAIL_HINT = "Enter Email ID";
  static const String SUBMIT = "Submit";
  static const String ERROR_MESSAGE = "Please fix all the errors";
}