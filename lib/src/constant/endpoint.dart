import 'package:olmaapp/src/constant/global.dart';

class Endpoint {
  static const String URL_GET_CART = SERVER_NAME + '/cart';
  static const String URL_GET_BANNER = SERVER_NAME + '/banner';
  static const String URL_GET_CURRENT_USER = SERVER_NAME + '/user';
  static const String URL_GET_LIST_DEAL_FOR_HOME = SERVER_NAME + '/deal';
  static const String URL_GET_LIST_DEAL_FOR_DETAIL =
      SERVER_NAME + '/deal/detail';
  static const String URL_GET_LIST_PROMOTION_FOR_HOME =
      SERVER_NAME + '/promotion';
  static const String URL_GET_LIST_PROMOTION_FOR_DETAIL =
      SERVER_NAME + '/promotion/detail';
  static const String URL_GET_TREE_CATEGORY =
      SERVER_NAME + '/category-product/tree';
  static const String URL_GET_LIST_PRODUCT_BY_CATEGORY_PRODUCT =
      SERVER_NAME + '/category-product/products';
  static const String URL_GET_LIST_PRODUCT_BY_BRAND =
      SERVER_NAME + '/brand/products';
  static const String URL_GET_LIST_PRODUCT_BY_FILTER = SERVER_NAME + '/search';
  static const String URL_GET_LIST_PRODUCT_SUGGEST_FILTER =
      SERVER_NAME + '/search/suggest';
  static const String URL_GET_LIST_PRODUCT_FOR_YOU = SERVER_NAME + '/for-you';
  static const String URL_GET_LIST_CATEGORY_NEWS =
      SERVER_NAME + '/news/category';
  static const String URL_GET_NEWS_DETAIL = SERVER_NAME + '/news/';
  static const String URL_GET_PRODUCT_DETAIL = SERVER_NAME + '/product/';
  static const String URL_GET_LIST_COMMENT_PRODUCT =
      SERVER_NAME + '/comment/product/';
  static const String URL_POST_REGISTER = SERVER_NAME + '/register';
  static const String URL_POST_LOGIN_DEFAULT = SERVER_NAME + '/login';
  static const String URL_POST_LOGIN_SOCAIL = SERVER_NAME + '/login/social';
  static const String URL_GET_LIST_DELIVERY_ADDRESS =
      SERVER_NAME + '/user/delivery-address';
  static const String URL_POST_ADD_DELIVERY_ADDRESS =
      SERVER_NAME + '/user/delivery-address/add';
  static const String URL_PUT_UPDATE_DELIVERY_ADDRESS =
      SERVER_NAME + '/user/delivery-address/update';
  static const String URL_PUT_UPDATE_DELIVERY_ADDRESS_DEFAULT =
      SERVER_NAME + '/user/delivery-address/update/default';
  static const String URL_DELETE_REMOVE_DELIVERY_ADDRESS =
      SERVER_NAME + '/user/delivery-address/delete';
  static const String URL_GET_LIST_ADDRESS_CITY = SERVER_NAME + '/address/city';
  static const String URL_GET_LIST_ADDRESS_DISTRICS =
      SERVER_NAME + '/address/city/cityCode/district';
  static const String URL_GET_LIST_ADDRESS_WARD =
      SERVER_NAME + '/address/district/districtCode/ward';
  static const String URL_POST_CALCULATE_FEE =
      SERVER_NAME + "/address/calculate-fee";
  static const String URL_POST_CHECKOUT = SERVER_NAME + "/order/check-out";
  static const String URL_POST_ADD_TO_CART = SERVER_NAME + "/cart/add";
  static const String URL_POST_UPDATE_QUANTITY_CART_ITEM =
      SERVER_NAME + "/cart/quantity/update";
  static const String URL_POST_REMOVE_CART_ITEM = SERVER_NAME + "/cart/remove";
  static const String URL_POST_COMMENT = SERVER_NAME + "/comment/product/add";
  static const String URL_GET_LIST_ORDER = SERVER_NAME + "/order/list";
  static const String URL_POST_UPDATE_PROFILE =
      SERVER_NAME + "/user/profile/update";
  static const String URL_POST_UPDATE_PASSWORD =
      SERVER_NAME + "/user/password/update";
}
