class ShippingMethod {
  bool success;
  String message;
  Fee fee;

  ShippingMethod({this.success, this.message, this.fee});

  ShippingMethod.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    fee = json['fee'] != null ? new Fee.fromJson(json['fee']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.fee != null) {
      data['fee'] = this.fee.toJson();
    }
    return data;
  }
}

class Fee {
  String name;
  int fee;
  int insuranceFee;
  bool isDelivery;
  String serviceName;

  Fee(
      {this.name,
      this.fee,
      this.insuranceFee,
      this.isDelivery,
      this.serviceName});

  Fee.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    fee = json['fee'];
    insuranceFee = json['insuranceFee'];
    isDelivery = json['isDelivery'];
    serviceName = json['ServiceName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['fee'] = this.fee;
    data['insuranceFee'] = this.insuranceFee;
    data['isDelivery'] = this.isDelivery;
    data['ServiceName'] = this.serviceName;
    return data;
  }
}
