class Banner {
  int id;
  String name;
  bool status;
  List<BannerImages> bannerImages;

  Banner({this.id, this.name, this.status, this.bannerImages});

  Banner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    if (json['bannerImages'] != null) {
      bannerImages = new List<BannerImages>();
      json['bannerImages'].forEach((v) {
        bannerImages.add(new BannerImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['status'] = this.status;
    if (this.bannerImages != null) {
      data['bannerImages'] = this.bannerImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BannerImages {
  int id;
  int bannerId;
  String title;
  String link;
  String image;
  int sortOrder;

  BannerImages(
      {this.id,
      this.bannerId,
      this.title,
      this.link,
      this.image,
      this.sortOrder});

  BannerImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bannerId = json['bannerId'];
    title = json['title'];
    link = json['link'];
    image = json['image'];
    sortOrder = json['sortOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bannerId'] = this.bannerId;
    data['title'] = this.title;
    data['link'] = this.link;
    data['image'] = this.image;
    data['sortOrder'] = this.sortOrder;
    return data;
  }
}
