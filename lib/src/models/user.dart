class User {
  int id;
  String email;
  String name;
  String avatar;
  String fullName;
  bool gender;
  DateTime birthday;
  DateTime createdAt;
  String phoneNumber;
  int rewardPoints;
  bool newsletter;
  bool isPassword;

  User({
    this.id,
    this.email,
    this.name,
    this.avatar,
    this.fullName,
    this.gender,
    this.birthday,
    this.phoneNumber,
    this.rewardPoints,
    this.newsletter,
    this.isPassword,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    name = json['name'];
    avatar = json['avatar'];
    fullName = json['fullName'];
    gender = json['gender'];
    birthday =
        json['birthday'] != null ? DateTime.tryParse(json['birthday']) : null;
    createdAt =
        json['createdAt'] != null ? DateTime.tryParse(json['createdAt']) : null;
    phoneNumber = json['phoneNumber'];
    rewardPoints = json['rewardPoints'];
    newsletter = json['newsletter'];
    isPassword = json['isPassword'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['name'] = this.name;
    data['avatar'] = this.avatar;
    data['fullName'] = this.fullName;
    data['gender'] = this.gender;
    data['birthday'] = this.birthday.toString();
    data['phoneNumber'] = this.phoneNumber;
    data['rewardPoints'] = this.rewardPoints;
    data['newsletter'] = this.newsletter;
    data['isPassword'] = this.isPassword;
    return data;
  }
}

class ResultUserResponse {
  bool success;
  String message;

  ResultUserResponse({this.success, this.message});

  ResultUserResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    return data;
  }
}
