class Attribute {
  int id;
  String code;
  String label;
  int attributeGroupId;
  String dataType;
  bool isRequire;
  bool isUnique;
  Null defaultValue;
  Null note;
  List<String> listItem;

  Attribute(
      {this.id,
      this.code,
      this.label,
      this.attributeGroupId,
      this.dataType,
      this.isRequire,
      this.isUnique,
      this.defaultValue,
      this.note,
      this.listItem});

  Attribute.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    label = json['label'];
    attributeGroupId = json['attributeGroupId'];
    dataType = json['dataType'];
    isRequire = json['isRequire'];
    isUnique = json['isUnique'];
    defaultValue = json['defaultValue'];
    note = json['note'];
    listItem = json['listItem'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['label'] = this.label;
    data['attributeGroupId'] = this.attributeGroupId;
    data['dataType'] = this.dataType;
    data['isRequire'] = this.isRequire;
    data['isUnique'] = this.isUnique;
    data['defaultValue'] = this.defaultValue;
    data['note'] = this.note;
    data['listItem'] = this.listItem;
    return data;
  }
}
