class DeliveryAddress {
  int id;
  String name;
  String phone;
  String cityId;
  String cityName;
  String districtId;
  String districtName;
  String wardId;
  String wardName;
  String address;
  bool isDefault;

  DeliveryAddress(
      {this.id,
      this.name,
      this.phone,
      this.cityId,
      this.cityName,
      this.districtId,
      this.districtName,
      this.wardId,
      this.wardName,
      this.address,
      this.isDefault});

  DeliveryAddress.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    cityId = json['cityId'];
    cityName = json['cityName'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    address = json['address'];
    isDefault = json['isDefault'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['cityId'] = this.cityId;
    data['cityName'] = this.cityName;
    data['districtId'] = this.districtId;
    data['districtName'] = this.districtName;
    data['wardId'] = this.wardId;
    data['wardName'] = this.wardName;
    data['address'] = this.address;
    data['isDefault'] = this.isDefault;
    return data;
  }
}
