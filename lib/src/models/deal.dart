import 'banner.dart';
import 'list_product_item.dart';

class DealForHome {
  int id;
  String name;
  String slug;
  List<ProductItem> listDealDetails;

  DealForHome({this.id, this.name, this.slug, this.listDealDetails});

  DealForHome.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    if (json['listDealDetails'] != null) {
      listDealDetails = new List<ProductItem>();
      json['listDealDetails'].forEach((v) {
        listDealDetails.add(new ProductItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    if (this.listDealDetails != null) {
      data['listDealDetails'] =
          this.listDealDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DealForDetail {
  int id;
  String name;
  String slug;
  String description;
  ListProductItems listProducts;
  Banner banner;

  DealForDetail(
      {this.id,
      this.name,
      this.slug,
      this.description,
      this.listProducts,
      this.banner});

  DealForDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    description = json['description'];
    listProducts = json['listProducts'] != null
        ? new ListProductItems.fromJson(json['listProducts'])
        : null;
    banner =
        json['banner'] != null ? new Banner.fromJson(json['banner']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['description'] = this.description;
    if (this.listProducts != null) {
      data['listProducts'] = this.listProducts.toJson();
    }
    if (this.banner != null) {
      data['banner'] = this.banner.toJson();
    }
    return data;
  }
}
