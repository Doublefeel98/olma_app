class ProductDiscount {
  int id;
  int promotionId;
  int productId;
  int quantity;
  int percentPromotion;
  int unitPrice;
  int promotionPrice;
  int quantitySold;
  DateTime startTime;
  DateTime endTime;
  int sortOrder;
  bool status;

  ProductDiscount(
      {this.id,
      this.productId,
      this.promotionId,
      this.quantity,
      this.percentPromotion,
      this.unitPrice,
      this.promotionPrice,
      this.quantitySold,
      this.startTime,
      this.endTime,
      this.sortOrder,
      this.status});

  ProductDiscount.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    promotionId = json['promotionId'];
    productId = json['productId'];
    quantity = json['quantity'];
    percentPromotion = json['percentPromotion'];
    unitPrice = json['unitPrice'];
    promotionPrice = json['promotionPrice'];
    quantitySold = json['quantitySold'];
    startTime =
        json['startTime'] != null ? DateTime.tryParse(json['startTime']) : null;
    endTime =
        json['endTime'] != null ? DateTime.tryParse(json['endTime']) : null;
    sortOrder = json['sortOrder'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['promotionId'] = this.promotionId;
    data['productId'] = this.productId;
    data['quantity'] = this.quantity;
    data['percentPromotion'] = this.percentPromotion;
    data['unitPrice'] = this.unitPrice;
    data['promotionPrice'] = this.promotionPrice;
    data['quantitySold'] = this.quantitySold;
    data['startTime'] = this.startTime.toString();
    data['endTime'] = this.endTime.toString();
    data['sortOrder'] = this.sortOrder;
    data['status'] = this.status;
    return data;
  }
}
