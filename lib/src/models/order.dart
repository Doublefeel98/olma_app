import 'package:olmaapp/src/models/deliveryAddress.dart';

class CheckoutResponse {
  bool result;
  String message;

  CheckoutResponse({this.result, this.message});

  CheckoutResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['message'] = this.message;
    return data;
  }
}

class OrderResponse {
  bool success;
  String message;
  List<Order> data;

  OrderResponse({this.success, this.message, this.data});

  OrderResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Order>();
      json['data'].forEach((v) {
        data.add(new Order.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Order {
  int id;
  String invoiceNo;
  DeliveryAddress deliveryAddress;
  int totalProduction;
  int totalMoney;
  int shippingFee;
  bool isFreeShip;
  int discountAmount;
  int rewardPoints;
  int status;
  String statusStr;
  String paymentMethod;
  String paymentMethodStr;
  String shippingMethod;
  String shippingMethodStr;
  DateTime createdAt;

  Order(
      {this.id,
      this.invoiceNo,
      this.deliveryAddress,
      this.totalProduction,
      this.totalMoney,
      this.shippingFee,
      this.isFreeShip,
      this.discountAmount,
      this.rewardPoints,
      this.status,
      this.statusStr,
      this.paymentMethod,
      this.paymentMethodStr,
      this.shippingMethod,
      this.shippingMethodStr,
      this.createdAt});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    invoiceNo = json['invoiceNo'];
    deliveryAddress = json['deliveryAddress'] != null
        ? new DeliveryAddress.fromJson(json['deliveryAddress'])
        : null;
    totalProduction = json['totalProduction'];
    totalMoney = json['totalMoney'];
    shippingFee = json['shippingFee'];
    isFreeShip = json['isFreeShip'];
    discountAmount = json['discountAmount'];
    rewardPoints = json['rewardPoints'];
    status = json['status'];
    statusStr = json['statusStr'];
    paymentMethod = json['paymentMethod'];
    paymentMethodStr = json['paymentMethodStr'];
    shippingMethod = json['shippingMethod'];
    shippingMethodStr = json['shippingMethodStr'];
    createdAt = json['created_at'] != null
        ? DateTime.tryParse(json['created_at'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['invoiceNo'] = this.invoiceNo;
    if (this.deliveryAddress != null) {
      data['deliveryAddress'] = this.deliveryAddress.toJson();
    }
    data['totalProduction'] = this.totalProduction;
    data['totalMoney'] = this.totalMoney;
    data['shippingFee'] = this.shippingFee;
    data['isFreeShip'] = this.isFreeShip;
    data['discountAmount'] = this.discountAmount;
    data['rewardPoints'] = this.rewardPoints;
    data['status'] = this.status;
    data['statusStr'] = this.statusStr;
    data['paymentMethod'] = this.paymentMethod;
    data['paymentMethodStr'] = this.paymentMethodStr;
    data['shippingMethod'] = this.shippingMethod;
    data['shippingMethodStr'] = this.shippingMethodStr;
    data['created_at'] = this.createdAt;
    return data;
  }
}
