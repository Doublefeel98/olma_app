class Brand {
  int id;
  String name;
  String slug;
  String describe;

  Brand({this.id, this.name, this.slug, this.describe});

  Brand.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    describe = json['describe'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['describe'] = this.describe;
    return data;
  }
}

class BrandFliter {
  Brand brand;
  int totalProduction;

  BrandFliter({this.brand, this.totalProduction});

  BrandFliter.fromJson(Map<String, dynamic> json) {
    brand = json['brand'] != null ? new Brand.fromJson(json['brand']) : null;
    totalProduction = json['totalProduction'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    data['totalProduction'] = this.totalProduction;
    return data;
  }
}
