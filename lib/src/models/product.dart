import 'category_product.dart';
import 'product_discount.dart';
import 'brand.dart';
import 'list_product_item.dart';
import 'attibute.dart';

class Product {
  int id;
  String idSelect;
  String name;
  String info;
  String poster;
  Brand brand;
  CategoryProduct categoryProduct;
  int price;
  int quantity;
  String sku;
  int avgRating;
  int totalRating;
  int sortOrder;
  String dateAvailable;
  List<ProductItem> similarProducts;
  List<ProductDetail> listProductDetails;
  List<ProductImage> listProductImages;
  List<ProductTab> listProductTabs;
  List<ProductOption> listProductOptions;
  List<Attribute> listAttributes;
  List<Breadcrumb> listBreadcrumbs;
  String stockStatus;
  String stockStatusStr;
  ProductDiscount productDiscount;

  Product(
      {this.id,
      this.idSelect,
      this.name,
      this.info,
      this.poster,
      this.brand,
      this.categoryProduct,
      this.price,
      this.quantity,
      this.sku,
      this.avgRating,
      this.totalRating,
      this.sortOrder,
      this.dateAvailable,
      this.similarProducts,
      this.listProductDetails,
      this.listProductImages,
      this.listProductTabs,
      this.listProductOptions,
      this.listAttributes,
      this.listBreadcrumbs,
      this.stockStatus,
      this.stockStatusStr,
      this.productDiscount});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idSelect = json['idSelect'];
    name = json['name'];
    info = json['info'];
    poster = json['poster'];
    brand = json['brand'] != null ? new Brand.fromJson(json['brand']) : null;
    categoryProduct = json['categoryProduct'] != null
        ? new CategoryProduct.fromJson(json['categoryProduct'])
        : null;
    price = json['price'];
    quantity = json['quantity'];
    sku = json['sku'];
    avgRating = json['avgRating'];
    totalRating = json['totalRating'];
    sortOrder = json['sortOrder'];
    dateAvailable = json['dateAvailable'];
    if (json['similarProducts'] != null) {
      similarProducts = new List<ProductItem>();
      json['similarProducts'].forEach((v) {
        similarProducts.add(new ProductItem.fromJson(v));
      });
    }
    if (json['listProductDetails'] != null) {
      listProductDetails = new List<ProductDetail>();
      json['listProductDetails'].forEach((v) {
        listProductDetails.add(new ProductDetail.fromJson(v));
      });
    }
    if (json['listProductImages'] != null) {
      listProductImages = new List<ProductImage>();
      json['listProductImages'].forEach((v) {
        listProductImages.add(new ProductImage.fromJson(v));
      });
    }
    if (json['listProductTabs'] != null) {
      listProductTabs = new List<ProductTab>();
      json['listProductTabs'].forEach((v) {
        listProductTabs.add(new ProductTab.fromJson(v));
      });
    }
    if (json['listProductOptions'] != null) {
      listProductOptions = new List<ProductOption>();
      json['listProductOptions'].forEach((v) {
        listProductOptions.add(new ProductOption.fromJson(v));
      });
    }
    if (json['listAttributes'] != null) {
      listAttributes = new List<Attribute>();
      json['listAttributes'].forEach((v) {
        listAttributes.add(new Attribute.fromJson(v));
      });
    }
    if (json['listBreadcrumbs'] != null) {
      listBreadcrumbs = new List<Breadcrumb>();
      json['listBreadcrumbs'].forEach((v) {
        listBreadcrumbs.add(new Breadcrumb.fromJson(v));
      });
    }
    stockStatus = json['stockStatus'];
    stockStatusStr = json['stockStatusStr'];
    productDiscount = json['productDiscount'] != null
        ? new ProductDiscount.fromJson(json['productDiscount'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idSelect'] = this.idSelect;
    data['name'] = this.name;
    data['info'] = this.info;
    data['poster'] = this.poster;
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    if (this.categoryProduct != null) {
      data['categoryProduct'] = this.categoryProduct.toJson();
    }
    data['price'] = this.price;
    data['quantity'] = this.quantity;
    data['sku'] = this.sku;
    data['avgRating'] = this.avgRating;
    data['totalRating'] = this.totalRating;
    data['sortOrder'] = this.sortOrder;
    data['dateAvailable'] = this.dateAvailable;
    if (this.similarProducts != null) {
      data['similarProducts'] =
          this.similarProducts.map((v) => v.toJson()).toList();
    }
    if (this.listProductDetails != null) {
      data['listProductDetails'] =
          this.listProductDetails.map((v) => v.toJson()).toList();
    }
    if (this.listProductImages != null) {
      data['listProductImages'] =
          this.listProductImages.map((v) => v.toJson()).toList();
    }
    if (this.listProductTabs != null) {
      data['listProductTabs'] =
          this.listProductTabs.map((v) => v.toJson()).toList();
    }
    if (this.listProductOptions != null) {
      data['listProductOptions'] =
          this.listProductOptions.map((v) => v.toJson()).toList();
    }
    if (this.listAttributes != null) {
      data['listAttributes'] =
          this.listAttributes.map((v) => v.toJson()).toList();
    }
    if (this.listBreadcrumbs != null) {
      data['listBreadcrumbs'] =
          this.listBreadcrumbs.map((v) => v.toJson()).toList();
    }
    data['stockStatus'] = this.stockStatus;
    data['stockStatusStr'] = this.stockStatusStr;
    if (this.productDiscount != null) {
      data['productDiscount'] = this.productDiscount.toJson();
    }
    return data;
  }
}

class ProductDetail {
  int id;
  int productId;
  String name;
  String content;
  int sortOrder;

  ProductDetail(
      {this.id, this.productId, this.name, this.content, this.sortOrder});

  ProductDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    name = json['name'];
    content = json['content'];
    sortOrder = json['sortOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['name'] = this.name;
    data['content'] = this.content;
    data['sortOrder'] = this.sortOrder;
    return data;
  }
}

class ProductImage {
  int id;
  int productId;
  String link;
  int sortOrder;

  ProductImage({this.id, this.productId, this.link, this.sortOrder});

  ProductImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    link = json['link'];
    sortOrder = json['sortOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['link'] = this.link;
    data['sortOrder'] = this.sortOrder;
    return data;
  }
}

class ProductTab {
  int id;
  int productId;
  String name;
  String content;
  int sortOrder;

  ProductTab(
      {this.id, this.productId, this.name, this.content, this.sortOrder});

  ProductTab.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    name = json['name'];
    content = json['content'];
    sortOrder = json['sortOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['name'] = this.name;
    data['content'] = this.content;
    data['sortOrder'] = this.sortOrder;
    return data;
  }
}

class ProductOption {
  int id;
  String name;
  String sku;
  String poster;
  int price;
  int quantity;
  List<ProductImage> listProductImages;
  ProductDiscount productDiscount;
  Map<String, dynamic> listAttributeValue;

  ProductOption(
      {this.id,
      this.name,
      this.sku,
      this.poster,
      this.price,
      this.quantity,
      this.listProductImages,
      this.productDiscount,
      this.listAttributeValue});

  ProductOption.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sku = json['sku'];
    poster = json['poster'];
    price = json['price'];
    quantity = json['quantity'];
    if (json['listProductImages'] != null) {
      listProductImages = new List<ProductImage>();
      json['listProductImages'].forEach((v) {
        listProductImages.add(new ProductImage.fromJson(v));
      });
    }

    productDiscount = json['productDiscount'];
    listAttributeValue = json['listAttributeValue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['sku'] = this.sku;
    data['poster'] = this.poster;
    data['price'] = this.price;
    data['quantity'] = this.quantity;
    if (this.listProductImages != null) {
      data['listProductImages'] =
          this.listProductImages.map((v) => v.toJson()).toList();
    }
    data['productDiscount'] = this.productDiscount;
    data['listAttributeValue'] = this.listAttributeValue;
    return data;
  }
}

class Breadcrumb {
  int id;
  String name;
  String slug;
  int sortOrder;
  int parentId;
  String imageUrl;
  String imageHtml;
  int level;
  String bannerUrl;
  String linkAdvertisement;

  Breadcrumb(
      {this.id,
      this.name,
      this.slug,
      this.sortOrder,
      this.parentId,
      this.imageUrl,
      this.imageHtml,
      this.level,
      this.bannerUrl,
      this.linkAdvertisement});

  Breadcrumb.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    sortOrder = json['sortOrder'];
    parentId = json['parentId'];
    imageUrl = json['imageUrl'];
    imageHtml = json['imageHtml'];
    level = json['level'];
    bannerUrl = json['bannerUrl'];
    linkAdvertisement = json['linkAdvertisement'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['sortOrder'] = this.sortOrder;
    data['parentId'] = this.parentId;
    data['imageUrl'] = this.imageUrl;
    data['imageHtml'] = this.imageHtml;
    data['level'] = this.level;
    data['bannerUrl'] = this.bannerUrl;
    data['linkAdvertisement'] = this.linkAdvertisement;
    return data;
  }
}
