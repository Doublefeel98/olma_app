import 'package:olmaapp/src/models/product.dart';

import 'category_product.dart';

import 'banner.dart';
import 'brand.dart';
import 'product_discount.dart';

class ListProductItems {
  int currentPage;
  List<ProductItem> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  String perPage;
  String prevPageUrl;
  int to;
  int total;

  ListProductItems(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  ListProductItems.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<ProductItem>();
      json['data'].forEach((v) {
        data.add(new ProductItem.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class ProductItem {
  int id;
  String name;
  String poster;
  int price;
  int quantity;
  String stockStatus;
  String stockStatusStr;
  ProductDiscount productDiscount;
  String avgRating;
  int totalRating;

  ProductItem(
      {this.id,
      this.name,
      this.poster,
      this.price,
      this.quantity,
      this.stockStatus,
      this.stockStatusStr,
      this.productDiscount,
      this.avgRating,
      this.totalRating});

  ProductItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    poster = json['poster'];
    price = json['price'];
    quantity = json['quantity'];
    stockStatus = json['stockStatus'];
    stockStatusStr = json['stockStatusStr'];
    productDiscount = json['productDiscount'] != null
        ? new ProductDiscount.fromJson(json['productDiscount'])
        : null;
    avgRating = json['avgRating'];
    totalRating = json['totalRating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['poster'] = this.poster;
    data['price'] = this.price;
    data['quantity'] = this.quantity;
    data['stockStatus'] = this.stockStatus;
    data['stockStatusStr'] = this.stockStatusStr;
    if (this.productDiscount != null) {
      data['productDiscount'] = this.productDiscount.toJson();
    }
    data['avgRating'] = this.avgRating;
    data['totalRating'] = this.totalRating;
    return data;
  }
}

class ListProductForView {
  ListProductItems listProducts;
  List<CategoryProduct> listCategoryProducts;
  Banner banner;
  String title;
  List<BrandFliter> listBrands;

  ListProductForView(
      {this.listProducts, this.listCategoryProducts, this.banner, this.title});

  ListProductForView.fromJson(Map<String, dynamic> json) {
    listProducts = json['listProducts'] != null
        ? new ListProductItems.fromJson(json['listProducts'])
        : null;
    if (json['listCategoryProducts'] != null) {
      listCategoryProducts = new List<CategoryProduct>();
      json['listCategoryProducts'].forEach((v) {
        listCategoryProducts.add(new CategoryProduct.fromJson(v));
      });
    }
    if (json['listBrands'] != null) {
      listBrands = new List<BrandFliter>();
      json['listBrands'].forEach((v) {
        listBrands.add(new BrandFliter.fromJson(v));
      });
    }
    banner =
        json['banner'] != null ? new Banner.fromJson(json['banner']) : null;
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.listProducts != null) {
      data['listProducts'] = this.listProducts.toJson();
    }
    if (this.listCategoryProducts != null) {
      data['listCategoryProducts'] =
          this.listCategoryProducts.map((v) => v.toJson()).toList();
    }
    if (this.banner != null) {
      data['banner'] = this.banner.toJson();
    }
    data['title'] = this.title;
    return data;
  }
}

class ListProductForYou {
  List<ProductItem> listProducts;
  List<CategoryProduct> listCategoryProducts;
  Banner banner;
  List<BrandFliter> listBrands;

  ListProductForYou(
      {this.listProducts, this.listCategoryProducts, this.banner});

  ListProductForYou.fromJson(Map<String, dynamic> json) {
    if (json['listProducts'] != null) {
      listProducts = new List<ProductItem>();
      json['listProducts'].forEach((v) {
        listProducts.add(new ProductItem.fromJson(v));
      });
    }
    if (json['listCategoryProducts'] != null) {
      listCategoryProducts = new List<CategoryProduct>();
      json['listCategoryProducts'].forEach((v) {
        listCategoryProducts.add(new CategoryProduct.fromJson(v));
      });
    }
    if (json['listBrands'] != null) {
      listBrands = new List<BrandFliter>();
      json['listBrands'].forEach((v) {
        listBrands.add(new BrandFliter.fromJson(v));
      });
    }
    banner =
        json['banner'] != null ? new Banner.fromJson(json['banner']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.listProducts != null) {
      data['listProducts'] = this.listProducts.map((v) => v.toJson()).toList();
    }
    if (this.listCategoryProducts != null) {
      data['listCategoryProducts'] =
          this.listCategoryProducts.map((v) => v.toJson()).toList();
    }
    if (this.banner != null) {
      data['banner'] = this.banner.toJson();
    }
    return data;
  }
}
