class City {
  String name;
  String slug;
  String type;
  String nameWithType;
  String code;

  City({this.name, this.slug, this.type, this.nameWithType, this.code});

  City.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    slug = json['slug'];
    type = json['type'];
    nameWithType = json['name_with_type'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['type'] = this.type;
    data['name_with_type'] = this.nameWithType;
    data['code'] = this.code;
    return data;
  }
}

class District {
  String name;
  String type;
  String slug;
  String nameWithType;
  String path;
  String pathWithType;
  String code;
  String parentCode;

  District(
      {this.name,
      this.type,
      this.slug,
      this.nameWithType,
      this.path,
      this.pathWithType,
      this.code,
      this.parentCode});

  District.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    slug = json['slug'];
    nameWithType = json['name_with_type'];
    path = json['path'];
    pathWithType = json['path_with_type'];
    code = json['code'];
    parentCode = json['parent_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['slug'] = this.slug;
    data['name_with_type'] = this.nameWithType;
    data['path'] = this.path;
    data['path_with_type'] = this.pathWithType;
    data['code'] = this.code;
    data['parent_code'] = this.parentCode;
    return data;
  }
}

class Ward {
  String name;
  String type;
  String slug;
  String nameWithType;
  String path;
  String pathWithType;
  String code;
  String parentCode;

  Ward(
      {this.name,
      this.type,
      this.slug,
      this.nameWithType,
      this.path,
      this.pathWithType,
      this.code,
      this.parentCode});

  Ward.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
    slug = json['slug'];
    nameWithType = json['name_with_type'];
    path = json['path'];
    pathWithType = json['path_with_type'];
    code = json['code'];
    parentCode = json['parent_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['type'] = this.type;
    data['slug'] = this.slug;
    data['name_with_type'] = this.nameWithType;
    data['path'] = this.path;
    data['path_with_type'] = this.pathWithType;
    data['code'] = this.code;
    data['parent_code'] = this.parentCode;
    return data;
  }
}
