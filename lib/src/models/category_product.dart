class CategoryProduct {
  int id;
  String name;
  int sortOrder;
  int parentId;
  String imageUrl;
  String imageHtml;
  int level;
  int bannerId;
  String bannerUrl;
  String linkAdvertisement;
  int countProduct;
  List<CategoryProduct> children;

  CategoryProduct(
      {this.id,
      this.name,
      this.sortOrder,
      this.parentId,
      this.imageUrl,
      this.imageHtml,
      this.level,
      this.bannerId,
      this.bannerUrl,
      this.linkAdvertisement,
      this.countProduct,
      this.children});

  CategoryProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sortOrder = json['sortOrder'];
    parentId = json['parentId'];
    imageUrl = json['imageUrl'];
    imageHtml = json['imageHtml'];
    level = json['level'];
    bannerId = json['bannerId'];
    bannerUrl = json['bannerUrl'];
    linkAdvertisement = json['linkAdvertisement'];
    countProduct = json['countProduct'];
    if (json['children'] != null) {
      children = new List<CategoryProduct>();
      json['children'].forEach((v) {
        children.add(new CategoryProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['sortOrder'] = this.sortOrder;
    data['parentId'] = this.parentId;
    data['imageUrl'] = this.imageUrl;
    data['imageHtml'] = this.imageHtml;
    data['level'] = this.level;
    data['bannerId'] = this.bannerId;
    data['bannerUrl'] = this.bannerUrl;
    data['linkAdvertisement'] = this.linkAdvertisement;
    data['countProduct'] = this.countProduct;
    if (this.children != null) {
      data['children'] = this.children.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
