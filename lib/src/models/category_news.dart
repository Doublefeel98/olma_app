class CategoryNews {
  int id;
  String name;
  String slug;
  String description;
  int sortOrder;
  List<News> listNews;

  CategoryNews(
      {this.id,
      this.name,
      this.slug,
      this.description,
      this.sortOrder,
      this.listNews});

  CategoryNews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    description = json['description'];
    sortOrder = json['sortOrder'];
    if (json['listNews'] != null) {
      listNews = new List<News>();
      json['listNews'].forEach((v) {
        listNews.add(new News.fromJson(v)..categoryNews = this);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['sortOrder'] = this.sortOrder;
    if (this.listNews != null) {
      data['listNews'] = this.listNews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class News {
  int id;
  String title;
  String content;
  String poster;
  Author author;
  String slug;
  int avgRating;
  int viewed;
  CategoryNews categoryNews;
  String createdAt;

  News(
      {this.id,
      this.title,
      this.content,
      this.poster,
      this.author,
      this.slug,
      this.avgRating,
      this.viewed,
      this.categoryNews,
      this.createdAt});

  News.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    content = json['content'];
    poster = json['poster'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    slug = json['slug'];
    avgRating = json['avgRating'];
    viewed = json['viewed'];
    categoryNews = json['categoryNews'] != null
        ? new CategoryNews.fromJson(json['categoryNews'])
        : null;
    createdAt = json['created_at'] != null ? json['created_at'] : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['content'] = this.content;
    data['poster'] = this.poster;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    data['slug'] = this.slug;
    data['avgRating'] = this.avgRating;
    data['viewed'] = this.viewed;
    if (this.categoryNews != null) {
      data['categoryNews'] = this.categoryNews.toJson();
    }
    data['created_at'] = this.createdAt;
    return data;
  }
}

class Author {
  int id;
  String avatar;
  String fullName;
  String name;
  String email;

  Author({this.id, this.avatar, this.fullName, this.name, this.email});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    name = json['name'];
    avatar = json['avatar'];
    fullName = json['fullName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['name'] = this.name;
    data['avatar'] = this.avatar;
    return data;
  }
}
