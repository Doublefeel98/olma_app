import 'list_product_item.dart';

class PromotionForHome {
  int id;
  String name;
  String slug;
  String banner;

  PromotionForHome({this.id, this.name, this.slug, this.banner});

  PromotionForHome.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    banner = json['banner'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['banner'] = this.banner;
    return data;
  }
}

class PromotionForDetail {
  int id;
  String name;
  String slug;
  String description;
  int percentPromotion;
  String content;
  String banner;
  String startTime;
  String endTime;
  ListProductItems listProducts;

  PromotionForDetail(
      {this.id,
      this.name,
      this.slug,
      this.description,
      this.percentPromotion,
      this.content,
      this.banner,
      this.startTime,
      this.endTime,
      this.listProducts});

  PromotionForDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    description = json['description'];
    percentPromotion = json['percentPromotion'];
    content = json['content'];
    banner = json['banner'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    listProducts = json['listProducts'] != null
        ? new ListProductItems.fromJson(json['listProducts'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['percentPromotion'] = this.percentPromotion;
    data['content'] = this.content;
    data['banner'] = this.banner;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    if (this.listProducts != null) {
      data['listProducts'] = this.listProducts.toJson();
    }
    return data;
  }
}
