import 'product_discount.dart';

class Cart {
  int totalPrice;
  int totalProduct;
  int totalProductSelected;
  List<CartItem> listCartItem;
  List<CartItem> listCartItemDisable;

  Cart(
      {this.totalPrice,
      this.totalProduct,
      this.totalProductSelected,
      this.listCartItem,
      this.listCartItemDisable});

  Cart.fromJson(Map<String, dynamic> json) {
    totalPrice = json['totalPrice'];
    totalProduct = json['totalProduct'];
    totalProductSelected = json['totalProductSelected'];
    if (json['listCartItem'] != null) {
      listCartItem = new List<CartItem>();
      json['listCartItem'].forEach((v) {
        listCartItem.add(new CartItem.fromJson(v));
      });
    }
    if (json['listCartItemDisable'] != null) {
      listCartItemDisable = new List<Null>();
      json['listCartItemDisable'].forEach((v) {
        listCartItemDisable.add(new CartItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalPrice'] = this.totalPrice;
    data['totalProduct'] = this.totalProduct;
    data['totalProductSelected'] = this.totalProductSelected;
    if (this.listCartItem != null) {
      data['listCartItem'] = this.listCartItem.map((v) => v.toJson()).toList();
    }
    if (this.listCartItemDisable != null) {
      data['listCartItemDisable'] =
          this.listCartItemDisable.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CartItem {
  int id;
  int quantity;
  int productId;
  String productName;
  String productPoster;
  int productPrice;
  int productQuantity;
  ProductDiscount productDiscount;
  int totalPriceCartItem;
  int quantityDiscount;
  bool selected;

  CartItem(
      {this.id,
      this.quantity,
      this.productId,
      this.productName,
      this.productPoster,
      this.productPrice,
      this.productQuantity,
      this.productDiscount,
      this.totalPriceCartItem,
      this.quantityDiscount,
      this.selected});

  CartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quantity = json['quantity'];
    productId = json['productId'];
    productName = json['productName'];
    productPoster = json['productPoster'];
    productPrice = json['productPrice'];
    productQuantity = json['productQuantity'];
    productDiscount = json['productDiscount'] != null
        ? new ProductDiscount.fromJson(json['productDiscount'])
        : null;
    totalPriceCartItem = json['totalPriceCartItem'];
    quantityDiscount = json['quantityDiscount'];
    selected = json['selected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    data['productId'] = this.productId;
    data['productName'] = this.productName;
    data['productPoster'] = this.productPoster;
    data['productPrice'] = this.productPrice;
    data['productQuantity'] = this.productQuantity;
    if (this.productDiscount != null) {
      data['productDiscount'] = this.productDiscount.toJson();
    }
    data['totalPriceCartItem'] = this.totalPriceCartItem;
    data['quantityDiscount'] = this.quantityDiscount;
    data['selected'] = this.selected;
    return data;
  }
}

class CartResponse {
  bool success;
  String message;
  Data data;

  CartResponse({this.success, this.message, this.data});

  CartResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  int productId;
  int quantity;

  Data({this.id, this.productId, this.quantity});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['productId'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['productId'] = this.productId;
    data['quantity'] = this.quantity;
    return data;
  }
}
