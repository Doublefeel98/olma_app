import 'user.dart';

class ListComment {
  List<Comment> listComments;
  double avgRating;
  int countRating;
  int countRatingFivePoint;
  int countRatingFourPoint;
  int countRatingThreePoint;
  int countRatingTwoPoint;
  int countRatingOnePoint;

  ListComment(
      {this.listComments,
      this.avgRating,
      this.countRating,
      this.countRatingFivePoint,
      this.countRatingFourPoint,
      this.countRatingThreePoint,
      this.countRatingTwoPoint,
      this.countRatingOnePoint});

  ListComment.fromJson(Map<String, dynamic> json) {
    if (json['listComments'] != null) {
      listComments = new List<Comment>();
      json['listComments'].forEach((v) {
        listComments.add(new Comment.fromJson(v));
      });
    }
    avgRating = double.tryParse(json['avgRating'].toString());
    countRating = json['countRating'];
    countRatingFivePoint = json['countRatingFivePoint'];
    countRatingFourPoint = json['countRatingFourPoint'];
    countRatingThreePoint = json['countRatingThreePoint'];
    countRatingTwoPoint = json['countRatingTwoPoint'];
    countRatingOnePoint = json['countRatingOnePoint'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.listComments != null) {
      data['listComments'] = this.listComments.map((v) => v.toJson()).toList();
    }
    data['avgRating'] = this.avgRating;
    data['countRating'] = this.countRating;
    data['countRatingFivePoint'] = this.countRatingFivePoint;
    data['countRatingFourPoint'] = this.countRatingFourPoint;
    data['countRatingThreePoint'] = this.countRatingThreePoint;
    data['countRatingTwoPoint'] = this.countRatingTwoPoint;
    data['countRatingOnePoint'] = this.countRatingOnePoint;
    return data;
  }
}

class Comment {
  int id;
  int userId;
  String content;
  int rating;
  int commentableId;
  String commentable;
  DateTime time;
  User user;

  Comment(
      {this.id,
      this.userId,
      this.content,
      this.rating,
      this.commentableId,
      this.commentable,
      this.time,
      this.user});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    content = json['content'];
    rating = json['rating'];
    commentableId = json['commentableId'];
    commentable = json['commentable'];
    time = json['time'] != null ? DateTime.tryParse(json['time']) : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['content'] = this.content;
    data['rating'] = this.rating;
    data['commentableId'] = this.commentableId;
    data['commentable'] = this.commentable;
    data['time'] = this.time.toString();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
